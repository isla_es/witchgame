﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a slider.
    /// Compatible with <see cref="float"/>, <see cref="System.Collections.Generic.List{T}"/> and array.
    /// Combinable with <see cref="KnReorderableListAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnRangeAttribute : Attribute
    {
        #region Fields
        private float _min = 0.0f;
        private float _max = 0.0f;
        private bool _showMinMax = false;
        #endregion

        #region Properties
        /// <summary>
        /// The min value.
        /// </summary>
        public float Min
        {
            get { return _min; }
        }

        /// <summary>
        /// The max value.
        /// </summary>
        public float Max
        {
            get { return _max; }
        }

        /// <summary>
        /// Dooes show min and max values ?
        /// </summary>
        public bool ShowMinMax
        {
            get { return _showMinMax; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a slider.
        /// Compatible with <see cref="float"/>, <see cref="System.Collections.Generic.List{T}"/> and array.
        /// Combinable with <see cref="KnReorderableListAttribute"/>.
        /// </summary>
        /// <param name="pMin">The min value.</param>
        /// <param name="pMax">The max value.</param>
        /// <param name="pShowMinMax">Does show min and max values ?</param>
        public KnRangeAttribute(float pMin, float pMax, bool pShowMinMax = false)
        {
            _min = pMin;
            _max = pMax;
            _showMinMax = pShowMinMax;
        }
        #endregion
    }
}
