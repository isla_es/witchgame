﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Display reorderable list.
    /// Compatible with <see cref="System.Collections.Generic.List{T}"/> and array.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnReorderableListAttribute : Attribute
    {
        #region Fields
        private bool _draggable = true;
        private bool _displayAddButton = true;
        private bool _displayRemoveButton = true;
        #endregion

        #region Properties
        /// <summary>
        /// Does the elements are draggables ?
        /// </summary>
        public bool Draggable
        {
            get { return _draggable; }
        }

        /// <summary>
        /// Shows add button ?
        /// </summary>
        public bool DisplayAddButton
        {
            get { return _displayAddButton; }
        }

        /// <summary>
        /// Shows remove button ?
        /// </summary>
        public bool DisplayRemoveButton
        {
            get { return _displayRemoveButton; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Display reorderable list.
        /// Compatible with <see cref="System.Collections.Generic.List{T}"/> and array.
        /// </summary>
        /// <param name="pDraggable">Does the elements are draggables ?</param>
        /// <param name="pDisplayAddButton">Shows add button ?</param>
        /// <param name="pDisplayRemoveButton">Shows remove button ?</param>
        public KnReorderableListAttribute(bool pDraggable = true, bool pDisplayAddButton = true, bool pDisplayRemoveButton = true)
        {
            _draggable = pDraggable;
            _displayAddButton = pDisplayAddButton;
            _displayRemoveButton = pDisplayRemoveButton;
        }
        #endregion
    }
}
