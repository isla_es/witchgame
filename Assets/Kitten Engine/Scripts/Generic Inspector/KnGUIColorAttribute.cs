﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
using UnityEngine;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Sets the GUI Color.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnGUIColorAttribute : Attribute
    {
        #region Fields
        private Color _color = Color.white;
        #endregion

        #region Properties
        /// <summary>
        /// The GUI Color to display.
        /// </summary>
        public Color Color
        {
            get { return _color; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Sets the GUI Color.
        /// </summary>
        /// <param name="pRed">The red composant of the color. From 0.0f to 255.0f;</param>
        /// <param name="pGreen">The green composant of the color.</param>
        /// <param name="pBlue">The blue composant of the color. From 0.0f to 255.0f;</param>
        /// <param name="pAlpha">The alpha composant of the color. From 0.0f to 255.0f;</param>
        public KnGUIColorAttribute(float pRed = 255.0f, float pGreen = 255.0f, float pBlue = 255.0f, float pAlpha = 255.0f)
        {
            _color = new Color(pRed / 255.0f, pGreen / 255.0f, pBlue / 255.0f, pAlpha / 255.0f);
        }
        #endregion
    }
}
