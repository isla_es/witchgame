﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a header above the member.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnHeaderAttribute : Attribute
    {
        #region Fields
        private string _header = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// The text to display.
        /// </summary>
        public string Header
        {
            get { return _header; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a header above the member.
        /// </summary>
        /// <param name="pHeader">The text to display.</param>
        public KnHeaderAttribute(string pHeader)
        {
            _header = pHeader;
        }
        #endregion
    }
}
