﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
using System.Collections.Generic;
using UnityEngine;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a popup field.
    /// Compatible with <see cref="int"/>, <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// Combinable with <see cref="KnReorderableListAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnPopupAttribute : Attribute
    {
        #region Fields
        private List<GUIContent> _displayedOptions = new List<GUIContent>();
        #endregion

        #region Properties
        /// <summary>
        /// The options to display.
        /// </summary>
        public List<GUIContent> DisplayedOptions
        {
            get { return _displayedOptions; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a popup field.
        /// Compatible with <see cref="int"/>, <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
        /// Combinable with <see cref="KnReorderableListAttribute"/>.
        /// </summary>
        /// <param name="pDisplayedOptions">The options to display.</param>
        public KnPopupAttribute(params string[] pDisplayedOptions)
        {
            _displayedOptions = new List<GUIContent>();
            for (int i = 0; i < pDisplayedOptions.Length; i++)
            {
                _displayedOptions.Add(new GUIContent(pDisplayedOptions[i]));
            }
        }
        #endregion
    }
}
