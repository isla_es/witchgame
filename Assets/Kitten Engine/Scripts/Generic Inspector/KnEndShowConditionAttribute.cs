﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Ends a conditional display of a control group.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnEndShowConditionAttribute : Attribute
    {
    }
}
