﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a bitwise mask field.
    /// Compatible with <see cref="int"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// Combinable with <see cref="KnReorderableListAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnMaskFieldAttribute : Attribute
    {
        #region Fields
        private string[] _displayedOptions = null;
        #endregion

        #region Properties
        /// <summary>
        /// The mask values.
        /// </summary>
        public string[] DisplayedOptions
        {
            get { return _displayedOptions; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a bitwise mask field.
        /// Compatible with <see cref="int"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
        /// Combinable with <see cref="KnReorderableListAttribute"/>.
        /// </summary>
        /// <param name="pDisplayedOptions">The mask values.</param>
        public KnMaskFieldAttribute(params string[] pDisplayedOptions)
        {
            _displayedOptions = pDisplayedOptions;
        }
        #endregion
    }
}
