﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
using UnityEngine;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a line.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnSeparatorLineAttribute : Attribute
    {
        #region Fields
        private int _topMargin = 3;
        private int _bottomMargin = 3;
        private Color _lineColor = Color.white;
        private bool _hasSetColor = false;
        private float _thickness = 1.0f;
        #endregion

        #region Properties
        /// <summary>
        /// The top margin of the line.
        /// </summary>
        public int TopMargin
        {
            get { return _topMargin; }
        }

        /// <summary>
        /// The bottom margin of the line.
        /// </summary>
        public int BottomMargin
        {
            get { return _bottomMargin; }
        }

        /// <summary>
        /// The color of the line.
        /// </summary>
        public Color LineColor
        {
            get { return _lineColor; }
        }

        /// <summary>
        /// Unity theme color or user color ?
        /// </summary>
        public bool HasSetColor
        {
            get { return _hasSetColor; }
        }

        /// <summary>
        /// The line thickness.
        /// </summary>
        public float Thickness
        {
            get { return _thickness; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a line.
        /// </summary>
        /// <param name="pTopMargin">The top margin of the line.</param>
        /// <param name="pBottomMargin">The bottom margin of the line.</param>
        /// <param name="pThickness">The line thickness.</param>
        public KnSeparatorLineAttribute(int pTopMargin = 3, int pBottomMargin = 3, float pThickness = 1.0f)
        {
            _topMargin = pTopMargin;
            _bottomMargin = pBottomMargin;
            _thickness = pThickness;
        }

        /// <summary>
        /// Displays a line with the given color.
        /// </summary>
        /// <param name="pRed">The red composant of the line color. From 0.0f to 255.0f.</param>
        /// <param name="pGreen">The green composant of the line color. From 0.0f to 255.0f.</param>
        /// <param name="pBlue">The blue composant of the line color. From 0.0f to 255.0f.</param>
        /// <param name="pAlpha">The alpha composant of the line color. From 0.0f to 255.0f.</param>
        /// <param name="pTopMargin">The top margin of the line.</param>
        /// <param name="pBottomMargin">The bottom margin of the line.</param>
        /// <param name="pThickness">The line thickness.</param>
        public KnSeparatorLineAttribute(float pRed, float pGreen, float pBlue, float pAlpha = 255.0f, int pTopMargin = 3, int pBottomMargin = 3, float pThickness = 1.0f)
        {
            _lineColor = new Color(pRed / 255.0f, pGreen / 255.0f, pBlue / 255.0f, pAlpha / 255.0f);
            _hasSetColor = true;
            _topMargin = pTopMargin;
            _bottomMargin = pBottomMargin;
            _thickness = pThickness;
        }
        #endregion
    }
}
