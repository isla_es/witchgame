﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Creates a classic Unity Popup but use a method to get the displayed options.
    /// Compatible with <see cref="int"/>, <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// Combinable with <see cref="KnReorderableListAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnAdvancedPopupAttribute : KnInvokeMethodBaseAttribute
    {
        #region Constructors
        /// <summary>
        /// Creates a classic Unity Popup but use a method to get the displayed options.
        /// Compatible with <see cref="int"/>, <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
        /// Combinable with <see cref="KnReorderableListAttribute"/>.
        /// </summary>
        /// <param name="pClass">If null, calls the method on the target, else call static method.</param>
        /// <param name="pFunction">The name of the method to call. The function must return a <see cref="System.Collections.Generic"/> of <see cref="UnityEngine.GUIContent"/>.</param>
        /// <param name="pIsEditor">Where to get the class (only useful for static methods).</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        public KnAdvancedPopupAttribute(string pClass, string pFunction, bool pIsEditor = false, object[] pParameters = null)
            : base(pClass, pFunction, pIsEditor, E_EditorStateCondition.All, 0, false, pParameters)
        {
        }

        /// <summary>
        /// Creates a classic Unity Popup but use a method to get the displayed options.
        /// Compatibles with int, string.
        /// Combinables with <see cref="KnReorderableListAttribute"/>.
        /// </summary>
        /// <param name="pClass">If null, calls the method on the target, else call static method.</param>
        /// <param name="pFunction">The name of the method to call. The function must return a <see cref="System.Collections.Generic"/> of <see cref="UnityEngine.GUIContent"/>.</param>
        /// <param name="pAssembly">Where to get the class (only useful for static methods).</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        public KnAdvancedPopupAttribute(string pClass, string pFunction, string pAssembly, object[] pParameters = null)
            : base(pClass, pFunction, pAssembly, E_EditorStateCondition.All, 0, false, pParameters)
        {
        }
        #endregion
    }
}
