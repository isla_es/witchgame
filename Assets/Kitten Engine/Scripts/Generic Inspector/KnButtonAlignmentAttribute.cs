﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// The type of buttons alignment.
    /// </summary>
    public enum E_ButtonAlignment
    {
        /// <summary>
        /// Displays the buttons horizontally and the field below.
        /// </summary>
        Boxed,
        /// <summary>
        /// Displays the buttons horizontally following by the field.
        /// </summary>
        Horizontal,
        /// <summary>
        /// Displays the buttons vertically following by the field.
        /// </summary>
        Vertical
    }

    /// <summary>
    /// Displays buttons regarding to the alignment type.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnButtonAlignmentAttribute : Attribute
    {
        #region Fields
        private E_ButtonAlignment _alignment = E_ButtonAlignment.Horizontal;
        #endregion

        #region Properties
        /// <summary>
        /// The buttons alignment type.
        /// </summary>
        public E_ButtonAlignment Alignment
        {
            get { return _alignment; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays buttons regarding to the alignment type.
        /// </summary>
        /// <param name="pAlignment">The buttons alignment type.</param>
        public KnButtonAlignmentAttribute(E_ButtonAlignment pAlignment = E_ButtonAlignment.Boxed)
        {
            _alignment = pAlignment;
        }
        #endregion
    }
}
