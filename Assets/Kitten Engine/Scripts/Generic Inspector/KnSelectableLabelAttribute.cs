﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays the member as a selectable label.
    /// Compatible with <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// Works with properties that has only a getter.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnSelectableLabelAttribute : Attribute
    {
    }
}
