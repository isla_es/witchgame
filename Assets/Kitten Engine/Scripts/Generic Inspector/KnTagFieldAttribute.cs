﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a Tag field.
    /// Compatible with <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and array.
    /// Combinable with <see cref="KnReorderableListAttribute"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnTagFieldAttribute : Attribute
    {
    }
}
