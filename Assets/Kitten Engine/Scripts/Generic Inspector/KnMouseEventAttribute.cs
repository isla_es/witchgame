﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
using UnityEngine;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// The type of events that can be rised.
    /// </summary>
    public enum E_EventType
    {
        /// <summary>
        /// When a mouse button is down.
        /// </summary>
        MouseDown = 0,
        /// <summary>
        /// When a mouse button is up.
        /// </summary>
        MouseUp = 1,
        /// <summary>
        /// When the mouse move.
        /// </summary>
        MouseMove = 2,
        /// <summary>
        /// When the mouse is dragged.
        /// </summary>
        MouseDrag = 3,
        /// <summary>
        /// When the mouse wheel is scrolled.
        /// </summary>
        ScrollWheel = 6
    }

    /// <summary>
    /// The different mouse buttons
    /// </summary>
    public enum E_MouseButton
    {
        /// <summary>
        /// None of the mouse buttons.
        /// </summary>
        None = -1,
        /// <summary>
        /// Left button.
        /// </summary>
        Left = 0,
        /// <summary>
        /// Right button.
        /// </summary>
        Right = 1,
        /// <summary>
        /// Middle button.
        /// </summary>
        Middle = 2
    }


    /// <summary>
    /// Calls a method responding to a mouse event.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class KnMouseEventAttribute : KnInvokeMethodBaseAttribute
    {
        #region Fields
        private int _button = 0;
        private EventModifiers _modifiers = EventModifiers.Alt;
        private bool _useModifiers = false;
        private EventType _type = EventType.ContextClick;
        #endregion

        #region Properties
        /// <summary>
        /// The mouse button rising the event.
        /// </summary>
        public int Button
        {
            get { return _button; }
        }

        /// <summary>
        /// The key modifiers rising the event.
        /// </summary>
        public EventModifiers Modifiers
        {
            get { return _modifiers; }
        }

        /// <summary>
        /// Does the event will rise with modifiers ?
        /// </summary>
        public bool UseModifiers
        {
            get { return _useModifiers; }
        }

        /// <summary>
        /// The type of event to rise.
        /// </summary>
        public EventType Type
        {
            get { return _type; }
        }
        #endregion

        #region Constructors
        private KnMouseEventAttribute(string pClass, string pFunction, string pAssembly, E_EditorStateCondition pCallCondition, int pOrder, bool pAllowMultipleTargets, E_EventType pType, E_MouseButton pButton, bool pUseModifiers, object[] pParameters)
            : base(pClass, pFunction, pAssembly, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
            _type = (EventType)pType;
            _button = (int)pButton;
            _useModifiers = pUseModifiers;
        }

        private KnMouseEventAttribute(string pClass, string pFunction, bool pIsEditor, E_EditorStateCondition pCallCondition, int pOrder, bool pAllowMultipleTargets, E_EventType pType, E_MouseButton pButton, bool pUseModifiers, object[] pParameters)
            : base(pClass, pFunction, pIsEditor, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
            _type = (EventType)pType;
            _button = (int)pButton;
            _useModifiers = pUseModifiers;
        }

        /// <summary>
        /// Calls a method responding to a mouse event.
        /// </summary>
        /// <param name="pClass">The class that holds the method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pButton">The mouse button rising the event.</param>
        /// <param name="pModifiers">The key modifiers rising the event.</param>
        /// <param name="pType">The type of event to rise.</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "event" if you want to send the current event.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        /// <param name="pIsEditor">Where to get the class (only useful for static methods).</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        public KnMouseEventAttribute(string pClass, string pFunction, E_MouseButton pButton, EventModifiers pModifiers, E_EventType pType, object[] pParameters, bool pIsEditor, E_EditorStateCondition pCallCondition = E_EditorStateCondition.All, int pOrder = 0, bool pAllowMultipleTargets = false)
            : this(pClass, pFunction, pIsEditor, pCallCondition, pOrder, pAllowMultipleTargets, pType, pButton, true, pParameters)
        {
            _modifiers = pModifiers;
        }

        /// <summary>
        /// Calls a method responding to a mouse event.
        /// </summary>
        /// <param name="pClass">The class that holds the method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pButton">The mouse button rising the event.</param>
        /// <param name="pModifiers">The key modifiers rising the event.</param>
        /// <param name="pType">The type of event to rise.</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "event" if you want to send the current event.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        /// <param name="pAssembly">Where to get the class (only useful for static methods).</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        public KnMouseEventAttribute(string pClass, string pFunction, E_MouseButton pButton, EventModifiers pModifiers, E_EventType pType, object[] pParameters, string pAssembly = KnInvokeMethodBaseAttribute.ASSEMBLY_GAME, E_EditorStateCondition pCallCondition = E_EditorStateCondition.All, int pOrder = 0, bool pAllowMultipleTargets = false)
            : this(pClass, pFunction, pAssembly, pCallCondition, pOrder, pAllowMultipleTargets, pType, pButton, true, pParameters)
        {
            _modifiers = pModifiers;
        }
        #endregion
    }
}
