﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.EditorUtils;
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Controls how controls are displayed.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnDisplayAttribute : Attribute
    {
        #region Fields
        private E_DisplayType _displayType = E_DisplayType.Normal;
        #endregion

        #region Properties
        /// <summary>
        /// The type of display.
        /// </summary>
        public E_DisplayType DisplayType
        {
            get { return _displayType; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Controls how controls are displayed.
        /// </summary>
        /// <param name="pDisplayType">The type of display.</param>
        public KnDisplayAttribute(E_DisplayType pDisplayType)
        {
            _displayType = pDisplayType;
        }
        #endregion
    }
}
