﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Sets the label of collection's items.
    /// Compatible with <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// Combinable with <see cref="KnReorderableListAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnItemLabelAttribute : Attribute
    {
        #region Fields
        private string _label = null;
        #endregion

        #region Properties
        /// <summary>
        /// The item label.
        /// </summary>
        public string Label
        {
            get { return _label; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Sets the label of collection's items.
        /// </summary>
        /// <param name="pLabel">The item label.</param>
        public KnItemLabelAttribute(string pLabel)
        {
            _label = pLabel;
        }
        #endregion
    }
}
