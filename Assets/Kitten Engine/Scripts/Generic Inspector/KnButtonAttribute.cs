﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.IO;
using System;
using UnityEngine;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a button.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = true)]
    public class KnButtonAttribute : KnInvokeMethodBaseAttribute
    {
        #region Fields
        private string _text = null;
        private string _tooltip = null;
        private string _texturePath = null;
        #endregion

        #region Properties
        /// <summary>
        /// The text of the button.
        /// </summary>
        public string Text
        {
            get { return _text; }
        }

        /// <summary>
        /// The tooltip of the button.
        /// </summary>
        public string Tooltip
        {
            get { return _tooltip; }
        }

        /// <summary>
        /// The path of the button's texture.
        /// </summary>
        public string TexturePath
        {
            get { return _texturePath; }
        }
        #endregion

        #region Constructors
        private KnButtonAttribute(string pText, string pTooltip, string pTexturePath, string pClass, string pFunction, bool pIsEditor, E_EditorStateCondition pCallCondition, int pOrder, bool pAllowMultipleTargets, object[] pParameters)
            : base(pClass, pFunction, pIsEditor, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
            _text = pText;
            _tooltip = pTooltip;
            if (!string.IsNullOrEmpty(pTexturePath))
            {
                _texturePath = KnPath.AssetsCombine(KnPath.PATH_ASSETS, pTexturePath);
            }
        }

        private KnButtonAttribute(string pText, string pTooltip, string pTexturePath, string pClass, string pFunction, string pAssembly, E_EditorStateCondition pCallCondition, int pOrder, bool pAllowMultipleTargets, object[] pParameters)
            : base(pClass, pFunction, pAssembly, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
            _text = pText;
            _tooltip = pTooltip;
            if (!string.IsNullOrEmpty(pTexturePath))
            {
                _texturePath = KnPath.AssetsCombine(KnPath.PATH_ASSETS, pTexturePath);
            }
        }

        /// <summary>
        /// Displays a button.
        /// </summary>
        /// <param name="pText">The text of the button.</param>
        /// <param name="pTooltip">The tooltip of the button.</param>
        /// <param name="pClass">If null, calls the method on the target, else call static method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        /// <param name="pIsEditor">Where to get the class (only useful for static methods).</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        public KnButtonAttribute(string pText, string pTooltip, string pClass, string pFunction, object[] pParameters, bool pIsEditor, E_EditorStateCondition pCallCondition = E_EditorStateCondition.All, int pOrder = 0, bool pAllowMultipleTargets = false)
            : this(pText, pTooltip, null, pClass, pFunction, pIsEditor, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
        }

        /// <summary>
        /// Displays a button.
        /// </summary>
        /// <param name="pText">The text of the button.</param>
        /// <param name="pTooltip">The tooltip of the button.</param>
        /// <param name="pClass">If null, calls the method on the target, else call static method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        /// <param name="pAssembly">Where to get the class (only useful for static methods).</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        public KnButtonAttribute(string pText, string pTooltip, string pClass, string pFunction, object[] pParameters, string pAssembly = KnInvokeMethodBaseAttribute.ASSEMBLY_GAME, E_EditorStateCondition pCallCondition = E_EditorStateCondition.All, int pOrder = 0, bool pAllowMultipleTargets = false)
            : this(pText, pTooltip, null, pClass, pFunction, pAssembly, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
        }

        /// <summary>
        /// Displays a button.
        /// </summary>
        /// <param name="pTexturePath">The path of the button's texture. Don't forget the file extension and don't start with Assets!</param>
        /// <param name="pClass">If null, calls the method on the target, else call static method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        /// <param name="pIsEditor">Where to get the class (only useful for static class).</param>
        /// <param name="pCallCondition">The editor state when to call the methods.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        public KnButtonAttribute(string pTexturePath, string pClass, string pFunction, object[] pParameters, bool pIsEditor, E_EditorStateCondition pCallCondition = E_EditorStateCondition.All, int pOrder = 0, bool pAllowMultipleTargets = false)
            : this(null, null, pTexturePath, pClass, pFunction, pIsEditor, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
        }

        /// <summary>
        /// Displays a button.
        /// </summary>
        /// <param name="pTexturePath">The path of the button's texture. Don't forget the file extension and don't start with Assets!</param>
        /// <param name="pClass">If null, calls the method on the target, else call static method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        /// <param name="pAssembly">Where to get the class (only useful for static methods).</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        public KnButtonAttribute(string pTexturePath, string pClass, string pFunction, object[] pParameters, string pAssembly = KnInvokeMethodBaseAttribute.ASSEMBLY_GAME, E_EditorStateCondition pCallCondition = E_EditorStateCondition.All, int pOrder = 0, bool pAllowMultipleTargets = false)
            : this(null, null, pTexturePath, pClass, pFunction, pAssembly, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
        }
        #endregion
    }
}
