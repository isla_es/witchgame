﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a popup to select Scenes that's has been add in the build settings.
    /// Compatible with <see cref="int"/>, <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// Combinable with <see cref="KnReorderableListAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnSceneAttribute : Attribute
    {
    }
}
