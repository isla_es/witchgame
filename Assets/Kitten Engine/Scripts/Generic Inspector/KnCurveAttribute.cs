﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
using UnityEngine;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a curve field with the specified settings.
    /// Compatible with <see cref="AnimationCurve"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// Combinable with <see cref="KnReorderableListAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnCurveAttribute : Attribute
    {
        #region Fields
        private Color _color = Color.white;
        private Rect _ranges = new Rect();
        #endregion

        #region Properties
        /// <summary>
        /// The color to show the curve with. 
        /// </summary>
        public Color Color
        {
            get { return _color; }
        }

        /// <summary>
        /// Optional rectangle that the curve is restrained within.
        /// </summary>
        public Rect Ranges
        {
            get { return _ranges; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a curve field with the specified settings.
        /// Compatible with <see cref="AnimationCurve"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
        /// Combinable with <see cref="KnReorderableListAttribute"/>.
        /// </summary>
        /// <param name="pRed">The red composant of the color. From 0.0f to 255.0f.</param>
        /// <param name="pGreen">The green composant of the color. From 0.0f to 255.0f.</param>
        /// <param name="pBlue">The blue composant of the color. From 0.0f to 255.0f.</param>
        /// <param name="pAlpha">The alpha composant of the color. From 0.0f to 255.0f.</param>
        /// <param name="pLeft">The left coordinate of the rectangle.</param>
        /// <param name="pTop">The top coordinate of the rectangle.</param>
        /// <param name="pWidth">The width of the rectangle.</param>
        /// <param name="pHeight">The height of the rectangle.</param>
        public KnCurveAttribute(float pRed, float pGreen, float pBlue, float pAlpha = 255.0f, float pLeft = 0.0f, float pTop = 0.0f, float pWidth = 1.0f, float pHeight = 1.0f)
        {
            _color = new Color(pRed / 255.0f, pGreen / 255.0f, pBlue / 255.0f, pAlpha / 255.0f);
            _ranges = new Rect(pLeft, pTop, pWidth, pHeight);
        }
        #endregion
    }
}
