﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a toggle group field.
    /// Compatible with <see cref="bool"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnToggleGroupAttribute : Attribute
    {
        #region Fields
        private string[] _toggleGroup = null;
        #endregion

        #region Properties
        /// <summary>
        /// The members in the group. Don't forget to add HideInInspector attribute to these members!
        /// Keywords:
        /// "f:" followed by the name of the target's field.
        /// "p:" followed by the name of the target's property.
        /// </summary>
        public string[] ToggleGroup
        {
            get { return _toggleGroup; }
        }
        #endregion

        #region Contructors
        /// <summary>
        /// Displays a toggle group field.
        /// Compatible with <see cref="bool"/>.
        /// </summary>
        /// <param name="pToggleGroup">
        /// The members in the group. Don't forget to add HideInInspector attribute to these members!
        /// Keywords:
        /// "f:" followed by the name of the target's field.
        /// "p:" followed by the name of the target's property.
        /// </param>
        public KnToggleGroupAttribute(params string[] pToggleGroup)
        {
            _toggleGroup = pToggleGroup;
        }
        #endregion
    }
}
