﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a fixed length array with the given names.
    /// Compatible with array.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnNamedArray : Attribute
    {
        #region Fields
        private string[] _names = null;
        #endregion

        #region Properties
        /// <summary>
        /// The element names.
        /// </summary>
        public string[] Names
        {
            get { return _names; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a fixed length array with the given names.
        /// Compatible with array.
        /// </summary>
        /// <param name="pNames">The element names.</param>
        public KnNamedArray(params string[] pNames)
        {
            _names = pNames;
        }
        #endregion
    }
}
