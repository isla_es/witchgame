﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.IO;
using System;
using System.Collections.Generic;
using System.IO;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Creates a Popup to pick paths.
    /// Compatible with <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// Combinable with <see cref="KnReorderableListAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnAssetPathAttribute : Attribute
    {
        #region Fields
        private string _root = string.Empty;
        private E_PathType _pathType = E_PathType.File;
        private bool _isRecursive = false;
        private E_FilterType _filterType = E_FilterType.None;
        private List<string> _extensions = new List<string>();
        #endregion

        #region Properties
        /// <summary>
        /// The root of the path.
        /// </summary>
        public string Root
        {
            get { return _root; }
        }

        /// <summary>
        /// The type of the paths to build.
        /// </summary>
        public E_PathType PathType
        {
            get { return _pathType; }
        }

        /// <summary>
        /// Is the search recursive.
        /// </summary>
        public bool IsRecursive
        {
            get { return _isRecursive; }
        }

        /// <summary>
        /// The type of filter to apply to the search.
        /// </summary>
        public E_FilterType FilterType
        {
            get { return _filterType; }
        }

        /// <summary>
        /// The extensions to apply to the filter.
        /// </summary>
        public List<string> Extensions
        {
            get { return _extensions; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a Popup to pick paths.
        /// Compatible with <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
        /// Combinable with <see cref="KnReorderableListAttribute"/>.
        /// </summary>
        /// <param name="pRoot">The root of the path. 
        /// Don't specified "Assets". 
        /// If you want to search in Assets directory, leave it null or empty.</param>
        /// <param name="pPathType">The type of path to build.</param>
        /// <param name="pIsRecursive">Is the search recursive ?</param>
        /// <param name="pFilterType">The type of filter to apply to the search.</param>
        /// <param name="pExtensions">The extensions to apply to the filter. Each extension must start with '.'</param>
        public KnAssetPathAttribute(string pRoot, E_PathType pPathType, bool pIsRecursive, E_FilterType pFilterType = E_FilterType.None, params string[] pExtensions)
        {
            if (string.IsNullOrEmpty(pRoot))
            {
                _root = KnPath.PATH_ASSETS;
            }
            else
            {
                _root = KnPath.Combine(Directory.GetCurrentDirectory(), KnPath.PATH_ASSETS, pRoot);
            }
            _pathType = pPathType;
            _isRecursive = pIsRecursive;
            _filterType = pFilterType;
            if (pExtensions != null)
            {
                _extensions.AddRange(pExtensions);
            }
        }
        #endregion
    }
}
