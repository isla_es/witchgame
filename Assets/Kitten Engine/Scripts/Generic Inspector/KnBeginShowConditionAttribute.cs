﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Begins a conditional display of a control group.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnBeginShowConditionAttribute : Attribute
    {
        #region Fields
        private E_EditorStateCondition _showCondition = E_EditorStateCondition.All;
        #endregion

        #region Properties
        /// <summary>
        /// The condition type.
        /// </summary>
        public E_EditorStateCondition ShowCondition
        {
            get { return _showCondition; }
        }
        #endregion

        /// <summary>
        /// Begins a conditional display of a control group.
        /// </summary>
        /// <param name="pShowCondition">The condition type.</param>
        public KnBeginShowConditionAttribute(E_EditorStateCondition pShowCondition)
        {
            _showCondition = pShowCondition;
        }
    }
}
