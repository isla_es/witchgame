﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
using UnityEngine;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// The state of the Unity editor.
    /// </summary>
    public enum E_EditorStateCondition
    {
        /// <summary>
        /// Unity is playing.
        /// </summary>
        Playing,
        /// <summary>
        /// Unity is not playing.
        /// </summary>
        Editing,
        /// <summary>
        /// Unity is either playing or not.
        /// </summary>
        All
    }

    /// <summary>
    /// Base class for all Attributes that deals with reflexive invocation of method.
    /// </summary>
    public class KnInvokeMethodBaseAttribute : Attribute
    {
        #region Constants
        /// <summary>
        /// The Unity Editor script assembly.
        /// </summary>
        public const string ASSEMBLY_EDITOR = "Assembly-CSharp-Editor";

        /// <summary>
        /// The Unity Game script assembly.
        /// </summary>
        public const string ASSEMBLY_GAME = "Assembly-CSharp";
        #endregion

        #region Fields
        private string _class = string.Empty;
        private string _function = string.Empty;
        private string _assembly = string.Empty;
        private E_EditorStateCondition _callCondition = E_EditorStateCondition.All;
        private int _order = 0;
        private bool _allowMultipleTargets = false;
        private object[] _parameters = null;
        #endregion

        #region Properties
        /// <summary>
        /// The class that holds the method.
        /// </summary>
        public string Class
        {
            get { return _class; }
        }

        /// <summary>
        /// The name of the method to call.
        /// </summary>
        public string Function
        {
            get { return _function; }
        }

        /// <summary>
        /// The assembly where to find the class type.
        /// </summary>
        public string Assembly
        {
            get { return _assembly; }
        }

        /// <summary>
        /// The editor state when to call the method.
        /// </summary>
        public E_EditorStateCondition CallCondition
        {
            get { return _callCondition; }
        }

        /// <summary>
        /// The call order. The less values or called before the highest.
        /// </summary>
        public int Order
        {
            get { return _order; }
        }

        /// <summary>
        /// Invoke the method on multiple targets ?
        /// </summary>
        public bool AllowMultipleTargets
        {
            get { return _allowMultipleTargets; }
            set { _allowMultipleTargets = value; }
        }

        /// <summary>
        /// The parameters of the method.
        /// </summary>
        public object[] Parameters
        {
            get { return _parameters; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Base class for all Attributes that deals with reflexive invocation of method.
        /// </summary>
        /// <param name="pClass">The class that holds the method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pIsEditor">Is Unity editor or game assembly ?</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        /// <param name="pParameters">The parameters of the method.</param>
        public KnInvokeMethodBaseAttribute(string pClass, string pFunction, bool pIsEditor, E_EditorStateCondition pCallCondition, int pOrder, bool pAllowMultipleTargets, object[] pParameters)
            : this(pClass, pFunction, pIsEditor ? ASSEMBLY_EDITOR : ASSEMBLY_GAME, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
        }

        /// <summary>
        /// Base class for all Attributes that deals with reflexive invocation of method.
        /// </summary>
        /// <param name="pClass">The class that holds the method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pAssembly">The assembly where to find the class type.</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        /// <param name="pParameters">The parameters of the method.</param>
        public KnInvokeMethodBaseAttribute(string pClass, string pFunction, string pAssembly, E_EditorStateCondition pCallCondition, int pOrder, bool pAllowMultipleTargets, object[] pParameters)
        {
            _class = pClass;
            _function = pFunction;
            _assembly = pAssembly;
            _callCondition = pCallCondition;
            _order = pOrder;
            _allowMultipleTargets = pAllowMultipleTargets;
            _parameters = pParameters;
        }
        #endregion
    }
}
