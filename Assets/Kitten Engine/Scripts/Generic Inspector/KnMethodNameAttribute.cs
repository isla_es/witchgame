﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
using System.Reflection;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a popup to select a method from an object.
    /// Compatible with <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and array.
    /// Combinable with <see cref="KnReorderableListAttribute"/>
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnMethodNameAttribute : Attribute
    {
        #region Fields
        private Type _targetType = null;
        private bool _getInheritedMethods = false;
        #endregion

        #region Properties
        /// <summary>
        /// The type of the object that holds the methods.
        /// </summary>
        public Type TargetType
        {
            get { return _targetType; }
        }

        /// <summary>
        /// Display inherited methods ?
        /// </summary>
        public bool GetInheritedMethods
        {
            get { return _getInheritedMethods; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a popup to select a method from an object.
        /// Compatible with <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and array.
        /// Combinable with <see cref="KnReorderableListAttribute"/>
        /// </summary>
        /// <param name="pTargetType">The type of the object that holds the methods.</param>
        /// <param name="pGetInheritedMethods">Display inherited methods ?</param>
        public KnMethodNameAttribute(Type pTargetType, bool pGetInheritedMethods = false)
        {
            _targetType = pTargetType;
            _getInheritedMethods = pGetInheritedMethods;
        }
        #endregion
    }
}
