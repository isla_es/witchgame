﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a Text Area.
    /// Compatible with <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and array.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnTextAreaAttribute : Attribute
    {
        #region Fields
        private int _maxLines = 3;
        #endregion

        #region Properties
        /// <summary>
        /// The maximum number of lines.
        /// </summary>
        public int MaxLines
        {
            get { return _maxLines; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a Text Area.
        /// Compatible with <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and array.
        /// </summary>
        /// <param name="pMaxLines">The maximum number of lines.</param>
        public KnTextAreaAttribute(int pMaxLines = 3)
        {
            _maxLines = pMaxLines;
        }
        #endregion
    }
}
