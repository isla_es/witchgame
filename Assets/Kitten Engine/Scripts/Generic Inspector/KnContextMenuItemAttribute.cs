﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a context menu strip item.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = true)]
    public class KnContextMenuItemAttribute : KnInvokeMethodBaseAttribute
    {
        #region Fields
        private string _text = string.Empty;
        private object _target = null;
        #endregion

        #region Properties
        /// <summary>
        /// The item text.
        /// </summary>
        public string Text
        {
            get { return _text; }
            set { _text = value; }
        }

        /// <summary>
        /// The object that holds the context menu.
        /// </summary>
        public object Target
        {
            get { return _target; }
            set { _target = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a context menu strip item.
        /// </summary>
        /// <param name="pText">The item text.</param>
        /// <param name="pClass">If null, calls the method on the target, else call static method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        /// <param name="pIsEditor">Where to get the class (only useful for static methods).</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        public KnContextMenuItemAttribute(string pText, string pClass, string pFunction, object[] pParameters, bool pIsEditor, E_EditorStateCondition pCallCondition = E_EditorStateCondition.All, int pOrder = 0, bool pAllowMultipleTargets = false)
            : base(pClass, pFunction, pIsEditor, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
            _text = pText;
        }

        /// <summary>
        /// Displays a context menu strip item.
        /// </summary>
        /// <param name="pText">The item text.</param>
        /// <param name="pClass">If null, calls the method on the target, else call static method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        /// <param name="pAssembly">Where to get the class (only useful for static methods).</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        public KnContextMenuItemAttribute(string pText, string pClass, string pFunction, object[] pParameters, string pAssembly = KnInvokeMethodBaseAttribute.ASSEMBLY_GAME, E_EditorStateCondition pCallCondition = E_EditorStateCondition.All, int pOrder = 0, bool pAllowMultipleTargets = false)
            : base(pClass, pFunction, pAssembly, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
            _text = pText;
        }
        #endregion
    }
}
