﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
using System.Collections.Generic;
using UnityEngine;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a popup with pairs of displayed string and an int value.
    /// Compatible with <see cref="int"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// Combinable with <see cref="KnReorderableListAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnIntPopupAttribute : Attribute
    {
        #region Fields
        private GUIContent[] _displayedOptions = null;
        private int[] _options = null;
        #endregion

        #region Properties
        /// <summary>
        /// The options to display.
        /// </summary>
        public GUIContent[] DisplayedOptions
        {
            get { return _displayedOptions; }
        }

        /// <summary>
        /// The values associated to the options.
        /// </summary>
        public int[] Options
        {
            get { return _options; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a popup with pairs of displayed string and an int value.
        /// Compatible with <see cref="int"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
        /// Combinable with <see cref="KnReorderableListAttribute"/>.
        /// </summary>
        /// <param name="pOptions">
        /// The params to display. Declare it by pair: displayed string and an int value.
        /// Example: "One", 1, "Two", two, ...
        /// </param>
        public KnIntPopupAttribute(params object[] pOptions)
        {
            List<GUIContent> displayedOptions = new List<GUIContent>();
            List<int> options = new List<int>();


            for (int i = 0; i < pOptions.Length; i += 2)
            {
                displayedOptions.Add(new GUIContent(pOptions[i] as string));
                options.Add((int)pOptions[i + 1]);
            }

            _displayedOptions = displayedOptions.ToArray();
            displayedOptions.Clear();

            _options = options.ToArray();
            options.Clear();
        }
        #endregion
    }
}