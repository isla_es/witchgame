﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// The message type.
    /// </summary>
    public enum E_MessageType
    {
        /// <summary>
        /// No type.
        /// </summary>
        None = 0,
        /// <summary>
        /// Informative message.
        /// </summary>
        Info = 1,
        /// <summary>
        /// Warning message.
        /// </summary>
        Warning = 2,
        /// <summary>
        /// Error message.
        /// </summary>
        Error = 3,
    }

    /// <summary>
    /// Displays a help box below the member.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = true)]
    public class KnHelpBoxAttribute : Attribute
    {
        #region Fields
        private string _message = string.Empty;
        private E_MessageType _messageType = E_MessageType.None;
        private int _order = 0;
        #endregion

        #region Properties
        /// <summary>
        /// The message to display.
        /// </summary>
        public string Message
        {
            get { return _message; }
        }

        /// <summary>
        /// The type of the message to display.
        /// </summary>
        public E_MessageType MessageType
        {
            get { return _messageType; }
        }

        /// <summary>
        /// If there is more than 1 help box, specifies the display order.
        /// </summary>
        public int Order
        {
            get { return _order; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a help box below the member.
        /// </summary>
        /// <param name="pMessage">The message to display.</param>
        /// <param name="pMessageType">The type of the message to display.</param>
        /// <param name="pOrder">If there is more than 1 help box, specifies the display order.</param>
        public KnHelpBoxAttribute(string pMessage, E_MessageType pMessageType = E_MessageType.None, int pOrder = 0)
        {
            _message = pMessage;
            _messageType = pMessageType;
            _order = pOrder;
        }
        #endregion
    }
}
