﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a UnityObject field.
    /// Compatible with <see cref="UnityEngine.Object"/>, <see cref="System.Collections.Generic.List{T}"/> and array.
    /// Combinable with <see cref="KnReorderableListAttribute"/> but will force IsEditable property to false.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnObjectFieldAttribute : Attribute
    {
        #region Fields
        private bool _allowSceneObjects = false;
        private bool _isEditable = false;
        #endregion

        #region Properties
        /// <summary>
        /// Allow assigning scene objects ?
        /// </summary>
        public bool AllowSceneObjects
        {
            get { return _allowSceneObjects; }
        }

        /// <summary>
        /// Is editable directly in the object member holder inspector ?
        /// </summary>
        public bool IsEditable
        {
            get { return _isEditable; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a UnityObject field.
        /// Compatible with <see cref="UnityEngine.Object"/>, <see cref="System.Collections.Generic.List{T}"/> and array.
        /// Combinable with <see cref="KnReorderableListAttribute"/> but will force IsEditable property to false.
        /// </summary>
        /// <param name="pAllowSceneObjects">Allow assigning scene objects ?</param>
        /// <param name="pIsEditable">Is editable directly in the object member holder inspector ?</param>
        public KnObjectFieldAttribute(bool pAllowSceneObjects = true, bool pIsEditable = false)
        {
            _allowSceneObjects = pAllowSceneObjects;
            _isEditable = pIsEditable;
        }
        #endregion
    }
}
