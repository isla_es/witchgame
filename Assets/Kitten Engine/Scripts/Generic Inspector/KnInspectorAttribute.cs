﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.EditorUtils;
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Provides all Attributes to create easily and quickly inspector display without using a custom inspector.
    /// </summary>
    internal class NamespaceDoc { }

    /// <summary>
    /// How to display titles in the inspector.
    /// </summary>
    public enum E_TitleDisplay
    {
        /// <summary>
        /// Don't display the titles.
        /// </summary>
        None,
        /// <summary>
        /// Only the members types: Fields or Properties.
        /// </summary>
        Members,
        /// <summary>
        /// Only display class name.
        /// </summary>
        Name,
        /// <summary>
        /// Display class name with namespace.
        /// </summary>
        FullName
    }

    /// <summary>
    /// Uses de generic custom inspector.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class KnInspectorAttribute : Attribute
    {
        #region Fields
        private bool _exposeFields = false;
        private bool _exposeProperties = false;
        private bool _showFoldout = true;
        private E_TitleDisplay _titleDisplay = E_TitleDisplay.None;
        private E_DisplayType _defaultDisplayType = E_DisplayType.Normal;
        private bool _showScript = true;
        #endregion

        #region Properties
        /// <summary>
        /// Does expose fields ?
        /// </summary>
        public bool ExposeFields
        {
            get { return _exposeFields; }
        }

        /// <summary>
        /// Does expost properties ?
        /// </summary>
        public bool ExposeProperties
        {
            get { return _exposeProperties; }
        }

        /// <summary>
        /// Shows a foldout for objects inside another inspector.
        /// </summary>
        public bool ShowFoldout
        {
            get { return _showFoldout; }
        }

        /// <summary>
        /// How to display titles.
        /// </summary>
        public E_TitleDisplay TitleDisplay
        {
            get { return _titleDisplay; }
        }

        /// <summary>
        /// Default display type for values.
        /// </summary>
        public E_DisplayType DefaultDisplayType
        {
            get { return _defaultDisplayType; }
        }

        /// <summary>
        /// Shows script field ?
        /// </summary>
        public bool ShowScript
        {
            get { return _showScript; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Uses de generic custom inspector.
        /// </summary>
        /// <param name="pExposeFields">Does expose fields ?</param>
        /// <param name="pExposeProperties">Does expost properties ?</param>
        /// <param name="pShowFoldout">Shows a foldout for objects inside another inspector.</param>
        /// <param name="ptitleDisplay">How to display titles.</param>
        /// <param name="pDefaultDisplayType">Default display type for values.</param>
        /// <param name="pShowScript">Shows script field ?</param>
        public KnInspectorAttribute(bool pExposeFields = true, bool pExposeProperties = false, bool pShowFoldout = true, E_TitleDisplay ptitleDisplay = E_TitleDisplay.None, E_DisplayType pDefaultDisplayType = E_DisplayType.Normal, bool pShowScript = true)
        {
            _exposeFields = pExposeFields;
            _exposeProperties = pExposeProperties;
            _showFoldout = pShowFoldout;
            _titleDisplay = ptitleDisplay;
            _defaultDisplayType = pDefaultDisplayType;
            _showScript = pShowScript;
        }
        #endregion
    }
}
