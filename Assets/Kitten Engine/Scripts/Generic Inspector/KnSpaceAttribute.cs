﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a space before the member.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnSpaceAttribute : Attribute
    {
        #region Fields
        private float _height = 0;
        #endregion

        #region Properties
        /// <summary>
        /// The size of the space.
        /// </summary>
        public float Height
        {
            get { return _height; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a space before the member.
        /// </summary>
        /// <param name="pHeight">The size of the space.</param>
        public KnSpaceAttribute(float pHeight = 12.0f)
        {
            _height = pHeight;
        }
        #endregion
    }
}
