﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a no resiazable array.
    /// Compatible with array.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnFixedLengthArray : Attribute
    {
        #region Fields
        private int _length = 0;
        #endregion

        #region Properties
        /// <summary>
        /// The length of the array.
        /// </summary>
        public int Length
        {
            get { return _length; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a no resiazable array.
        /// Compatible with array.
        /// </summary>
        /// <param name="pLength">The length of the array.</param>
        public KnFixedLengthArray(int pLength)
        {
            _length = pLength;
        }
        #endregion
    }
}
