﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a MinMaxSlider field.
    /// Compatible with <see cref="UnityEngine.Vector2"/>, <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// Combinable with <see cref="KnReorderableListAttribute"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnMinMaxSliderAttribute : Attribute
    {
        #region Fields
        private float _minLimit = 0.0f;
        private float _maxLimit = 0.0f;
        private bool _showMinMax = true;
        #endregion

        #region Properties
        /// <summary>
        /// The limit at the left end of the slider.
        /// </summary>
        public float MinLimit
        {
            get { return _minLimit; }
        }

        /// <summary>
        /// The limit at the right end of the slider.
        /// </summary>
        public float MaxLimit
        {
            get { return _maxLimit; }
        }

        /// <summary>
        /// Does show the min and max limit in the inspector ?
        /// </summary>
        public bool ShowMinMax
        {
            get { return _showMinMax; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a MinMaxSlider field.
        /// Compatible with <see cref="UnityEngine.Vector2"/>, <see cref="string"/>, <see cref="System.Collections.Generic.List{T}"/> and arrays.
        /// Combinable with <see cref="KnReorderableListAttribute"/>.
        /// </summary>
        /// <param name="pMinLimit">The limit at the left end of the slider.</param>
        /// <param name="pMaxLimit">The limit at the right end of the slider.</param>
        /// <param name="pShowMinMax">Does show the min and max limit in the inspector ?</param>
        public KnMinMaxSliderAttribute(float pMinLimit, float pMaxLimit, bool pShowMinMax = false)
        {
            _minLimit = pMinLimit;
            _maxLimit = pMaxLimit;
            _showMinMax = pShowMinMax;
        }
        #endregion
    }
}
