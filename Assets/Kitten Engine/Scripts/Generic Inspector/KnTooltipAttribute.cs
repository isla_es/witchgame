﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a tooltip for the member.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnTooltipAttribute : Attribute
    {
        #region Fields
        private string _tooltip = string.Empty;
        #endregion

        #region Properties
        /// <summary>
        /// The tooltip text.
        /// </summary>
        public string Tooltip
        {
            get { return _tooltip; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a tooltip for the member.
        /// </summary>
        /// <param name="pToolTip">The tooltip text.</param>
        public KnTooltipAttribute(string pToolTip)
        {
            _tooltip = pToolTip;
        }
        #endregion
    }
}