﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Calls a method.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class KnMethodAttribute : KnInvokeMethodBaseAttribute
    {
        #region Constructors
        /// <summary>
        /// Calls a method.
        /// </summary>
        /// <param name="pClass">The class that holds the method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "event" if you want to send the current event.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        /// <param name="pIsEditor">Where to get the class (only useful for static methods).</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        public KnMethodAttribute(string pClass, string pFunction, object[] pParameters, bool pIsEditor, E_EditorStateCondition pCallCondition = E_EditorStateCondition.All, int pOrder = 0, bool pAllowMultipleTargets = false)
            : base(pClass, pFunction, pIsEditor, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
        }

        /// <summary>
        /// Calls a method.
        /// </summary>
        /// <param name="pClass">The class that holds the method.</param>
        /// <param name="pFunction">The name of the method to call.</param>
        /// <param name="pParameters">
        /// The parameters to send to the method.
        /// Keywords:
        /// "self" if you want to send the target.
        /// "event" if you want to send the current event.
        /// "f:" followed by the name of the target's field you want to send.
        /// "p:" followed by the name of the target's property you want to send.
        /// </param>
        /// <param name="pAssembly">Where to get the class (only useful for static methods).</param>
        /// <param name="pCallCondition">The editor state when to call the method.</param>
        /// <param name="pOrder">The call order. The less values or called before the highest.</param>
        /// <param name="pAllowMultipleTargets">Invoke the method on multiple targets ?</param>
        public KnMethodAttribute(string pClass, string pFunction, object[] pParameters, string pAssembly = KnInvokeMethodBaseAttribute.ASSEMBLY_GAME, E_EditorStateCondition pCallCondition = E_EditorStateCondition.All, int pOrder = 0, bool pAllowMultipleTargets = false)
            : base(pClass, pFunction, pAssembly, pCallCondition, pOrder, pAllowMultipleTargets, pParameters)
        {
        }
        #endregion
    }
}
