﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Indent with the given amount.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnIndentAttribute : Attribute
    {
        #region Fields
        private int _amount = 0;
        #endregion

        #region Properties
        /// <summary>
        /// The amount to indent.
        /// </summary>
        public int Amount
        {
            get { return _amount; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Indent with the given amount.
        /// </summary>
        /// <param name="pAmount">The amount to indent.</param>
        public KnIndentAttribute(int pAmount = 1)
        {
            _amount = pAmount;
        }
        #endregion
    }
}