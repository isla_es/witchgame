﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays an enum mask field.
    /// Compatible with enums.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnEnumMaskAttribute : Attribute
    {
    }
}
