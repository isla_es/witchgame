﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Draws a custom preview.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class KnPreviewAttribute : Attribute
    {
        #region Fields
        private string _title = null;
        private string _class = null;
        private string _drawPreviewFunction = null;
        private string _onPreviewSettingsFunction = null;
        #endregion

        #region Properties
        /// <summary>
        /// The preview window title.
        /// </summary>
        public string Title
        {
            get { return _title; }
        }

        /// <summary>
        /// The editor class that holds the methods to call. If null, it will call target object methods instead.
        /// </summary>
        public string Class
        {
            get { return _class; }
        }

        /// <summary>
        /// The DrawPreview method to call. 
        /// The method signature must be: DrawPreview(<see cref="UnityEngine.Rect"/>, <see cref="UnityEngine.Object"/>).
        /// </summary>
        public string DrawPreviewFunction
        {
            get { return _drawPreviewFunction; }
        }

        /// <summary>
        /// The OnPreviewSettings method to call if you want to show custom controls in the preview header. 
        /// The method signature must be: OnPreviewSettings(<see cref="UnityEngine.Object"/>).
        /// </summary>
        public string OnPreviewSettingsFunction
        {
            get { return _onPreviewSettingsFunction; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Draws a custom preview.
        /// </summary>
        /// <param name="pTitle">The preview window title.</param>
        /// <param name="pClass">The editor class that holds the methods to call. If null, it will call target object methods instead.</param>
        /// <param name="pDrawPreviewFunction">
        /// The DrawPreview method to call. 
        /// The method signature must be: DrawPreview(<see cref="UnityEngine.Rect"/>, <see cref="UnityEngine.Object"/>).
        /// </param>
        /// <param name="pOnPreviewSettingsFunction">
        /// The OnPreviewSettings method to call if you want to show custom controls in the preview header. 
        /// The method signature must be: OnPreviewSettings(<see cref="UnityEngine.Object"/>).
        /// </param>
        public KnPreviewAttribute(string pTitle, string pClass = null, string pDrawPreviewFunction = null, string pOnPreviewSettingsFunction = null)
        {
            _title = pTitle;
            _class = pClass;
            _drawPreviewFunction = pDrawPreviewFunction;
            _onPreviewSettingsFunction = pOnPreviewSettingsFunction;
        }
        #endregion
    }
}
