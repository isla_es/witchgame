﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
#endregion

namespace KittenEngine.GenericInspector
{
    /// <summary>
    /// Displays a collection regarding to an enum. Each index will correspond to the enum values.
    /// Compatible with <see cref="System.Collections.Generic.List{T}"/> and arrays.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class KnEnumCollectionAttribute : Attribute
    {
        #region Fields
        private Type _enumType = null;
        #endregion

        #region Properties
        /// <summary>
        /// The type of enum to display.
        /// </summary>
        public Type EnumType
        {
            get { return _enumType; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Displays a collection regarding to an enum. Each index will correspond to the enum values.
        /// Compatible with <see cref="System.Collections.Generic.List{T}"/> and arrays.
        /// </summary>
        /// <param name="pEnumType">The type of enum to display.</param>
        public KnEnumCollectionAttribute(Type pEnumType)
        {
            _enumType = pEnumType;
        }
        #endregion
    }
}
