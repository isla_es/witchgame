﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

namespace KittenEngine.EditorUtils
{
    /// <summary>
    /// Provides usefull methods when working with UnityEditor.
    /// </summary>
    internal class NamespaceDoc { }

    /// <summary>
    /// The type of display for inspector values
    /// </summary>
    public enum E_DisplayType
    {
        /// <summary>
        /// Unity 3d display
        /// </summary>
        Normal,
        /// <summary>
        /// Fit the value in the space it needs
        /// </summary>
        Fitted
    }
}
