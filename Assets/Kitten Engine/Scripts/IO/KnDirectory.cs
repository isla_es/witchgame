﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System.Collections.Generic;
using System.IO;
using UnityEngine;
#endregion

namespace KittenEngine.IO
{
    #region Enums
    /// <summary>
    /// Indicates which files to get (never get .meta files).
    /// </summary>
    public enum E_FilterType
    {
        /// <summary>
        /// Get all the files.
        /// </summary>
        None,
        /// <summary>
        /// Get only files with the given extensions.
        /// </summary>
        Include,
        /// <summary>
        /// Get only files without the given extensions.
        /// </summary>
        Exclude
    }
    #endregion

    /// <summary>
    /// Gets files with specified <see cref="E_FilterType">filter type</see> and extensions.
    /// </summary>
    public static class KnDirectory
    {
        #region Methods
        /// <summary>
        /// Fills all the files exept the .meta files.
        /// </summary>
        /// <param name="pRoot">The root path directory.</param>
        /// <param name="pPathType">Important when looking for the files.</param>
        /// <param name="pIsRecursive">Indicates if the search includes sub directories.</param>
        /// <param name="rpFileNames">The list of files names to fill.</param>
        /// <param name="rpFullPaths">The list of full file paths to fill.</param>
        public static void GetFiles(string pRoot, E_PathType pPathType, bool pIsRecursive, ref List<string> rpFileNames, ref List<string> rpFullPaths)
        {
            string[] files = Directory.GetFiles(pRoot);
            foreach (string iFile in files)
            {
                string fileName = Path.GetFileNameWithoutExtension(iFile);
                string extension = Path.GetExtension(iFile);
                if (!extension.Equals(".meta"))
                {
                    rpFileNames.Add(string.Format("{0} ({1})", fileName, extension));
                    string filePath = KnPath.GetPath(iFile, pPathType);
                    rpFullPaths.Add(filePath);
                }
            }

            if (pIsRecursive)
            {
                string[] subDirectories = Directory.GetDirectories(pRoot);
                foreach (string iSubDirectory in subDirectories)
                {
                    GetFiles(iSubDirectory, pPathType, pIsRecursive, ref rpFileNames, ref rpFullPaths);
                }
            }
        }

        /// <summary>
        /// Fills the files with the given extensions exept the .meta files.
        /// </summary>
        /// <param name="pRoot">The root path directory.</param>
        /// <param name="pPathType">Important when looking for the files.</param>
        /// <param name="pExtensions">The included file extensions.</param>
        /// <param name="pIsRecursive">Indicates if the search includes sub directories.</param>
        /// <param name="rpFileNames">The list of files names to fill.</param>
        /// <param name="rpFullPaths">The list of full file paths to fill.</param>
        public static void GetFilesWithExtension(string pRoot, E_PathType pPathType, List<string> pExtensions, bool pIsRecursive, ref List<string> rpFileNames, ref List<string> rpFullPaths)
        {
            string[] files = Directory.GetFiles(pRoot);
            foreach (string iFile in files)
            {
                string fileName = Path.GetFileNameWithoutExtension(iFile);
                string extension = Path.GetExtension(iFile);
                if (!extension.Equals(".meta") && pExtensions.Contains(extension))
                {
                    rpFileNames.Add(string.Format("{0} ({1})", fileName, extension));
                    string filePath = KnPath.GetPath(iFile, pPathType);
                    rpFullPaths.Add(filePath);
                }
            }

            if (pIsRecursive)
            {
                string[] subDirectories = Directory.GetDirectories(pRoot);
                foreach (string iSubDirectory in subDirectories)
                {
                    GetFilesWithExtension(iSubDirectory, pPathType, pExtensions, pIsRecursive, ref rpFileNames, ref rpFullPaths);
                }
            }
        }

        /// <summary>
        /// Fills the files without the given extensions exept the .meta files.
        /// </summary>
        /// <param name="pRoot">The root path directory.</param>
        /// <param name="pPathType">Important when looking for the files.</param>
        /// <param name="pExtensions">The excluded file extensions.</param>
        /// <param name="pIsRecursive">Indicates if the search includes sub directories.</param>
        /// <param name="rpFileNames">The list of files names to fill.</param>
        /// <param name="rpFullPaths">The list of full file paths to fill.</param>
        public static void GetFilesWithoutExtension(string pRoot, E_PathType pPathType, List<string> pExtensions, bool pIsRecursive, ref List<string> rpFileNames, ref List<string> rpFullPaths)
        {
            string[] files = Directory.GetFiles(pRoot);
            foreach (string iFile in files)
            {
                string fileName = Path.GetFileNameWithoutExtension(iFile);
                string extension = Path.GetExtension(iFile);
                if (!extension.Equals(".meta") && !pExtensions.Contains(extension))
                {
                    rpFileNames.Add(string.Format("{0} ({1})", fileName, extension));
                    string filePath = KnPath.GetPath(iFile, pPathType);
                    rpFullPaths.Add(filePath);
                }
            }

            if (pIsRecursive)
            {
                string[] subDirectories = Directory.GetDirectories(pRoot);
                foreach (string iSubDirectory in subDirectories)
                {
                    GetFilesWithoutExtension(iSubDirectory, pPathType, pExtensions, pIsRecursive, ref rpFileNames, ref rpFullPaths);
                }
            }
        }

        /// <summary>
        /// Fills all the files exept the .meta files.
        /// </summary>
        /// <param name="pRoot">The root path directory.</param>
        /// <param name="pPathType">Important when looking for the files.</param>
        /// <param name="pIsRecursive">Indicates if the search includes sub directories.</param>
        /// <param name="rpFileNames">The list of files names to fill.</param>
        /// <param name="rpFullPaths">The list of full file paths to fill.</param>
        public static void GetFiles(string pRoot, E_PathType pPathType, bool pIsRecursive, ref List<GUIContent> rpFileNames, ref List<GUIContent> rpFullPaths)
        {
            string[] files = Directory.GetFiles(pRoot);
            foreach (string iFile in files)
            {
                string fileName = Path.GetFileNameWithoutExtension(iFile);
                string extension = Path.GetExtension(iFile);
                if (!extension.Equals(".meta"))
                {
                    rpFileNames.Add(new GUIContent(string.Format("{0} ({1})", fileName, extension)));
                    string filePath = KnPath.GetPath(iFile, pPathType);
                    rpFullPaths.Add(new GUIContent(filePath));
                }
            }

            if (pIsRecursive)
            {
                string[] subDirectories = Directory.GetDirectories(pRoot);
                foreach (string iSubDirectory in subDirectories)
                {
                    GetFiles(iSubDirectory, pPathType, pIsRecursive, ref rpFileNames, ref rpFullPaths);
                }
            }
        }

        /// <summary>
        /// Fills the files with the given extensions exept the .meta files.
        /// </summary>
        /// <param name="pRoot">The root path directory.</param>
        /// <param name="pPathType">Important when looking for the files.</param>
        /// <param name="pExtensions">The included file extensions.</param>
        /// <param name="pIsRecursive">Indicates if the search includes sub directories.</param>
        /// <param name="rpFileNames">The list of files names to fill.</param>
        /// <param name="rpFullPaths">The list of full file paths to fill.</param>
        public static void GetFilesWithExtension(string pRoot, E_PathType pPathType, List<string> pExtensions, bool pIsRecursive, ref List<GUIContent> rpFileNames, ref List<GUIContent> rpFullPaths)
        {
            string[] files = Directory.GetFiles(pRoot);
            foreach (string iFile in files)
            {
                string fileName = Path.GetFileNameWithoutExtension(iFile);
                string extension = Path.GetExtension(iFile);
                if (!extension.Equals(".meta") && pExtensions.Contains(extension))
                {
                    rpFileNames.Add(new GUIContent(string.Format("{0} ({1})", fileName, extension)));
                    string filePath = KnPath.GetPath(iFile, pPathType);
                    rpFullPaths.Add(new GUIContent(filePath));
                }
            }

            if (pIsRecursive)
            {
                string[] subDirectories = Directory.GetDirectories(pRoot);
                foreach (string iSubDirectory in subDirectories)
                {
                    GetFilesWithExtension(iSubDirectory, pPathType, pExtensions, pIsRecursive, ref rpFileNames, ref rpFullPaths);
                }
            }
        }

        /// <summary>
        /// Fills the files without the given extensions exept the .meta files.
        /// </summary>
        /// <param name="pRoot">The root path directory.</param>
        /// <param name="pPathType">Important when looking for the files.</param>
        /// <param name="pExtensions">The excluded file extensions.</param>
        /// <param name="pIsRecursive">Indicates if the search includes sub directories.</param>
        /// <param name="rpFileNames">The list of files names to fill.</param>
        /// <param name="rpFullPaths">The list of full file paths to fill.</param>
        public static void GetFilesWithoutExtension(string pRoot, E_PathType pPathType, List<string> pExtensions, bool pIsRecursive, ref List<GUIContent> rpFileNames, ref List<GUIContent> rpFullPaths)
        {
            string[] files = Directory.GetFiles(pRoot);
            foreach (string iFile in files)
            {
                string fileName = Path.GetFileNameWithoutExtension(iFile);
                string extension = Path.GetExtension(iFile);
                if (!extension.Equals(".meta") && !pExtensions.Contains(extension))
                {
                    rpFileNames.Add(new GUIContent(string.Format("{0} ({1})", fileName, extension)));
                    string filePath = KnPath.GetPath(iFile, pPathType);
                    rpFullPaths.Add(new GUIContent(filePath));
                }
            }

            if (pIsRecursive)
            {
                string[] subDirectories = Directory.GetDirectories(pRoot);
                foreach (string iSubDirectory in subDirectories)
                {
                    GetFilesWithoutExtension(iSubDirectory, pPathType, pExtensions, pIsRecursive, ref rpFileNames, ref rpFullPaths);
                }
            }
        }
        #endregion
    }
}
