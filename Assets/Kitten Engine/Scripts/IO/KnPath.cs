/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat � 2012-2014
 * Author : Timoth�e Verrouil
 * *******************************************************************************************************************/

#region Imports
using System.IO;
using UnityEngine;
#endregion

namespace KittenEngine.IO
{
    /// <summary>
    /// Provides uniform methods to deal with IO stuff.
    /// </summary>
    internal class NamespaceDoc { }

    /// <summary>
    /// Helps when working with paths.
    /// </summary>
    public enum E_PathType
    {
        /// <summary>
        /// Classic C# file path.
        /// </summary>
        File,
        /// <summary>
        /// Unity Assets path.
        /// </summary>
        Asset,
        /// <summary>
        /// Unity Resources path.
        /// </summary>
        Resource
    }

    /// <summary>
    /// Provides methods to manipulate paths
    /// </summary>
    public static class KnPath
    {
        /// <summary>
        /// Path for Assets Directory.
        /// </summary>
        public const string PATH_ASSETS = "Assets";

        /// <summary>
        /// Path for Resources Directory.
        /// </summary>
        public const string PATH_RESOURCES = "Resources";

        /// <summary>
        /// The Length of string Resources path.
        /// </summary>
        public static readonly int LENGTH_PATH_RESOURCES = (PATH_RESOURCES + "/").Length;

        #region Methods
        /// <summary>
        /// Combines a list of paths.
        /// </summary>
        /// <param name="pPaths">The paths to combine.</param>
        /// <returns>The combined path.</returns>
        public static string Combine(params string[] pPaths)
        {
            string results = null;
            if (pPaths != null)
            {
                results = pPaths[0];
                for (int i = 1; i < pPaths.Length; i++)
                {
                    results = Path.Combine(results, pPaths[i]);
                }
            }
            return results;
        }

        /// <summary>
        /// Combines a list of paths for Unity assets paths.
        /// </summary>
        /// <param name="pPaths">The paths to combine.</param>
        /// <returns>The combined path.</returns>
        public static string AssetsCombine(params string[] pPaths)
        {
            string results = null;
            if (pPaths != null)
            {
                results = pPaths[0];
                for (int i = 1; i < pPaths.Length; i++)
                {
                    results += "/" + pPaths[i];
                }
            }
            return results;
        }

        /// <summary>
        /// Creates a unique file name base on the given original file name.
        /// </summary>
        /// <param name="pFile">The original file name.</param>
        /// <returns>The unique file name.</returns>
        public static string CreateName(string pFile)
        {
            string fileName = Path.GetFileNameWithoutExtension(pFile);
            string extension = Path.GetExtension(pFile);
            string directory = Path.GetDirectoryName(pFile);
            int index = 1;
            string newName = Path.Combine(directory, fileName + " (" + index.ToString() + ")" + extension);
            while (KnFile.Exists(newName))
            {
                index++;
                newName = Path.Combine(directory, fileName + " (" + index.ToString() + ")" + extension);
            }
            return newName;
        }

        /// <summary>
        /// Returns a converted asset path regarding to the given path.
        /// </summary>
        /// <param name="pPath">The path to convert.</param>
        /// <returns>The converted path.</returns>
        public static string ConvertToAssetPath(string pPath)
        {
            string result = pPath.Replace(Directory.GetCurrentDirectory(), string.Empty);
            result = result.Replace('\\', '/');
            if (result.StartsWith("/"))
            {
                result = result.Substring(1);
            }
            return result;
        }

        /// <summary>
        /// Returns a converted resource path regarding to the given path.
        /// </summary>
        /// <param name="pPath">The path to convert.</param>
        /// <returns>The converted Path.</returns>
        public static string ConvertToResourcePath(string pPath)
        {
            if (pPath.Contains(PATH_RESOURCES))
            {
                int index = pPath.LastIndexOf(PATH_RESOURCES);
                string result = pPath.Substring(index + LENGTH_PATH_RESOURCES);
                result = result.Replace('\\', '/');
                index = result.LastIndexOf('.');
                return result.Substring(0, index);
            }
            else
            {
                Debug.LogWarning("This is not a Resources Path !");
                return pPath;
            }
        }

        /// <summary>
        /// Returns the correct path regarding to the given path and its type.
        /// </summary>
        /// <param name="pPath">The original path.</param>
        /// <param name="pPathType">The type of paths to return.</param>
        /// <returns>The correct typed path.</returns>
        public static string GetPath(string pPath, E_PathType pPathType)
        {
            switch (pPathType)
            {
                case (E_PathType.File):
                    {
                        return pPath;
                    }
                case (E_PathType.Asset):
                    {
                        return KnPath.ConvertToAssetPath(pPath);
                    }
                case (E_PathType.Resource):
                    {
                        return KnPath.ConvertToResourcePath(pPath);
                    }
            }
            return null;
        }
        #endregion
    }
}
