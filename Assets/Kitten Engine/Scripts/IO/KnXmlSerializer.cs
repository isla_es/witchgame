/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat � 2012-2014
 * Author : Timoth�e Verrouil
 * *******************************************************************************************************************/

#region Imports
using System;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
#endregion

namespace KittenEngine.IO
{
    /// <summary>
    /// Manages Xml object serialization.
    /// </summary>
    public class KnXmlSerializer
    {
        /// <summary>
        /// Serializes the object to Xml to the specified stream.
        /// </summary>
        /// <typeparam name="T">The type of the serialized object.</typeparam>
        /// <param name="pStream">The Stream where to serialize.</param>
        /// <param name="pObject">The object to serialize.</param>
        /// <param name="pIncludeTypes">The included types.</param>
        public static void Serialize<T>(Stream pStream, T pObject, Type[] pIncludeTypes)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T), pIncludeTypes);
            serializer.Serialize(pStream, pObject);
        }

        /// <summary>
        /// Serializes the object to Xml to the specified path.
        /// </summary>
        /// <typeparam name="T">The type of the serialized object.</typeparam>
        /// <param name="pPath">The path where to serialize the object.</param>
        /// <param name="pObject">The object to serialize.</param>
        /// <param name="pIncludeTypes">The included types.</param>
        public static void Serialize<T>(string pPath, T pObject, Type[] pIncludeTypes)
        {
            Stream stream = null;
            try
            {
                stream = KnFile.Open(pPath);
                Serialize<T>(stream, pObject, pIncludeTypes);
            }
            catch (Exception exception)
            {
                Debug.LogError(exception);
            }
            finally
            {
                if (stream != null)
                {
#if UNITY_METRO && !UNITY_EDITOR
                    stream.Flush();
#else
                    stream.Close();
#endif
                }
            }
        }

        /// <summary>
        /// Deserializes the Xml object from the specified stream.
        /// </summary>
        /// <typeparam name="T">The type of the deserialized object.</typeparam>
        /// <param name="pStream">The Stream where to deserialize.</param>
        /// <param name="pIncludeTypes">The included types.</param>
        /// <returns>The deserialized object.</returns>
        public static T Deserialize<T>(Stream pStream, Type[] pIncludeTypes)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T), pIncludeTypes);
            return (T)serializer.Deserialize(pStream);
        }

        /// <summary>
        /// Deserializes the Xml object from the specified path.
        /// </summary>
        /// <typeparam name="T">The type of the deserialized object.</typeparam>
        /// <param name="pPath">The path where to deserialize.</param>
        /// <param name="pIncludeTypes">The included types.</param>
        /// <returns>The deserialized object.</returns>
        public static T Deserialize<T>(string pPath, Type[] pIncludeTypes)
        {
            Stream stream = null;
            T result = default(T);
            try
            {
                return Deserialize<T>(stream, pIncludeTypes);
            }
            catch (Exception exception)
            {
                Debug.LogError(exception);
            }
            finally
            {
                if (stream != null)
                {
#if UNITY_METRO && !UNITY_EDITOR
                    stream.Flush();
#else
                    stream.Close();
#endif
                }
            }
            return result;
        }
    }
}
