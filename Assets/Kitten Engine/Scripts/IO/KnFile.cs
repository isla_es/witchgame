﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
#if UNITY_METRO && !UNITY_EDITOR
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using UnityEngine.Windows;
#endif
using System.IO;
#endregion

namespace KittenEngine.IO
{
    /// <summary>
    /// Multiplatorm implementation of File class.\n
    /// Windows Metro is not yey supported.
    /// </summary>
    public static class KnFile
    {
        #region Methods
        /// <summary>
        /// Determines whether the specified file exists.
        /// </summary>
        /// <param name="pPath">The file to check.</param>
        /// <returns>true if file exists; otherwise, false.</returns>
        public static bool Exists(string pPath)
        {
            return File.Exists(pPath);
        }

        /// <summary>
        /// Opens a file on the specified path. If the file doesn't exist, it's created.
        /// </summary>
        /// <param name="pPath">The path of the file to open.</param>
        /// <returns>A Stream that provides read/write access to the file specified in path.</returns>
        public static Stream Open(string pPath)
        {
            if (!File.Exists(pPath))
            {
                return Create(pPath);
            }
#if UNITY_METRO && !UNITY_EDITOR
            throw new NotImplementedException();
#else
            return File.Open(pPath, FileMode.Open);
#endif
        }

        /// <summary>
        /// Creates a file on the specified path.
        /// </summary>
        /// <param name="pPath">The path of the file to create.</param>
        /// <returns>A Stream that provides read/write access to the file specified in path.</returns>
        public static Stream Create(string pPath)
        {
#if UNITY_METRO && !UNITY_EDITOR
            throw new NotImplementedException();
#else
            return File.Create(pPath);
#endif
        }
        #endregion
    }
}
