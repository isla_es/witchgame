﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System.Reflection;
using UnityEngine;
#endregion

namespace KittenEngine.Extensions
{
    /// <summary>
    /// Extension for the MemberInfo class.
    /// </summary>
    public static class KnMemberInfoExtension
    {
        /// <summary>
        /// Returns the value member stored in the given object.
        /// </summary>
        /// <param name="tpMember">The MemberInfo.</param>
        /// <param name="pObject">The object that hold the member.</param>
        /// <returns>The value holded by the member.</returns>
        public static object GetValue(this MemberInfo tpMember, object pObject)
        {
            FieldInfo field = tpMember as FieldInfo;
            if (field != null)
            {
                return field.GetValue(pObject);
            }
            PropertyInfo property = tpMember as PropertyInfo;
            if (property != null)
            {
                return property.GetValue(pObject, null);
            }
            return null;
        }

        /// <summary>
        /// Sets the value member stored in the given object
        /// </summary>
        /// <param name="tpMember">The object that hold the member.</param>
        /// <param name="pObject">The object that </param>
        /// <param name="pValue">The value to set.</param>
        public static void SetValue(this MemberInfo tpMember, object pObject, object pValue)
        {
            FieldInfo field = tpMember as FieldInfo;
            if (field != null)
            {
                field.SetValue(pObject, pValue);
                return;
            }
            PropertyInfo property = tpMember as PropertyInfo;
            if (property != null)
            {
                property.SetValue(pObject, pValue, null);
                return;
            }
        }
    }
}
