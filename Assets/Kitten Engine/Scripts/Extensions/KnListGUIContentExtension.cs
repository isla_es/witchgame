﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System.Collections.Generic;
using UnityEngine;
#endregion

namespace KittenEngine.Extensions
{
    /// <summary>
    /// Extension of List&lt;GUIContent&gt; Class.
    /// </summary>
    public static class KnListGUIContentExtension
    {
        #region Methods
        /// <summary>
        /// Searches for the specified object and returns the zero-based index of the first occurrence within the entire List&lt;GUIContent&gt;.
        /// </summary>
        /// <param name="tpListGUIContent">The list where the search is performed.</param>
        /// <param name="pItem">The object to locate in the List&lt;GUIContent&gt;.</param>
        /// <returns>The zero-based index of the first occurrence of item within the entire List&lt;GUIContent&gt;, if found; otherwise, –1.</returns>
        public static int IndexOf(this List<GUIContent> tpListGUIContent, string pItem)
        {
            for (int i = 0; i < tpListGUIContent.Count; i++)
            {
                if (tpListGUIContent[i].text.Equals(pItem))
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Add a new item to the list with the given string.
        /// </summary>
        /// <param name="tpListGUIContent">The list where the search is performed.</param>
        /// <param name="pContent">The string content to add.</param>
        public static void Add(this List<GUIContent> tpListGUIContent, string pContent)
        {
            tpListGUIContent.Add(new GUIContent(pContent));
        }

        /// <summary>
        /// Add a new item to the list with the given string and the given tooltip.
        /// </summary>
        /// <param name="tpListGUIContent">The list where the search is performed.</param>
        /// <param name="pContent">The string content to add.</param>
        /// <param name="pToolTip">The tooltip to add.</param>
        public static void Add(this List<GUIContent> tpListGUIContent, string pContent, string pToolTip)
        {
            tpListGUIContent.Add(new GUIContent(pContent, pToolTip));
        }
        #endregion
    }
}
