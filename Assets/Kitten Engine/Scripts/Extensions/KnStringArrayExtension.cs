﻿/**********************************************************************************************************************
 * Kitten Engine
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

namespace KittenEngine.Extensions
{
    /// <summary>
    /// Provides extensions methods to help for basic stuff.
    /// </summary>
    internal class NamespaceDoc { }

    /// <summary>
    /// Extension of string[].
    /// </summary>
    public static class KnStringArrayExtension
    {
        #region Methods
        /// <summary>
        /// Searches for the specified object and returns the zero-based index of the first occurrence within the entire string array.
        /// </summary>
        /// <param name="tpArray">The array where the search is performed.</param>
        /// <param name="pItem">The object to locate in the string array.</param>
        /// <returns>The zero-based index of the first occurrence of item within the entire string array, if found; otherwise, –1.</returns>
        public static int IndexOf(this string[] tpArray, string pItem)
        {
            for (int i = 0; i < tpArray.Length; i++)
            {
                if (tpArray[i].Equals(pItem))
                {
                    return i;
                }
            }
            return -1;
        }
        #endregion
    }
}
