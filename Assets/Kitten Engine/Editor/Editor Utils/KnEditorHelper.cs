﻿/**********************************************************************************************************************
 * Kitten Editor
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
#endregion

namespace KittenEditor.EditorUtils
{
    /// <summary>
    /// Provides usefull methods when working with UnityEditor.
    /// </summary>
    internal class NamespaceDoc { }

    /// <summary>
    /// Provides usefull methods for editor features.
    /// </summary>
    public static class KnEditorHelper
    {
        #region Methods
        /// <summary>
        /// Creates a Scriptable Object of the specified type.
        /// </summary>
        /// <typeparam name="T">Type of the Scriptable Object to create.</typeparam>
        public static void CreateAsset<T>() where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();

            string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (string.IsNullOrEmpty(path))
            {
                path = KnPath.PATH_ASSETS;
            }
            else if (!string.IsNullOrEmpty(Path.GetExtension(path)))
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }

            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/New" + typeof(T).ToString() + ".asset");

            AssetDatabase.CreateAsset(asset, assetPathAndName);

            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }

        /// <summary>
        /// Creates a Scriptable Object of the specified type at the specified path.
        /// </summary>
        /// <typeparam name="T">Type of the Scriptable Object to create.</typeparam>
        /// <param name="pPath">The path where to create the Scriptable Object.</param>
        public static void CreateAsset<T>(string pPath) where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();

            AssetDatabase.CreateAsset(asset, pPath + ".asset");

            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = asset;
        }

        /// <summary>
        /// Returns the CamelCase format string of the specified string.
        /// </summary>
        /// <param name="pOriginalName">The string to format.</param>
        /// <returns>The CamelCase format string.</returns>
        public static string FormatName(string pOriginalName)
        {
            StringBuilder formattedName = new StringBuilder();
            formattedName.Append(pOriginalName[0]);
            int length = pOriginalName.Length;
            for (int i = 1; i < length; i++)
            {
                if (char.IsUpper(pOriginalName[i]))
                    if (!char.IsUpper(pOriginalName[i - 1]) || (i < length - 1 && !char.IsUpper(pOriginalName[i + 1])))
                    {
                        formattedName.Append(' ');
                    }
                formattedName.Append(pOriginalName[i]);
            }
            return formattedName.ToString();
        }

        /// <summary>
        /// Gets the names of the Unity layers in a string array.
        /// </summary>
        /// <returns>The names of the Unity layers.</returns>
        public static string[] GetLayerNames()
        {
            List<string> layerNames = new List<string>();
            for (int i = 0; i < 32; i++)
            {
                string layerName = LayerMask.LayerToName(i);
                if (layerName != null)
                {
                    layerNames.Add(layerName);
                }
            }
            return layerNames.ToArray();
        }

        /// <summary>
        /// Gets the sorted names of the Unity layers in a string array.
        /// </summary>
        /// <returns>The sorted names of the Unity layers.</returns>
        public static string[] GetSortingLayerNames()
        {
            Type internalEditorUtilityType = typeof(InternalEditorUtility);
            PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
            return sortingLayersProperty.GetValue(null, new object[0]) as string[];
        }

        /// <summary>
        /// Gets the names of the Unity scence in a list of GUIContent.
        /// </summary>
        /// <returns>The names of the Unity scenes.</returns>
        public static List<GUIContent> GetSceneNames()
        {
            List<GUIContent> scenes = new List<GUIContent>();
            foreach (EditorBuildSettingsScene iScene in EditorBuildSettings.scenes)
            {
                string name = iScene.path.Substring(iScene.path.LastIndexOf('/') + 1);
                name = name.Substring(0, name.Length - 6);
                scenes.Add(new GUIContent(name));
            }
            return scenes;
        }
        #endregion
    }
}