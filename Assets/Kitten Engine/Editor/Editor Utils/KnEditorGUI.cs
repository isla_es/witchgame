﻿/**********************************************************************************************************************
 * Kitten Editor
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.Extensions;
using KittenEngine.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
#endregion

namespace KittenEditor.EditorUtils
{
    /// <summary>
    /// Provides new methods of EditorGUI.
    /// </summary>
    public static class KnEditorGUI
    {
        #region Fields
        private static List<GUIContent> _scenes = new List<GUIContent>();
        private static string[] _layerNames = null;
        private static List<string> _methodNames = new List<string>();
        private static List<string> _assets = new List<string>();
        private static List<string> _assetPaths = new List<string>();
        #endregion

        #region Methods
        private static int SceneField(Rect pRect, string pLabel, int pScene, List<GUIContent> pScenes)
        {
            return EditorGUI.Popup(pRect, new GUIContent(pLabel), pScene, pScenes.ToArray());
        }

        /// <summary>
        /// Makes a popup for Scenes.
        /// </summary>
        /// <param name="pRect">Rectangle on the screen to use for the Scene field.</param>
        /// <param name="pLabel">The label to display in front of the Scene field.</param>
        /// <param name="pScene">The current Scene index.</param>
        /// <param name="pUseCache">If true, use previous calculated Scenes collection; otherwise, recalculates the Scenes collection.</param>
        /// <returns>The zero-based index of the selected Scene. Returns -1 if no Scene were selected.</returns>
        public static int SceneField(Rect pRect, string pLabel, int pScene, bool pUseCache)
        {
            if (!pUseCache)
            {
                _scenes = KnEditorHelper.GetSceneNames();
            }
            return SceneField(pRect, pLabel, pScene, _scenes);
        }

        /// <summary>
        /// Makes a popup for Scenes.
        /// </summary>
        /// <param name="pRect">Rectangle on the screen to use for the Scene field.</param>
        /// <param name="pLabel">The label to display in front of the Scene field.</param>
        /// <param name="pScene">The current Scene name.</param>
        /// <param name="pUseCache">If true, use previous calculated Scenes collection; otherwise, recalculates the Scenes collection.</param>
        /// <returns>The name of the selected Scene. Returns null if no Scene were selected.</returns>
        public static string SceneField(Rect pRect, string pLabel, string pScene, bool pUseCache)
        {
            if (!pUseCache)
            {
                _scenes = KnEditorHelper.GetSceneNames();
            }
            int index = _scenes.IndexOf(pScene);

            index = SceneField(pRect, pLabel, index, _scenes);

            if (index >= 0 && index < _scenes.Count)
            {
                return _scenes[index].text;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Makes a field for Layer Masks.
        /// </summary>
        /// <param name="pRect">Rectangle on the screen to use for the Layer Mask field.</param>
        /// <param name="pLabel">The label to display in front of the Layer Mask field.</param>
        /// <param name="pMask">The current Layer Mask value.</param>
        /// <param name="pUseCache">If true, use previous calculated Layer Masks collection; otherwise, recalculates the Layer Masks collection.</param>
        /// <returns>The zero-based index of the selected Layer Mask. Returns -1 if no Layer Mask were selected.</returns>
        public static int LayerMaskField(Rect pRect, string pLabel, int pMask, bool pUseCache)
        {
            if (!pUseCache)
            {
                _layerNames = KnEditorHelper.GetLayerNames();
            }
            return EditorGUI.MaskField(pRect, pLabel, pMask, _layerNames);
        }

        /// <summary>
        /// Makes a popup for object Methods.
        /// </summary>
        /// <param name="pTargetType">Type of the inspected object.</param>
        /// <param name="pRect">Rectangle on the screen to use for the Asset Path field.</param>
        /// <param name="pLabel">The label to display in front of the Method field.</param>
        /// <param name="pCurrentMethod">The current method.</param>
        /// <param name="pGetInheritedMethods">If true get only methods that has been declared by the instance; otherwise, get even inherited methods.</param>
        /// <param name="pUseCache">If true, use previous calculated Methods collection; otherwise, recalculates the Methods collection.</param>
        /// <returns>The selected Method. Return null if no Method were selected.</returns>
        public static string MethodField(Type pTargetType, Rect pRect, string pLabel, string pCurrentMethod, bool pGetInheritedMethods, bool pUseCache)
        {
            if (pTargetType == null)
            {
                EditorGUI.Popup(pRect, pLabel, -1, new List<string>().ToArray());
                return null;
            }

            int currentIndex = -1;
            if (!pUseCache)
            {
                MethodInfo[] methods = pTargetType.GetMethods();
                _methodNames = new List<string>();
                foreach (MethodInfo iMethod in methods)
                {
                    string methodName = iMethod.Name;
                    if (_methodNames.Contains(methodName))
                    {
                        continue;
                    }
                    if (!pGetInheritedMethods && iMethod.DeclaringType != pTargetType)
                    {
                        continue;
                    }
                    if (iMethod.GetParameters().Length > 0)
                    {
                        continue;
                    }
                    _methodNames.Add(methodName);
                }
            }
            currentIndex = _methodNames.IndexOf(pCurrentMethod);
            int newIndex = EditorGUI.Popup(pRect, pLabel, currentIndex, _methodNames.ToArray());
            return newIndex >= 0 ? _methodNames[newIndex] : null;
        }

        /// <summary>
        /// Makes a popup for Asset Paths.
        /// </summary>
        /// <param name="pRect">Rectangle on the screen to use for the Asset Path field.</param>
        /// <param name="pLabel">The label to display in front of the Asset Path field.</param>
        /// <param name="pAssetPath">The current Asset Path.</param>
        /// <param name="pRoot">The root directory path to create the collection of the available Asset Paths.</param>
        /// <param name="pPathType">The type of path to build.</param>
        /// <param name="pFilterType">The type of filter to create the collection of the available Asset Paths.</param>
        /// <param name="pExtensions">The extensions to consider.</param>
        /// <param name="pIsRecursive">If true, search in the sub directories; otherwise, only serach in the root directory.</param>
        /// <param name="pUseCache">If true, use previous calculated Asset Paths collection; otherwise, recalculates the Asset Paths collection.</param>
        /// <returns>The selected Asset Path. Return null if no Asset Path were selected.</returns>
        public static string AssetPathField(Rect pRect, string pLabel, string pAssetPath, string pRoot, E_PathType pPathType, E_FilterType pFilterType, List<string> pExtensions, bool pIsRecursive, bool pUseCache)
        {
            if (!pUseCache)
            {
                _assets = new List<string>();
                _assetPaths = new List<string>();
                if (!Directory.Exists(pRoot))
                {
                    Debug.LogWarning(string.Format("Directory \"{0}\" not found", pRoot));
                    return null;
                }
                switch (pFilterType)
                {
                    case (E_FilterType.None):
                        {
                            KnDirectory.GetFiles(pRoot, pPathType, pIsRecursive, ref _assets, ref _assetPaths);
                            break;
                        }
                    case (E_FilterType.Include):
                        {
                            KnDirectory.GetFilesWithExtension(pRoot, pPathType, pExtensions, pIsRecursive, ref _assets, ref _assetPaths);
                            break;
                        }
                    case (E_FilterType.Exclude):
                        {
                            KnDirectory.GetFilesWithoutExtension(pRoot, pPathType, pExtensions, pIsRecursive, ref _assets, ref _assetPaths);
                            break;
                        }
                }
            }
            int index = _assetPaths.IndexOf(pAssetPath);
            index = EditorGUI.Popup(pRect, pLabel, index, _assets.ToArray());
            if (index >= 0 && index < _assetPaths.Count)
            {
                return _assetPaths[index];
            }
            else
            {
                return null;
            }
        }
        #endregion
    }
}
