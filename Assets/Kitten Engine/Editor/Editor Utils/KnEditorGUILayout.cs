﻿/**********************************************************************************************************************
 * Kitten Editor
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.EditorUtils;
using KittenEngine.Extensions;
using KittenEngine.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityObject = UnityEngine.Object;
#endregion

namespace KittenEditor.EditorUtils
{
    /// <summary>
    /// Provides new methods and new display of EditorGUILayout
    /// </summary>
    public static class KnEditorGUILayout
    {
        #region Fields
        private static List<GUIContent> _scenes = new List<GUIContent>();
        private static string[] _layerNames = null;
        private static List<string> _methodNames = new List<string>();
        private static List<GUIContent> _methodGUINames = new List<GUIContent>();
        private static List<GUIContent> _assetsGUI = new List<GUIContent>();
        private static List<GUIContent> _assetPathsGUI = new List<GUIContent>();
        private static float _defaultLabelWidth = 0.0f;
        private static E_DisplayType _displayType = E_DisplayType.Normal;
        #endregion

        #region Properties
        /// <summary>
        /// How to display controls.
        /// </summary>
        public static E_DisplayType DisplayType
        {
            get { return _displayType; }
            set { _displayType = value; }
        }
        #endregion

        #region Constructors
        static KnEditorGUILayout()
        {
            _defaultLabelWidth = EditorGUIUtility.labelWidth;
        }
        #endregion

        #region Methods
        private static void ResetLabelWidth()
        {
            EditorGUIUtility.labelWidth = _defaultLabelWidth;
        }

        /// <summary>
        /// Sets the width of the label to display in front of the field with GUI.skin.label GUIStyle.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        public static void PrefixLabel(GUIContent pContent)
        {
            PrefixLabel(pContent, GUI.skin.label);
        }

        /// <summary>
        /// Sets the width of the label to display in front of the field.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pStyle">The GUIStyle used to calculated the size of the label.</param>
        public static void PrefixLabel(GUIContent pContent, GUIStyle pStyle)
        {
            if (_displayType == E_DisplayType.Fitted)
            {
                Vector2 size = pStyle.CalcSize(pContent);
                EditorGUIUtility.labelWidth = size.x + EditorGUI.indentLevel * 15.0f;
            }
        }

        /// <summary>
        /// Displays a PropertyField.
        /// </summary>
        /// <param name="pProperty">The SerializedProperty to make a field for.</param>
        /// <param name="pContent">The content to display.</param>
        /// <param name="pIncludeChildren">If true the property including children is drawn; otherwise only the control itself (such as only a foldout but nothing below it).</param>
        public static void PropertyField(SerializedProperty pProperty, GUIContent pContent, bool pIncludeChildren)
        {
            PrefixLabel(pContent);
            EditorGUILayout.PropertyField(pProperty, pContent, pIncludeChildren);
            ResetLabelWidth();
        }

        /// <summary>
        /// Makes an object field without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <param name="pObjectType">The type of the objects that can be assigned.</param>
        /// <param name="pAllowSceneObjects">If true, allow assigning scene objects.</param>
        /// <returns>The Unity object that has been set by the user.</returns>
        public static UnityObject ObjectField(GUIContent pContent, UnityObject pValue, Type pObjectType, bool pAllowSceneObjects)
        {
            PrefixLabel(pContent);
            UnityObject result = EditorGUILayout.ObjectField(pContent, pValue, pObjectType, pAllowSceneObjects);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Make an int field without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The int value that has been set by the user.</returns>
        public static int IntField(GUIContent pContent, int pValue)
        {
            PrefixLabel(pContent);
            int result = EditorGUILayout.IntField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Make a popup without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <param name="pDisplayedOptions">An array with the options shown in the popup.</param>
        /// <returns>The index of the option that has been selected by the user.</returns>
        public static int Popup(GUIContent pContent, int pValue, GUIContent[] pDisplayedOptions)
        {
            PrefixLabel(pContent);
            int result = EditorGUILayout.Popup(pContent, pValue, pDisplayedOptions);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Make an integer popup selection field without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <param name="pDisplayedOptions">An array with the displayed options the user can choose from.</param>
        /// <param name="pOptionValues">An array with the values for each option. If optionValues a direct mapping of selectedValue to displayedOptions is assumed.</param>
        /// <returns>The value of the option that has been selected by the user.</returns>
        public static int IntPopup(GUIContent pContent, int pValue, GUIContent[] pDisplayedOptions, int[] pOptionValues)
        {
            PrefixLabel(pContent);
            int result = EditorGUILayout.IntPopup(pContent, pValue, pDisplayedOptions, pOptionValues);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a field for masks without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <param name="pDisplayedOptions">A string array containing the labels for each flag</param>
        /// <returns>The value that has been selected by the user.</returns>
        public static int MaskField(GUIContent pContent, int pValue, string[] pDisplayedOptions)
        {
            PrefixLabel(pContent);
            int result = EditorGUILayout.MaskField(pContent, pValue, pDisplayedOptions);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a slider the user can drag to change an integer value between a min and a max without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <param name="pLeftValue">The value at the left end of the slider.</param>
        /// <param name="pRightValue">The value at the right end of the slider.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static int IntSlider(GUIContent pContent, int pValue, int pLeftValue, int pRightValue)
        {
            PrefixLabel(pContent);
            int result = EditorGUILayout.IntSlider(pContent, pValue, pLeftValue, pRightValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a layer selection field without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static int LayerField(GUIContent pContent, int pValue)
        {
            PrefixLabel(pContent);
            int result = EditorGUILayout.LayerField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a text field for entering floats without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static float FloatField(GUIContent pContent, float pValue)
        {
            PrefixLabel(pContent);
            float result = EditorGUILayout.FloatField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a slider the user can drag to change a value between a min and a max without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <param name="pLeftValue">The value at the left end of the slider.</param>
        /// <param name="pRightValue">The value at the right end of the slider.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static float Slider(GUIContent pContent, float pValue, float pLeftValue, float pRightValue)
        {
            PrefixLabel(pContent);
            float result = EditorGUILayout.Slider(pContent, pValue, pLeftValue, pRightValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a label field without whitespace after the label. (Useful for showing read-only info).
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to display.</param>
        public static void LabelField(GUIContent pContent, string pValue)
        {
            GUIContent fullContent = new GUIContent(string.Format("{0} : {1}", pContent.text, pValue), pContent.tooltip);
            PrefixLabel(fullContent);
            EditorGUILayout.LabelField(fullContent);
            ResetLabelWidth();
        }

        /// <summary>
        /// Makes a selectable label field without whitespace after the label. (Useful for showing read-only info that can be copy-pasted).
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to display.</param>
        public static void SelectableLabel(GUIContent pContent, string pValue)
        {
            GUIContent fullContent = new GUIContent(string.Format("{0} : {1}", pContent.text, pValue));
            PrefixLabel(fullContent);
            EditorGUILayout.SelectableLabel(fullContent.text);
            ResetLabelWidth();
        }

        /// <summary>
        /// Makes a text field without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static string TextField(GUIContent pContent, string pValue)
        {
            PrefixLabel(pContent);
            string result = EditorGUILayout.TextField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a tag selection field without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static string TagField(GUIContent pContent, string pValue)
        {
            PrefixLabel(pContent);
            string result = EditorGUILayout.TagField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a text field where the user can enter a password without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static string PasswordField(GUIContent pContent, string pValue)
        {
            PrefixLabel(pContent);
            string result = EditorGUILayout.PasswordField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes an X and Y field for entering a Vector2 without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static Vector2 Vector2Field(GUIContent pContent, Vector2 pValue)
        {
            PrefixLabel(pContent);
            Vector2 result = EditorGUILayout.Vector2Field(pContent.text, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes an X, Y and Z field for entering a Vector3 without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static Vector3 Vector3Field(GUIContent pContent, Vector3 pValue)
        {
            PrefixLabel(pContent);
            Vector3 result = EditorGUILayout.Vector3Field(pContent.text, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes an X, Y, Z and W field for entering a Vector4 without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static Vector4 Vector4Field(GUIContent pContent, Vector4 pValue)
        {
            PrefixLabel(pContent);
            Vector4 result = EditorGUILayout.Vector4Field(pContent.text, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a special slider the user can use to specify a range between a min and a max without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="rpMinValue">The lower value of the range the slider shows, passed by reference.</param>
        /// <param name="rpMaxValue">The upper value at the range the slider shows, passed by reference.</param>
        /// <param name="pMinLimit">The limit at the left end of the slider.</param>
        /// <param name="pMaxLimit">The limit at the right end of the slider.</param>
        public static void MinMaxSlider(GUIContent pContent, ref float rpMinValue, ref float rpMaxValue, float pMinLimit, float pMaxLimit)
        {
            PrefixLabel(pContent);
            EditorGUILayout.MinMaxSlider(pContent, ref rpMinValue, ref rpMaxValue, pMinLimit, pMaxLimit);
            ResetLabelWidth();
        }

        /// <summary>
        /// Makes a field for enum based masks without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static Enum EnumMaskField(GUIContent pContent, Enum pValue)
        {
            PrefixLabel(pContent);
            Enum result = EditorGUILayout.EnumMaskField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes an enum popup selection field without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static Enum EnumPopup(GUIContent pContent, Enum pValue)
        {
            PrefixLabel(pContent);
            Enum result = EditorGUILayout.EnumPopup(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a field for editing an AnimationCurve without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static AnimationCurve CurveField(GUIContent pContent, AnimationCurve pValue)
        {
            PrefixLabel(pContent);
            AnimationCurve result = EditorGUILayout.CurveField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a field for editing an AnimationCurve without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <param name="pColor">The color to show the curve with.</param>
        /// <param name="pRanges">Optional rectangle that the curve is restrained within.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static AnimationCurve CurveField(GUIContent pContent, AnimationCurve pValue, Color pColor, Rect pRanges)
        {
            PrefixLabel(pContent);
            AnimationCurve result = EditorGUILayout.CurveField(pContent, pValue, pColor, pRanges);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes an X, Y, W and H field for entering a Rect without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static Rect RectField(GUIContent pContent, Rect pValue)
        {
            PrefixLabel(pContent);
            Rect result = EditorGUILayout.RectField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes Center and Extents field for entering a Bounds without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static Bounds BoundsField(GUIContent pContent, Bounds pValue)
        {
            PrefixLabel(pContent);
            Bounds result = EditorGUILayout.BoundsField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a field for selecting a Color without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static Color ColorField(GUIContent pContent, Color pValue)
        {
            PrefixLabel(pContent);
            Color result = EditorGUILayout.ColorField(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a toggle without whitespace after the label.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pValue">The value to edit.</param>
        /// <returns>The value that has been set by the user.</returns>
        public static bool Toggle(GUIContent pContent, bool pValue)
        {
            PrefixLabel(pContent);
            bool result = EditorGUILayout.Toggle(pContent, pValue);
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a single press button with the perfect width. The user clicks them and something happens immediately.
        /// </summary>
        /// <param name="pContent">Text to display on the button.</param>
        /// <returns>true when the users clicks the button.</returns>
        public static bool Button(GUIContent pContent)
        {
            return Button(pContent, GUI.skin.button);
        }

        /// <summary>
        /// Makes a single press button with the perfect width. The user clicks them and something happens immediately.
        /// </summary>
        /// <param name="pContent">Text to display on the button.</param>
        /// <param name="pStyle">The style to use. If left out, the button style from the current GUISkin is used.</param>
        /// <returns>true when the users clicks the button.</returns>
        public static bool Button(GUIContent pContent, GUIStyle pStyle)
        {
            Vector2 size = pStyle.CalcSize(pContent);
            return GUILayout.Button(pContent, pStyle, GUILayout.Width(size.x));
        }

        /// <summary>
        /// Displays a button with the given <see cref="UnityEngine.Texture"/>.
        /// </summary>
        /// <param name="pTexture">The button <see cref="UnityEngine.Texture"/>.</param>
        /// <returns>true if the button has been hit.</returns>
        public static bool Button(Texture pTexture)
        {
            return GUILayout.Button(pTexture, GUILayout.Width(pTexture.width));
        }

        /// <summary>
        /// Makes a label with a foldout arrow to the left of it and indents if necessary.
        /// </summary>
        /// <param name="pLabel">The label to display.</param>
        /// <param name="pShow">The shown foldout state.</param>
        /// <param name="pIndentLevel">The indent level increment value.</param>
        /// <returns>The foldout state selected by the user. If true, you should render sub-objects.</returns>
        public static bool BeginFoldout(string pLabel, bool pShow, int pIndentLevel)
        {
            return BeginFoldout(new GUIContent(pLabel), pShow, pIndentLevel);
        }

        /// <summary>
        /// Makes a label with a foldout arrow to the left of it and indents 1 unit of indent level  if necessary.
        /// </summary>
        /// <param name="pLabel">The label to display.</param>
        /// <param name="pShow">The shown foldout state.</param>
        /// <returns>The foldout state selected by the user. If true, you should render sub-objects.</returns>
        public static bool BeginFoldout(string pLabel, bool pShow)
        {
            return BeginFoldout(new GUIContent(pLabel), pShow, 1);
        }

        /// <summary>
        /// Makes a label with a foldout arrow to the left of it and indents if necessary.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pShow">The shown foldout state.</param>
        /// <param name="pIndentLevel">The indent level increment value.</param>
        /// <returns>The foldout state selected by the user. If true, you should render sub-objects.</returns>
        public static bool BeginFoldout(GUIContent pContent, bool pShow, int pIndentLevel)
        {
            bool show = EditorGUILayout.Foldout(pShow, pContent);
            if (show)
            {
                EditorGUI.indentLevel += pIndentLevel;
            }
            return show;
        }

        /// <summary>
        /// Makes a label with a foldout arrow to the left of it and indents with 1 unit of indent level if necessary.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pShow">The shown foldout state.</param>
        /// <returns>The foldout state selected by the user. If true, you should render sub-objects.</returns>
        public static bool BeginFoldout(GUIContent pContent, bool pShow)
        {
            return BeginFoldout(pContent, pShow, 1);
        }

        /// <summary>
        /// Displays an inspector window-like titlebar.
        /// </summary>
        /// <param name="pShow">The current foldout state.</param>
        /// <param name="pObject">The object that the titlebar is for.</param>
        /// <returns>The foldout state selected by the user.</returns>
        public static bool InspectorTitlebar(bool pShow, UnityObject pObject)
        {
            return InspectorTitlebar(pShow, pObject, 1);
        }

        /// <summary>
        /// Displays an inspector window-like titlebar.
        /// </summary>
        /// <param name="pShow">The current foldout state.</param>
        /// <param name="pObject">The object that the titlebar is for.</param>
        /// <param name="pIndentLevel">The ident level to add.</param>
        /// <returns>The foldout state selected by the user.</returns>
        public static bool InspectorTitlebar(bool pShow, UnityObject pObject, int pIndentLevel)
        {
            bool show = EditorGUILayout.InspectorTitlebar(pShow, pObject);
            if (show && pObject != null)
            {
                EditorGUI.indentLevel += pIndentLevel;
            }
            return show;
        }

        /// <summary>
        /// Reindents correctly after a BeginFoldout call.
        /// </summary>
        /// <param name="pIndentLevel">The indent level decrement value.</param>
        public static void EndFoldout(int pIndentLevel)
        {
            EditorGUI.indentLevel -= pIndentLevel;
        }

        /// <summary>
        /// Reindents correctly after a BeginFoldout call with 1 unit of indent level.
        /// </summary>
        public static void EndFoldout()
        {
            EndFoldout(1);
        }

        private static int SceneField(GUIContent pGUIContent, int pScene, List<GUIContent> pScenes)
        {
            return Popup(pGUIContent, pScene, pScenes.ToArray());
        }

        /// <summary>
        /// Makes a popup for Scenes without whitespace.
        /// </summary>
        /// <param name="pGUIContent">The GUIContent to display.</param>
        /// <param name="pScene">The value to edit.</param>
        /// <param name="pUseCache">If true, use previous calculated Scenes collection; otherwise, recalculates the Scenes collection.</param>
        /// <returns>The zero-based index of the selected Scene. Returns -1 if no Scene were selected.</returns>
        public static int SceneField(GUIContent pGUIContent, int pScene, bool pUseCache)
        {
            if (!pUseCache)
            {
                _scenes = KnEditorHelper.GetSceneNames();
            }
            return SceneField(pGUIContent, pScene, _scenes);
        }

        /// <summary>
        /// Makes a popup for Scenes without whitespace.
        /// </summary>
        /// <param name="pGUIContent">The GUIContent to display.</param>
        /// <param name="pScene">The value to edit.</param>
        /// <param name="pUseCache">If true, use previous calculated Scenes collection; otherwise, recalculates the Scenes collection.</param>
        /// <returns>The name of the selected Scene. Returns null if no Scene were selected.</returns>
        public static GUIContent SceneField(GUIContent pGUIContent, string pScene, bool pUseCache)
        {
            if (!pUseCache)
            {
                _scenes = KnEditorHelper.GetSceneNames();
            }
            int index = _scenes.IndexOf(pScene);

            index = SceneField(pGUIContent, index, _scenes);

            if (index >= 0 && index < _scenes.Count)
            {
                return _scenes[index];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Makes a field for Layer Masks without whitespace.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pMask">The current Layer Mask value.</param>
        /// <param name="pUseCache">If true, use previous calculated Layer Masks collection; otherwise, recalculates the Layer Masks collection.</param>
        /// <returns>The zero-based index of the selected Layer Mask. Returns -1 if no Layer Mask were selected.</returns>
        public static int LayerMaskField(GUIContent pContent, int pMask, bool pUseCache)
        {
            EditorGUILayout.BeginHorizontal();
            PrefixLabel(pContent);
            if (!pUseCache)
            {
                _layerNames = KnEditorHelper.GetLayerNames();
            }
            int result = KnEditorGUILayout.MaskField(pContent, pMask, _layerNames);
            EditorGUILayout.EndHorizontal();
            ResetLabelWidth();
            return result;
        }

        /// <summary>
        /// Makes a popup for object Methods without whitespace.
        /// </summary>
        /// <param name="pTargetType">The type of the target that holds the method.</param>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pCurrentMethod">The current method.</param>
        /// <param name="pGetInheritedMethods">If true get only methods that has been declared by the instance; otherwise, get even inherited methods.</param>
        /// <param name="pUseCache">If true, use previous calculated Methods collection; otherwise, recalculates the Methods collection.</param>
        /// <returns>The selected Method. Return null if no Method were selected.</returns>
        public static string MethodField(Type pTargetType, GUIContent pContent, string pCurrentMethod, bool pGetInheritedMethods, bool pUseCache)
        {
            if (pTargetType == null)
            {
                KnEditorGUILayout.Popup(pContent, -1, new List<GUIContent>().ToArray());
                return null;
            }

            int currentIndex = -1;
            if (!pUseCache)
            {
                MethodInfo[] methods = pTargetType.GetMethods();
                _methodNames = new List<string>();
                _methodGUINames = new List<GUIContent>();
                foreach (MethodInfo iMethod in methods)
                {
                    string methodName = iMethod.Name;
                    if (_methodNames.Contains(methodName))
                    {
                        continue;
                    }
                    if (!pGetInheritedMethods && iMethod.DeclaringType != pTargetType)
                    {
                        continue;
                    }
                    if (iMethod.GetParameters().Length > 0)
                    {
                        continue;
                    }
                    _methodNames.Add(methodName);
                    _methodGUINames.Add(new GUIContent(methodName));
                }
            }
            currentIndex = _methodNames.IndexOf(pCurrentMethod);
            int newIndex = KnEditorGUILayout.Popup(pContent, currentIndex, _methodGUINames.ToArray());
            return newIndex >= 0 ? _methodNames[newIndex] : null;
        }

        /// <summary>
        /// Makes a popup for Asset Paths without whitespace.
        /// </summary>
        /// <param name="pContent">The GUIContent to display.</param>
        /// <param name="pAssetPath">The current Asset Path.</param>
        /// <param name="pRoot">The root directory path to create the collection of the available Asset Paths.</param>
        /// <param name="pPathType">The type of path to return.</param>
        /// <param name="pFilterType">The type of filter to create the collection of the available Asset Paths.</param>
        /// <param name="pExtensions">The extensions to consider.</param>
        /// <param name="pIsRecursive">If true, search in the sub directories; otherwise, only serach in the root directory.</param>
        /// <param name="pUseCache">If true, use previous calculated Asset Paths collection; otherwise, recalculates the Asset Paths collection.</param>
        /// <returns>The selected Asset Path. Return null if no Asset Path were selected.</returns>
        public static string AssetPathField(GUIContent pContent, string pAssetPath, string pRoot, E_PathType pPathType, E_FilterType pFilterType, List<string> pExtensions, bool pIsRecursive, bool pUseCache)
        {
            if (!pUseCache)
            {
                _assetsGUI = new List<GUIContent>();
                _assetPathsGUI = new List<GUIContent>();
                if (!Directory.Exists(pRoot))
                {
                    Debug.LogWarning(string.Format("Directory \"{0}\" not found", pRoot));
                    return null;
                }
                switch (pFilterType)
                {
                    case (E_FilterType.None):
                        {
                            KnDirectory.GetFiles(pRoot, pPathType, pIsRecursive, ref _assetsGUI, ref _assetPathsGUI);
                            break;
                        }
                    case (E_FilterType.Include):
                        {
                            KnDirectory.GetFilesWithExtension(pRoot, pPathType, pExtensions, pIsRecursive, ref _assetsGUI, ref _assetPathsGUI);
                            break;
                        }
                    case (E_FilterType.Exclude):
                        {
                            KnDirectory.GetFilesWithoutExtension(pRoot, pPathType, pExtensions, pIsRecursive, ref _assetsGUI, ref _assetPathsGUI);
                            break;
                        }
                }
            }

            int index = _assetPathsGUI.IndexOf(pAssetPath);
            index = KnEditorGUILayout.Popup(pContent, index, _assetsGUI.ToArray());
            if (index >= 0 && index < _assetPathsGUI.Count)
            {
                return _assetPathsGUI[index].text;
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion
    }
}
