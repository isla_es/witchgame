﻿/**********************************************************************************************************************
 * Kitten Editor
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using UnityEditor;
using UnityEngine;
#endregion

namespace KittenEditor.EditorUtils
{
    /// <summary>
    /// Draws a separator line in the inspector.
    /// </summary>
    public static class KnSeparatorLine
    {
        #region Fields
        private static readonly GUIStyle _lineStyle;
        private static readonly Color _lineColor = EditorGUIUtility.isProSkin ? new Color(0.35f, 0.35f, 0.35f) : new Color(0.5f, 0.5f, 0.5f);
        #endregion

        #region Constructors
        static KnSeparatorLine()
        {
            _lineStyle = new GUIStyle();
            _lineStyle.normal.background = EditorGUIUtility.whiteTexture;
            _lineStyle.stretchWidth = true;
            _lineStyle.margin = new RectOffset(0, 0, 3, 9);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Draws a separator line in the inspector.
        /// </summary>
        /// <param name="pTopMargin">The margin above the line.</param>
        /// <param name="pBottomMargin">The margin below the line.</param>
        /// <param name="pThickness">The line thickness.</param>
        public static void Draw(int pTopMargin, int pBottomMargin, float pThickness = 1.0f)
        {
            Draw(pTopMargin, pBottomMargin, _lineColor, pThickness);
        }

        /// <summary>
        /// Draws a separator line in the inspector.
        /// </summary>
        /// <param name="pTopMargin">The margin above the line.</param>
        /// <param name="pBottomMargin">The margin below the line.</param>
        /// <param name="pColor">The line color.</param>
        /// <param name="pThickness">The line thickness.</param>
        public static void Draw(int pTopMargin, int pBottomMargin, Color pColor, float pThickness = 1.0f)
        {
            _lineStyle.margin = new RectOffset(0, 0, pTopMargin, pBottomMargin);
            Draw(pThickness, _lineStyle, pColor);
        }

        /// <summary>
        /// Draws a separator line in the inspector.
        /// </summary>
        /// <param name="pThickness">The line thickness.</param>
        /// <param name="pStyle">The line style.</param>
        /// <param name="pColor">The line color.</param>
        public static void Draw(float pThickness, GUIStyle pStyle, Color pColor)
        {
            Rect position = GUILayoutUtility.GetRect(GUIContent.none, pStyle, GUILayout.Height(pThickness));

            if (Event.current.type == EventType.Repaint)
            {
                Color restoreColor = GUI.color;
                GUI.color = pColor;
                pStyle.Draw(position, false, false, false, false);
                GUI.color = restoreColor;
            }
        }

        /// <summary>
        /// Draws a separator line in the inspector.
        /// </summary>
        /// <param name="pPosition">The line position.</param>
        /// <param name="pTopMargin">The margin above the line.</param>
        /// <param name="pBottomMargin">The margin below the line.</param>
        public static void Draw(Rect pPosition, int pTopMargin, int pBottomMargin)
        {
            if (Event.current.type == EventType.Repaint)
            {
                Color restoreColor = GUI.color;
                GUI.color = _lineColor;
                _lineStyle.margin = new RectOffset(0, 0, pTopMargin, pBottomMargin);
                _lineStyle.Draw(pPosition, false, false, false, false);
                GUI.color = restoreColor;
            }
        }

        /// <summary>
        /// Draws a separator line in the inspector.
        /// </summary>
        /// <param name="pColor">The line color.</param>
        /// <param name="pThickness">The line thickness.</param>
        /// <param name="pTopMargin">The margin above the line.</param>
        /// <param name="pBottomMargin">The margin below the line.</param>
        public static void Draw(Color pColor, float pThickness, int pTopMargin, int pBottomMargin)
        {
            Rect position = GUILayoutUtility.GetRect(GUIContent.none, _lineStyle, GUILayout.Height(pThickness));
            if (Event.current.type == EventType.Repaint)
            {
                Color restoreColor = GUI.color;
                GUI.color = pColor;
                _lineStyle.margin = new RectOffset(0, 0, pTopMargin, pBottomMargin);
                _lineStyle.Draw(position, false, false, false, false);
                GUI.color = restoreColor;
            }
        }
        #endregion
    }
}
