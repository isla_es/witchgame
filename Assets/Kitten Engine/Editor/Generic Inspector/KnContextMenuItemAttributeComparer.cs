﻿/**********************************************************************************************************************
 * Kitten Editor
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using System.Collections.Generic;
using UnityEngine;
#endregion

namespace KittenEditor.GenericInspector
{
    /// <summary>
    /// Compares <see cref="KnContextMenuItemAttribute"/>.
    /// </summary>
    public class KnContextMenuItemAttributeComparer : IComparer<KnContextMenuItemAttribute>
    {
        #region Methods
        /// <summary>
        /// Compares <see cref="KnContextMenuItemAttribute"/>, if equal, compares their names.
        /// </summary>
        /// <param name="pFirst">The first <see cref="KnContextMenuItemAttribute"/> to compare.</param>
        /// <param name="pSecond">The second <see cref="KnContextMenuItemAttribute"/> to compare.</param>
        /// <returns>If result &lt; 0 : pFirst is less than pSecond.</returns>
        public int Compare(KnContextMenuItemAttribute pFirst, KnContextMenuItemAttribute pSecond)
        {
            int firstOrder = pFirst.Order;
            int secondOrder = pSecond.Order;

            if (firstOrder == secondOrder)
            {
                return pFirst.Text.CompareTo(pSecond.Text);
            }
            else
            {
                return firstOrder.CompareTo(secondOrder);
            }
        }
        #endregion
    }
}
