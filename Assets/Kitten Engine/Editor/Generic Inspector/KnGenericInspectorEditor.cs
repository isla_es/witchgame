﻿/**********************************************************************************************************************
 * Kitten Editor
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEditor.EditorUtils;
using KittenEngine.EditorUtils;
using KittenEngine.Extensions;
using KittenEngine.GenericInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using UnityObject = UnityEngine.Object;
#endregion

namespace KittenEditor.GenericInspector
{
    /// <summary>
    /// Uses defined attributes to create a generic custom inspector.
    /// </summary>
    internal class NamespaceDoc { }

    /// <summary>
    /// Generic custom inspector.
    /// </summary>
    [InitializeOnLoad]
    [CanEditMultipleObjects]
    [CustomEditor(typeof(UnityObject), true)]
    public class KnGenericInspectorEditor : Editor
    {
        #region Constants
        /// <summary>
        /// Keyword for a field in method parameters.
        /// </summary>
        public const string FIELD_PARAMETER = "f:";

        /// <summary>
        /// Keyword for a property in method parameters.
        /// </summary>
        public const string PROPERTY_PARAMETER = "p:";

        /// <summary>
        /// Keyword for the target in method parameters.
        /// </summary>
        public const string SELF_PARAMETER = "self";

        /// <summary>
        /// Keyword for an avent in method parameters.
        /// </summary>
        public const string EVENT_PARAMETER = "event";

        /// <summary>
        /// The default collection item label.
        /// </summary>
        public const string ITEM_LABEL = "Element";
        #endregion

        #region Fields
        private static Dictionary<string, bool> _commonShowFoldouts = new Dictionary<string, bool>();
        private static Dictionary<string, Dictionary<int, bool>> _showFoldouts = new Dictionary<string, Dictionary<int, bool>>();
        private static Dictionary<string, ReorderableList> _reorderableLists = new Dictionary<string, ReorderableList>();
        private static ReorderableList _currentList = null;
        private static string _currentListName = string.Empty;
        private static Attribute _currentAttribute = null;
        private static Stack<object[]> _targets = new Stack<object[]>();
        private static int _targetID = -1;
        private static int _scriptID = -1;
        private static Dictionary<string, Assembly> _assemblies = new Dictionary<string, Assembly>();
        private static List<GUIContent> _displayedOptions = new List<GUIContent>();
        private static E_EditorStateCondition _showCondition = E_EditorStateCondition.All;
        private static E_DisplayType _defaultDisplayType = E_DisplayType.Normal;
        private static bool _showPreview = true;
        #endregion

        #region Properties
        private static object[] _CurrentTargets
        {
            get { return _targets.Peek(); }
        }

        private static object _CurrentTarget
        {
            get { return _CurrentTargets[0]; }
        }

        private KnPreviewAttribute Preview
        {
            get
            {
                Type targetType = target.GetType();
                return Attribute.GetCustomAttribute(targetType, typeof(KnPreviewAttribute)) as KnPreviewAttribute;
            }
        }
        #endregion


        #region Constructors
        static KnGenericInspectorEditor()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (Assembly iAssembly in assemblies)
            {
                _assemblies.Add(iAssembly.GetName().Name, iAssembly);
            }
            _targets = null;
        }
        #endregion

        #region Methods
        private static void PushTargets(MemberInfo pMemberInfo)
        {
            object[] newTargets = new object[_CurrentTargets.Length];
            for (int i = 0; i < newTargets.Length; i++)
            {
                newTargets[i] = pMemberInfo.GetValue(_CurrentTargets[i]);
            }
            _targets.Push(newTargets);
        }

        private static bool CanDo(E_EditorStateCondition pCondition)
        {
            return pCondition == E_EditorStateCondition.All ||
                pCondition == E_EditorStateCondition.Editing && !EditorApplication.isPlaying ||
                pCondition == E_EditorStateCondition.Playing && EditorApplication.isPlaying;
        }

        private static bool BeginFoldOut(GUIContent pContent, string pKey)
        {
            bool show = false;
            if (!_commonShowFoldouts.ContainsKey(pKey))
            {
                _commonShowFoldouts.Add(pKey, false);
            }
            else
            {
                show = _commonShowFoldouts[pKey];
            }
            if (!_showFoldouts.ContainsKey(pKey))
            {
                _showFoldouts.Add(pKey, new Dictionary<int, bool>());
            }
            Dictionary<int, bool> showFoldOuts = _showFoldouts[pKey];
            if (!showFoldOuts.ContainsKey(_scriptID))
            {
                showFoldOuts.Add(_scriptID, false);
                _showFoldouts[pKey] = showFoldOuts;
            }
            else
            {
                show = showFoldOuts[_scriptID];
            }
            show = KnEditorGUILayout.BeginFoldout(pContent, show);
            _commonShowFoldouts[pKey] = show;
            _showFoldouts[pKey][_scriptID] = show;
            return show;
        }

        private static bool InspectorTitlebar(string pKey, UnityObject pObject)
        {
            bool show = false;
            if (!_commonShowFoldouts.ContainsKey(pKey))
            {
                _commonShowFoldouts.Add(pKey, false);
            }
            else
            {
                show = _commonShowFoldouts[pKey];
            }
            if (!_showFoldouts.ContainsKey(pKey))
            {
                _showFoldouts.Add(pKey, new Dictionary<int, bool>());
            }
            Dictionary<int, bool> showFoldOuts = _showFoldouts[pKey];
            if (!showFoldOuts.ContainsKey(_scriptID))
            {
                showFoldOuts.Add(_scriptID, false);
                _showFoldouts[pKey] = showFoldOuts;
            }
            else
            {
                show = showFoldOuts[_scriptID];
            }
            show = KnEditorGUILayout.InspectorTitlebar(show, pObject);
            _commonShowFoldouts[pKey] = show;
            _showFoldouts[pKey][_scriptID] = show;
            return show;
        }

        private static void Clear()
        {
            _showFoldouts.Clear();
            _reorderableLists.Clear();
            _currentList = null;
            _currentListName = string.Empty;
            _currentAttribute = null;
        }

        private void OnEnable()
        {
            int currentTargetID = GetTargetID();
            if (currentTargetID != _targetID)
            {
                Clear();
                _targetID = currentTargetID;
            }
        }

        private int GetTargetID()
        {
            MonoBehaviour currentTarget = target as MonoBehaviour;
            if (currentTarget != null)
            {
                return currentTarget.gameObject.GetInstanceID();
            }
            else
            {
                ScriptableObject scriptableTarget = target as ScriptableObject;
                if (scriptableTarget != null)
                {
                    return scriptableTarget.GetInstanceID();
                }
            }
            return -1;
        }

        private void OnSceneGUI()
        {
            int currentTargetID = GetTargetID();
            bool isCurrentTarget = currentTargetID == _targetID;
            Type targetType = target.GetType();

            Event currentEvent = Event.current;
            KnMouseEventAttribute[] mouseEventMethods = Attribute.GetCustomAttributes(targetType, typeof(KnMouseEventAttribute)) as KnMouseEventAttribute[];
            KnMethodAttribute[] methods = Attribute.GetCustomAttributes(targetType, typeof(KnMethodAttribute)) as KnMethodAttribute[];

            KnInvokeMethodBaseAttribute[] invokeMethods = new KnInvokeMethodBaseAttribute[mouseEventMethods.Length + methods.Length];
            if (mouseEventMethods.Length > 0)
            {
                mouseEventMethods.CopyTo(invokeMethods, 0);
            }
            if (methods.Length > 0)
            {
                methods.CopyTo(invokeMethods, mouseEventMethods.Length);
            }

            if (invokeMethods.Length > 0)
            {
                Array.Sort<KnInvokeMethodBaseAttribute>(invokeMethods, new KnInvokeMethodBaseAttributeComparer());
                foreach (KnInvokeMethodBaseAttribute iInvokeMethod in invokeMethods)
                {
                    KnMouseEventAttribute mouseMethod = iInvokeMethod as KnMouseEventAttribute;
                    if (mouseMethod != null && (mouseMethod.AllowMultipleTargets || isCurrentTarget))
                    {
                        if (mouseMethod.Type != currentEvent.type)
                        {
                            continue;
                        }
                        if (mouseMethod.UseModifiers && mouseMethod.Modifiers != currentEvent.modifiers)
                        {
                            continue;
                        }
                        if (mouseMethod.Button != (int)E_MouseButton.None && mouseMethod.Button != currentEvent.button)
                        {
                            continue;
                        }
                        mouseMethod.AllowMultipleTargets = false;
                        Invoke(mouseMethod, target, targetType);
                        continue;
                    }
                    KnMethodAttribute method = iInvokeMethod as KnMethodAttribute;
                    if (method != null && (method.AllowMultipleTargets || isCurrentTarget))
                    {
                        method.AllowMultipleTargets = false;
                        Invoke(method, target, targetType);
                    }
                }
            }
        }

        /// <summary>
        /// The custom inspector.
        /// </summary>
        public override void OnInspectorGUI()
        {
            Type targetType = target.GetType();

            KnInspectorAttribute inspector = Attribute.GetCustomAttribute(targetType, typeof(KnInspectorAttribute)) as KnInspectorAttribute;
            if (inspector == null)
            {
                base.OnInspectorGUI();
            }
            else
            {
                if (_targets == null)
                {
                    _targets = new Stack<object[]>();
                    _targets.Push(targets);
                }

                Undo.RecordObjects(targets, "Inspector");
                _defaultDisplayType = inspector.DefaultDisplayType;
                _scriptID = target.GetInstanceID();
                serializedObject.Update();
                if (inspector.ShowScript)
                {
                    KnEditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"), new GUIContent("Script"), true);
                }
                serializedObject.ApplyModifiedProperties();

                if (targets.Length == 1 && Preview != null)
                {
                    _showPreview = EditorGUILayout.ToggleLeft(new GUIContent(" Show Preview"), _showPreview);
                }
                EditorGUILayout.Space();

                ShowObject(target, targetType, target.ToString(), inspector);

                if (GUI.changed)
                {
                    EditorUtility.SetDirty(target);
                }
                if (_targets.Count == 1)
                {
                    _targets = null;
                }
            }
        }

        #region Invoke Methods
        private static void ComputeAdvancedPopupDisplayedOptions(KnAdvancedPopupAttribute pAdvancedPopup)
        {
            Assembly assembly = _assemblies[pAdvancedPopup.Assembly];
            Type targetType = _CurrentTarget.GetType();
            object[] parameters = ComputeParameters(_CurrentTarget, targetType, pAdvancedPopup.Parameters);
            if (string.IsNullOrEmpty(pAdvancedPopup.Class))
            {
                MethodInfo method = targetType.GetMethod(pAdvancedPopup.Function);
                _displayedOptions = method.Invoke(_CurrentTarget, parameters) as List<GUIContent>;
            }
            else
            {
                Type classType = assembly.GetType(pAdvancedPopup.Class);
                MethodInfo method = classType.GetMethod(pAdvancedPopup.Function);
                _displayedOptions = method.Invoke(null, parameters) as List<GUIContent>;
            }
        }

        private static object[] ComputeParameters(object pObject, Type pObjectType, params object[] pParameters)
        {
            if (pParameters == null || pParameters.Length == 0)
            {
                return null;
            }
            int length = pParameters.Length;
            object[] result = new object[length];
            for (int i = 0; i < length; i++)
            {
                object value = pParameters[i];
                if (value.GetType() == typeof(string))
                {
                    string stringValue = value as string;
                    if (stringValue.StartsWith(FIELD_PARAMETER))
                    {
                        string fieldName = stringValue.Substring(2);
                        FieldInfo field = pObjectType.GetField(fieldName);
                        value = field.GetValue(pObject);
                    }
                    else if (stringValue.StartsWith(PROPERTY_PARAMETER))
                    {
                        string propertyName = stringValue.Substring(2);
                        PropertyInfo property = pObjectType.GetProperty(propertyName);
                        value = property.GetValue(pObject, null);
                    }
                    else if (stringValue.Equals(SELF_PARAMETER))
                    {
                        value = pObject;
                    }
                    else if (stringValue.Equals(EVENT_PARAMETER))
                    {
                        value = Event.current;
                    }
                }
                result[i] = value;
            }
            return result;
        }

        private static void Invoke(KnInvokeMethodBaseAttribute pInvokeMethod, object pObject, Type pObjectType)
        {
            if (!CanDo(pInvokeMethod.CallCondition))
            {
                return;
            }
            object[] parameters = ComputeParameters(pObject, pObjectType, pInvokeMethod.Parameters);
            if (pInvokeMethod.Class == null)
            {
                if (pInvokeMethod.AllowMultipleTargets)
                {
                    foreach (object iTarget in _CurrentTargets)
                    {
                        Invoke(iTarget, pObjectType, pInvokeMethod.Function, parameters);
                    }
                }
                else
                {
                    Invoke(pObject, pObjectType, pInvokeMethod.Function, parameters);
                }
            }
            else
            {
                if (pInvokeMethod.AllowMultipleTargets)
                {
                    foreach (object iTarget in _CurrentTargets)
                    {
                        Invoke(iTarget, pInvokeMethod.Class, pInvokeMethod.Function, pInvokeMethod.Assembly, parameters);
                    }
                }
                else
                {
                    Invoke(pObject, pInvokeMethod.Class, pInvokeMethod.Function, pInvokeMethod.Assembly, parameters);
                }
            }
        }

        private static void Invoke(KnInvokeMethodBaseAttribute pInvokeMethod, object pObject, Type pObjectType, params object[] pParameters)
        {
            if (!CanDo(pInvokeMethod.CallCondition))
            {
                return;
            }

            int length = 0;
            if (pParameters != null)
            {
                length += pParameters.Length;
            }
            object[] methodParameters = ComputeParameters(pObject, pObjectType, pInvokeMethod.Parameters);
            if (methodParameters != null)
            {
                length += methodParameters.Length;
            }
            object[] parameters = null;
            if (length > 0)
            {
                parameters = new object[length];
                int absoluteIndex = 0;
                if (pParameters != null)
                {
                    for (int i = 0; i < pParameters.Length; i++)
                    {
                        parameters[i] = pParameters[i];
                    }
                    absoluteIndex = pParameters.Length - 1;
                }
                if (methodParameters != null)
                {
                    for (int i = 0; i < methodParameters.Length; i++)
                    {
                        parameters[i + absoluteIndex] = methodParameters[i];
                    }
                }
            }
            if (pInvokeMethod.Class == null)
            {
                if (pInvokeMethod.AllowMultipleTargets)
                {
                    foreach (object iTarget in _CurrentTargets)
                    {
                        Invoke(iTarget, pObjectType, pInvokeMethod.Function, parameters);
                    }
                }
                else
                {
                    Invoke(pObject, pObjectType, pInvokeMethod.Function, parameters);
                }
            }
            else
            {
                if (pInvokeMethod.AllowMultipleTargets)
                {
                    foreach (object iTarget in _CurrentTargets)
                    {
                        Invoke(iTarget, pInvokeMethod.Class, pInvokeMethod.Function, pInvokeMethod.Assembly, parameters);
                    }
                }
                else
                {
                    Invoke(pObject, pInvokeMethod.Class, pInvokeMethod.Function, pInvokeMethod.Assembly, parameters);
                }
            }
        }

        private static void Invoke(object pObject, Type pObjectType, string pFunction, params object[] pParameters)
        {
            MethodInfo method = null;
            int length = pParameters == null ? 0 : pParameters.Length;
            Type[] parametersTypes = new Type[length];
            for (int i = 0; i < length; i++)
            {
                parametersTypes[i] = pParameters[i].GetType();
            }
            method = pObjectType.GetMethod(pFunction, parametersTypes);
            method.Invoke(pObject, pParameters);
        }

        private static void Invoke(object pObject, string pClass, string pFunction, string pAssembly, params object[] pParameters)
        {
            Assembly assembly = _assemblies[pAssembly];
            Type classType = assembly.GetType(pClass);
            MethodInfo method = null;
            int length = pParameters == null ? 0 : pParameters.Length;
            Type[] parametersTypes = new Type[length];
            for (int i = 0; i < length; i++)
            {
                parametersTypes[i] = pParameters[i].GetType();
            }
            method = classType.GetMethod(pFunction, parametersTypes);
            method.Invoke(null, pParameters);
        }
        #endregion

        #region Show Object
        private static void ShowObject(object pObject, Type pObjectType, string pKey, KnInspectorAttribute pInspector)
        {
            KnHelpBoxAttribute[] helpBoxes = Attribute.GetCustomAttributes(pObjectType, typeof(KnHelpBoxAttribute)) as KnHelpBoxAttribute[];
            if (helpBoxes.Length > 0)
            {
                Array.Sort<KnHelpBoxAttribute>(helpBoxes, new KnHelpBoxAttributeComparer());
                foreach (KnHelpBoxAttribute iHelpBox in helpBoxes)
                {
                    EditorGUILayout.HelpBox(iHelpBox.Message, (MessageType)iHelpBox.MessageType);
                }
            }

            KnButtonAlignmentAttribute alignment = Attribute.GetCustomAttribute(pObjectType, typeof(KnButtonAlignmentAttribute)) as KnButtonAlignmentAttribute;
            E_ButtonAlignment buttonAlignment = alignment != null ? alignment.Alignment : E_ButtonAlignment.Vertical;

            KnGUIColorAttribute color = Attribute.GetCustomAttribute(pObjectType, typeof(KnGUIColorAttribute)) as KnGUIColorAttribute;
            GUI.color = color == null ? Color.white : color.Color;

            KnButtonAttribute[] buttons = Attribute.GetCustomAttributes(pObjectType, typeof(KnButtonAttribute)) as KnButtonAttribute[];

            bool align = ShowButtons(buttons, pObject, pObjectType, buttonAlignment);

            GUI.color = Color.white;

            if (align)
            {
                if (buttonAlignment == E_ButtonAlignment.Vertical)
                {
                    EditorGUILayout.EndVertical();
                }
                else
                {
                    EditorGUILayout.EndHorizontal();
                }
            }
            if (_CurrentTargets.Length == 1)
            {
                ShowObject(pObject, pKey, pInspector);
            }
            else
            {
                EditorGUILayout.HelpBox("Multi Object Editing is not yet supported.\nIt only works for class buttons, functions and events", MessageType.None);
            }
        }

        /// <summary>
        /// Shows an object with the generic inspector way.
        /// </summary>
        /// <param name="pObject">The object to displays.</param>
        /// <param name="pPrefix">The current depth hierarchy.</param>
        /// <param name="pInspector">The inspector attribute associated to the object.</param>
        public static void ShowObject(object pObject, string pPrefix, KnInspectorAttribute pInspector)
        {
            Type objectType = pObject.GetType();

            if (pInspector.ExposeFields)
            {
                FieldInfo[] fields = objectType.GetFields();
                if (fields.Length > 0)
                {
                    EditorGUILayout.Space();
                    if (pInspector.TitleDisplay != E_TitleDisplay.None)
                    {
                        string objectTypeName = string.Empty;
                        switch (pInspector.TitleDisplay)
                        {
                            case (E_TitleDisplay.Name):
                                {
                                    objectTypeName = objectType.Name + " - ";
                                    break;
                                }
                            case (E_TitleDisplay.FullName):
                                {
                                    objectTypeName = objectType.FullName + " - ";
                                    break;
                                }
                        }
                        EditorGUILayout.LabelField(string.Format("{0}Fields", objectTypeName), EditorStyles.toolbarButton);
                        EditorGUILayout.Space();
                    }
                    foreach (FieldInfo iField in fields)
                    {
                        if (!iField.IsStatic && iField.IsPublic && !iField.IsLiteral && Attribute.GetCustomAttribute(iField, typeof(HideInInspector)) == null)
                        {
                            KnBeginShowConditionAttribute beginShowCondition = Attribute.GetCustomAttribute(iField, typeof(KnBeginShowConditionAttribute)) as KnBeginShowConditionAttribute;
                            if (beginShowCondition != null)
                            {
                                _showCondition = beginShowCondition.ShowCondition;
                            }
                            if (CanDo(_showCondition))
                            {
                                ShowHeader(iField);

                                KnButtonAlignmentAttribute alignment = Attribute.GetCustomAttribute(iField, typeof(KnButtonAlignmentAttribute)) as KnButtonAlignmentAttribute;
                                E_ButtonAlignment buttonAlignment = alignment != null ? alignment.Alignment : E_ButtonAlignment.Horizontal;

                                KnButtonAttribute[] buttons = Attribute.GetCustomAttributes(iField, typeof(KnButtonAttribute)) as KnButtonAttribute[];

                                bool align = ShowButtons(buttons, pObject, objectType, buttonAlignment);
                                if (align && buttonAlignment == E_ButtonAlignment.Boxed)
                                {
                                    EditorGUILayout.EndHorizontal();
                                }
                                ShowField(pObject, iField, pPrefix + "_" + pObject.GetType().ToString());

                                if (align)
                                {
                                    switch (buttonAlignment)
                                    {
                                        case (E_ButtonAlignment.Horizontal):
                                            {
                                                EditorGUILayout.EndHorizontal();
                                                break;
                                            }
                                        case (E_ButtonAlignment.Vertical):
                                            {
                                                EditorGUILayout.EndVertical();
                                                break;
                                            }
                                    }
                                }
                                ShowFooter(iField);
                            }
                            KnEndShowConditionAttribute endShowCondition = Attribute.GetCustomAttribute(iField, typeof(KnEndShowConditionAttribute)) as KnEndShowConditionAttribute;
                            if (endShowCondition != null)
                            {
                                _showCondition = E_EditorStateCondition.All;
                            }
                        }
                    }
                }
            }
            if (pInspector.ExposeProperties)
            {
                PropertyInfo[] properties = objectType.GetProperties();
                if (properties.Length > 0)
                {
                    EditorGUILayout.Space();
                    if (pInspector.TitleDisplay != E_TitleDisplay.None)
                    {
                        string objectTypeName = string.Empty;
                        switch (pInspector.TitleDisplay)
                        {
                            case (E_TitleDisplay.Name):
                                {
                                    objectTypeName = objectType.Name + " - ";
                                    break;
                                }
                            case (E_TitleDisplay.FullName):
                                {
                                    objectTypeName = objectType.FullName + " - ";
                                    break;
                                }
                        }
                        EditorGUILayout.LabelField(string.Format("{0}Properties", objectTypeName), EditorStyles.toolbarButton);
                        EditorGUILayout.Space();
                    }
                    foreach (PropertyInfo iProperty in properties)
                    {
                        Type declaringType = iProperty.DeclaringType;
                        if (Attribute.GetCustomAttribute(iProperty, typeof(HideInInspector)) == null &&
                            declaringType != typeof(MonoBehaviour) &&
                            declaringType != typeof(Behaviour) &&
                            declaringType != typeof(Component) &&
                            declaringType != typeof(UnityObject))
                        {
                            MethodInfo getMethod = iProperty.GetGetMethod();
                            MethodInfo setMethod = iProperty.GetSetMethod();

                            if (getMethod != null && getMethod.IsPublic)
                            {
                                KnBeginShowConditionAttribute beginShowCondition = Attribute.GetCustomAttribute(iProperty, typeof(KnBeginShowConditionAttribute)) as KnBeginShowConditionAttribute;
                                if (beginShowCondition != null)
                                {
                                    _showCondition = beginShowCondition.ShowCondition;
                                }
                                if (CanDo(_showCondition))
                                {
                                    ShowHeader(iProperty);

                                    KnButtonAlignmentAttribute alignment = Attribute.GetCustomAttribute(iProperty, typeof(KnButtonAlignmentAttribute)) as KnButtonAlignmentAttribute;
                                    E_ButtonAlignment buttonAlignment = alignment != null ? alignment.Alignment : E_ButtonAlignment.Horizontal;

                                    KnButtonAttribute[] buttons = Attribute.GetCustomAttributes(iProperty, typeof(KnButtonAttribute)) as KnButtonAttribute[];

                                    bool align = ShowButtons(buttons, pObject, objectType, buttonAlignment);
                                    if (align && buttonAlignment == E_ButtonAlignment.Boxed)
                                    {
                                        EditorGUILayout.EndHorizontal();
                                    }
                                    if (setMethod != null && setMethod.IsPublic)
                                    {
                                        ShowProperty(pObject, iProperty, pPrefix + "_" + pObject.GetType().ToString());
                                    }
                                    else
                                    {
                                        ShowReadOnlyProperty(pObject, iProperty);
                                    }

                                    if (align)
                                    {
                                        switch (buttonAlignment)
                                        {
                                            case (E_ButtonAlignment.Horizontal):
                                                {
                                                    EditorGUILayout.EndHorizontal();
                                                    break;
                                                }
                                            case (E_ButtonAlignment.Vertical):
                                                {
                                                    EditorGUILayout.EndVertical();
                                                    break;
                                                }
                                        }
                                    }
                                    ShowFooter(iProperty);
                                }
                                KnEndShowConditionAttribute endShowCondition = Attribute.GetCustomAttribute(iProperty, typeof(KnEndShowConditionAttribute)) as KnEndShowConditionAttribute;
                                if (endShowCondition != null)
                                {
                                    _showCondition = E_EditorStateCondition.All;
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void ShowReadOnlyValue(object pValue, GUIContent pContent, KnLabelAttribute pLabel, KnSelectableLabelAttribute pSelectableLabel)
        {
            if (pLabel != null)
            {
                KnEditorGUILayout.LabelField(pContent, pValue != null ? pValue.ToString() : "None");
            }
            else if (pSelectableLabel != null)
            {
                KnEditorGUILayout.SelectableLabel(pContent, pValue != null ? pValue.ToString() : "None");
            }
        }

        private static void ShowReadOnlyArray(Array pArray, KnLabelAttribute pLabel, KnSelectableLabelAttribute pSelectableLabel, KnItemLabelAttribute pItemLabel)
        {
            string prefixLabel = (pItemLabel == null ? ITEM_LABEL : pItemLabel.Label) + " ";

            if (pLabel != null)
            {
                for (int i = 0; i < pArray.Length; i++)
                {
                    object value = pArray.GetValue(i);
                    KnEditorGUILayout.LabelField(new GUIContent(prefixLabel + i.ToString()), value != null ? value.ToString() : "None");
                }
            }
            else if (pSelectableLabel != null)
            {
                for (int i = 0; i < pArray.Length; i++)
                {
                    object value = pArray.GetValue(i);
                    KnEditorGUILayout.SelectableLabel(new GUIContent(prefixLabel + i.ToString()), value != null ? value.ToString() : "None");
                }
            }
        }

        private static void ShowReadOnlyList(IList pList, KnLabelAttribute label, KnSelectableLabelAttribute selectableLabel, KnItemLabelAttribute pItemLabel)
        {
            string prefixLabel = (pItemLabel == null ? ITEM_LABEL : pItemLabel.Label) + " ";

            if (label != null)
            {
                for (int i = 0; i < pList.Count; i++)
                {
                    object value = pList[i];
                    KnEditorGUILayout.LabelField(new GUIContent(prefixLabel + i.ToString()), value != null ? value.ToString() : "None");
                }
            }
            else if (selectableLabel != null)
            {
                for (int i = 0; i < pList.Count; i++)
                {
                    object value = pList[i];
                    KnEditorGUILayout.SelectableLabel(new GUIContent(prefixLabel + i.ToString()), value != null ? value.ToString() : "None");
                }
            }
        }

        private static void ShowReadOnlyProperty(object pObject, PropertyInfo pProperty)
        {
            object value = pProperty.GetValue(pObject, null);
            KnTooltipAttribute tooltip = Attribute.GetCustomAttribute(pProperty, typeof(KnTooltipAttribute)) as KnTooltipAttribute;
            string tooltipText = tooltip != null ? tooltip.Tooltip : string.Empty;

            GUIContent content = new GUIContent(pProperty.Name, tooltipText);
            KnLabelAttribute label = Attribute.GetCustomAttribute(pProperty, typeof(KnLabelAttribute)) as KnLabelAttribute;
            KnSelectableLabelAttribute selectableLabel = Attribute.GetCustomAttribute(pProperty, typeof(KnSelectableLabelAttribute)) as KnSelectableLabelAttribute;

            Type memberType = pProperty.PropertyType;

            if (memberType.IsArray)
            {
                Array array = value as Array;
                ShowReadOnlyArray(array, label, selectableLabel, Attribute.GetCustomAttribute(pProperty, typeof(KnItemLabelAttribute)) as KnItemLabelAttribute);
            }
            else if (memberType.GetInterface("IList") != null)
            {
                IList list = value as IList;
                ShowReadOnlyList(list, label, selectableLabel, Attribute.GetCustomAttribute(pProperty, typeof(KnItemLabelAttribute)) as KnItemLabelAttribute);
            }
            else
            {
                ShowReadOnlyValue(value, content, label, selectableLabel);
            }
        }

        /// <summary>
        /// Displays a collection of buttons in the inspector.
        /// </summary>
        /// <param name="pButtons">The buttons to display.</param>
        /// <param name="pObject">The object that holds the buttons.</param>
        /// <param name="pObjectType">The object type.</param>
        /// <param name="pButtonAlignment">How to display the buttons.</param>
        /// <returns>Returns true if the buttons are aligned (with any type of alignment).</returns>
        public static bool ShowButtons(KnButtonAttribute[] pButtons, object pObject, Type pObjectType, E_ButtonAlignment pButtonAlignment)
        {
            int buttonLength = pButtons.Length;
            bool align = buttonLength > 0;

            if (align)
            {
                switch (pButtonAlignment)
                {
                    case (E_ButtonAlignment.Horizontal):
                        {
                            EditorGUILayout.BeginHorizontal();
                            break;
                        }
                    case (E_ButtonAlignment.Vertical):
                        {
                            EditorGUILayout.BeginVertical();
                            break;
                        }
                    case (E_ButtonAlignment.Boxed):
                        {
                            EditorGUILayout.BeginHorizontal();
                            break;
                        }
                }
            }

            if (buttonLength > 0)
            {
                Array.Sort<KnButtonAttribute>(pButtons, new KnButtonAttributeComparer());
                foreach (KnButtonAttribute iButton in pButtons)
                {
                    if (!CanDo(iButton.CallCondition))
                    {
                        continue;
                    }
                    if ((iButton.Text != null && KnEditorGUILayout.Button(new GUIContent(iButton.Text, iButton.Tooltip))) ||
                        (iButton.TexturePath != null && KnEditorGUILayout.Button(AssetDatabase.LoadAssetAtPath(iButton.TexturePath, typeof(Texture)) as Texture)))
                    {
                        Invoke(iButton, pObject, pObjectType);
                    }
                }
            }
            return align;
        }

        /// <summary>
        /// Shows a member whether it's a field or a property.
        /// </summary>
        /// <param name="pObject">The object that holds the member.</param>
        /// <param name="pMemberInfo">The member to display.</param>
        /// <param name="pMemberType">The member type.</param>
        /// <param name="pPrefix">The current hierarchy depth.</param>
        /// <param name="rpValue">The member value.</param>
        public static void ShowMember(object pObject, MemberInfo pMemberInfo, Type pMemberType, string pPrefix, ref object rpValue)
        {
            if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnBeginHorizontalAttribute)) as KnBeginHorizontalAttribute != null)
            {
                EditorGUILayout.BeginHorizontal();
            }

            Rect inspectorRect = new Rect();

            string memberName = KnEditorHelper.FormatName(pMemberInfo.Name);

            KnTooltipAttribute tooltip = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnTooltipAttribute)) as KnTooltipAttribute;
            string tooltipText = tooltip != null ? tooltip.Tooltip : string.Empty;

            KnLabelAttribute label = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnLabelAttribute)) as KnLabelAttribute;
            KnSelectableLabelAttribute selectableLabel = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnSelectableLabelAttribute)) as KnSelectableLabelAttribute;

            GUIContent content = new GUIContent(memberName, tooltipText);
            if (pMemberType.IsArray)
            {
                Array array = rpValue as Array;
                string key = pPrefix + "_" + content.text;

                bool show = BeginFoldOut(content, key);
                inspectorRect = GUILayoutUtility.GetLastRect();
                if (show)
                {
                    Type itemType = pMemberType.GetElementType();
                    if (array == null)
                    {
                        array = Array.CreateInstance(itemType, 0);
                    }
                    if (label != null || selectableLabel != null)
                    {
                        ShowReadOnlyArray(array, label, selectableLabel, Attribute.GetCustomAttribute(pMemberInfo, typeof(KnItemLabelAttribute)) as KnItemLabelAttribute);
                    }
                    else
                    {
                        KnReorderableListAttribute reorderableList = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnReorderableListAttribute)) as KnReorderableListAttribute;
                        if (reorderableList != null)
                        {
                            int length = array.Length;

                            IList list = typeof(List<>).MakeGenericType(itemType).GetConstructor(Type.EmptyTypes).Invoke(null) as IList;
                            for (int i = 0; i < length; i++)
                            {
                                list.Add(array.GetValue(i));
                            }

                            KnItemLabelAttribute itemLabel = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnItemLabelAttribute)) as KnItemLabelAttribute;
                            ShowReorderableList(pMemberInfo, pObject.GetType(), ref list, itemType, key, itemLabel == null ? ITEM_LABEL : itemLabel.Label,
                                reorderableList.Draggable, reorderableList.DisplayAddButton, reorderableList.DisplayRemoveButton);

                            length = list.Count;
                            array = Array.CreateInstance(itemType, length);
                            for (int i = 0; i < length; i++)
                            {
                                array.SetValue(list[i], i);
                            }
                        }
                        else
                        {
                            ShowArray(pMemberInfo, pObject.GetType(), ref array, itemType, content, pPrefix, key);
                        }
                    }
                    KnEditorGUILayout.EndFoldout();
                }
                rpValue = array;
            }
            else if (pMemberType.GetInterface("IList") != null)
            {
                string key = pPrefix + "_" + content.text;
                bool show = BeginFoldOut(content, key);
                inspectorRect = GUILayoutUtility.GetLastRect();
                if (show)
                {
                    IList list = rpValue as IList;
                    if (list == null)
                    {
                        list = Activator.CreateInstance(pMemberType) as IList;
                    }
                    if (label != null || selectableLabel != null)
                    {
                        ShowReadOnlyList(list, label, selectableLabel, Attribute.GetCustomAttribute(pMemberInfo, typeof(KnItemLabelAttribute)) as KnItemLabelAttribute);
                    }
                    else
                    {
                        KnReorderableListAttribute reorderableList = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnReorderableListAttribute)) as KnReorderableListAttribute;
                        if (reorderableList != null)
                        {
                            KnItemLabelAttribute itemLabel = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnItemLabelAttribute)) as KnItemLabelAttribute;
                            ShowReorderableList(pMemberInfo, pObject.GetType(), ref list, pMemberType.GetGenericArguments()[0], key, itemLabel == null ? ITEM_LABEL : itemLabel.Label,
                                reorderableList.Draggable, reorderableList.DisplayAddButton, reorderableList.DisplayRemoveButton);
                        }
                        else
                        {
                            ShowList(pMemberInfo, pObject.GetType(), ref list, pMemberType.GetGenericArguments()[0], key);
                        }
                        rpValue = list;
                    }
                    KnEditorGUILayout.EndFoldout();
                }
            }
            else
            {
                if (label != null || selectableLabel != null)
                {
                    ShowReadOnlyValue(rpValue, content, label, selectableLabel);
                }
                else if (pMemberType == typeof(LayerMask))
                {
                    LayerMask currentValue = (LayerMask)rpValue;
                    currentValue = KnEditorGUILayout.LayerMaskField(content, currentValue, false);
                    rpValue = currentValue;
                }
                else if (pMemberType.IsSubclassOf(typeof(UnityObject)))
                {
                    UnityObject currentValue = rpValue as UnityObject;
                    ShowUnityObject(pMemberInfo, content, pPrefix, pMemberType, ref currentValue);
                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(int))
                {
                    int currentValue = (int)rpValue;
                    ShowInt(pMemberInfo, content, ref currentValue, false);
                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(float))
                {
                    float currentValue = (float)rpValue;
                    ShowFloat(pMemberInfo, content, ref currentValue);
                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(bool))
                {
                    bool currentValue = (bool)rpValue;
                    KnToggleGroupAttribute toggleGroup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnToggleGroupAttribute)) as KnToggleGroupAttribute;

                    if (toggleGroup != null)
                    {
                        currentValue = EditorGUILayout.BeginToggleGroup(content, currentValue);
                        foreach (string iToogleGroupElement in toggleGroup.ToggleGroup)
                        {
                            Type objectType = pObject.GetType();
                            MemberInfo toggleMember = null;
                            string elementVariableName = iToogleGroupElement;
                            if (elementVariableName.StartsWith(PROPERTY_PARAMETER))
                            {
                                elementVariableName = elementVariableName.Substring(2);
                                toggleMember = objectType.GetProperty(elementVariableName);
                            }
                            else if (elementVariableName.StartsWith(FIELD_PARAMETER))
                            {
                                elementVariableName = elementVariableName.Substring(2);
                                toggleMember = objectType.GetField(elementVariableName);
                            }
                            else
                            {
                                toggleMember = objectType.GetField(elementVariableName);
                            }

                            string toggleFieldName = KnEditorHelper.FormatName(toggleMember.Name);
                            bool currentFieldValue = (bool)toggleMember.GetValue(pObject);
                            currentFieldValue = KnEditorGUILayout.Toggle(new GUIContent(toggleFieldName), currentFieldValue);
                            toggleMember.SetValue(pObject, currentFieldValue);
                        }
                        EditorGUILayout.EndToggleGroup();
                    }
                    else if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnToggleLeftAttribute)) != null)
                    {
                        currentValue = EditorGUILayout.ToggleLeft(content, currentValue);
                    }
                    else
                    {
                        currentValue = KnEditorGUILayout.Toggle(content, currentValue);
                    }

                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(string))
                {
                    string currentValue = rpValue as string;
                    ShowString(pMemberInfo, pObject.GetType(), content, ref currentValue, false);
                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(Vector2))
                {
                    Vector2 currentValue = (Vector2)rpValue;
                    ShowVector2(pMemberInfo, content, ref currentValue);
                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(Vector3))
                {
                    Vector3 currentValue = (Vector3)rpValue;
                    currentValue = KnEditorGUILayout.Vector3Field(content, currentValue);
                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(Vector4))
                {
                    Vector4 currentValue = (Vector4)rpValue;
                    currentValue = KnEditorGUILayout.Vector4Field(content, currentValue);
                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(Rect))
                {
                    Rect currentValue = (Rect)rpValue;
                    currentValue = KnEditorGUILayout.RectField(content, currentValue);
                    rpValue = currentValue;
                }
                else if (pMemberType.IsEnum)
                {
                    Enum currentValue = rpValue as Enum;
                    ShowEnum(pMemberInfo, content, ref currentValue);
                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(Color))
                {
                    Color currentValue = (Color)rpValue;
                    currentValue = KnEditorGUILayout.ColorField(content, currentValue);
                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(Bounds))
                {
                    Bounds currentValue = (Bounds)rpValue;
                    currentValue = KnEditorGUILayout.BoundsField(content, currentValue);
                    rpValue = currentValue;
                }
                else if (pMemberType == typeof(AnimationCurve))
                {
                    AnimationCurve currentValue = (AnimationCurve)rpValue as AnimationCurve;
                    ShowAnimationCurve(pMemberInfo, content, ref currentValue);
                    rpValue = currentValue;
                }
                else
                {
                    content.text += " (" + pMemberType.ToString() + ")";
                    KnInspectorAttribute inspector = Attribute.GetCustomAttribute(pMemberType, typeof(KnInspectorAttribute)) as KnInspectorAttribute;
                    if (inspector != null)
                    {
                        string key = pPrefix + "_" + content;
                        KnSeparatorLine.Draw(3, 3);
                        bool show = true;
                        if (inspector.ShowFoldout)
                        {
                            show = BeginFoldOut(content, key);
                        }
                        else
                        {
                            KnEditorGUILayout.PrefixLabel(content);
                            EditorGUILayout.LabelField(content);
                            EditorGUI.indentLevel += 1;
                        }
                        inspectorRect = GUILayoutUtility.GetLastRect();
                        if (show)
                        {
                            PushTargets(pMemberInfo);
                            ShowObject(rpValue, pMemberType, key, inspector);
                            KnEditorGUILayout.EndFoldout();
                            _targets.Pop();
                        }
                        KnSeparatorLine.Draw(3, 3);
                    }
                    else
                    {
                        KnEditorGUILayout.LabelField(content, rpValue.ToString());
                    }
                }
            }
            if (inspectorRect.width == 0.0f)
            {
                inspectorRect = GUILayoutUtility.GetLastRect();
            }
            ShowContextMenu(inspectorRect, pObject, pMemberInfo);
        }

        private static bool HasMultipleDifferentValues(FieldInfo pField)
        {
            if (_CurrentTargets.Length == 1)
            {
                return false;
            }
            for (int i = 0; i < _CurrentTargets.Length - 1; i++)
            {
                if (!object.Equals(pField.GetValue(_CurrentTargets[i]), pField.GetValue(_CurrentTargets[i + 1])))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Displays a member field.
        /// </summary>
        /// <param name="pObject">The object that holds the field.</param>
        /// <param name="pField">The field to display.</param>
        /// <param name="pPrefix">The current hierarchy depth.</param>
        public static void ShowField(object pObject, FieldInfo pField, string pPrefix)
        {
            object value = pField.GetValue(pObject);
            //EditorGUI.showMixedValue = HasMultipleDifferentValues(pField);
            ShowMember(pObject, pField, pField.FieldType, pPrefix, ref value);
            pField.SetValue(pObject, value);
            //foreach (object iObject in _targets)
            //{
            //    pField.SetValue(iObject, value);
            //}
        }

        /// <summary>
        /// Displays a member property.
        /// </summary>
        /// <param name="pObject">The object that holds the property.</param>
        /// <param name="pProperty">The property to display.</param>
        /// <param name="pPrefix">The current hierarchy depth.</param>
        public static void ShowProperty(object pObject, PropertyInfo pProperty, string pPrefix)
        {
            object value = pProperty.GetValue(pObject, null);
            //EditorGUI.showMixedValue = HasMultipleDifferentValues(pField);
            ShowMember(pObject, pProperty, pProperty.PropertyType, pPrefix, ref value);
            pProperty.SetValue(pObject, value, null);
            //foreach (object iObject in _targets)
            //{
            //    pProperty.SetValue(iObject, value, null);
            //}
        }

        private static void CallContextMenuItemMethod(object pUserData)
        {
            KnContextMenuItemAttribute contextMenuItem = pUserData as KnContextMenuItemAttribute;
            object target = contextMenuItem.Target;
            Type targetType = target.GetType();

            Invoke(contextMenuItem, target, targetType);
        }
        private static void ShowHeader(MemberInfo pMemberInfo)
        {
            KnIndentAttribute indent = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnIndentAttribute)) as KnIndentAttribute;
            if (indent != null)
            {
                EditorGUI.indentLevel += indent.Amount;
            }

            KnGUIColorAttribute color = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnGUIColorAttribute)) as KnGUIColorAttribute;
            GUI.color = color == null ? Color.white : color.Color;

            KnDisplayAttribute display = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnDisplayAttribute)) as KnDisplayAttribute;
            KnEditorGUILayout.DisplayType = display != null ? display.DisplayType : _defaultDisplayType;

            KnSpaceAttribute space = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnSpaceAttribute)) as KnSpaceAttribute;
            if (space != null)
            {
                GUILayout.Space(space.Height);
            }

            KnHeaderAttribute header = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnHeaderAttribute)) as KnHeaderAttribute;
            if (header != null)
            {
                EditorGUILayout.LabelField(header.Header, EditorStyles.boldLabel);
            }
        }

        private static void ShowFooter(MemberInfo pMemberInfo)
        {
            GUI.color = Color.white;

            if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnEndHorizontalAttribute)) as KnEndHorizontalAttribute != null)
            {
                EditorGUILayout.EndHorizontal();
            }

            KnHelpBoxAttribute[] helpBoxes = Attribute.GetCustomAttributes(pMemberInfo, typeof(KnHelpBoxAttribute)) as KnHelpBoxAttribute[];
            if (helpBoxes.Length > 0)
            {
                Array.Sort<KnHelpBoxAttribute>(helpBoxes, new KnHelpBoxAttributeComparer());
                foreach (KnHelpBoxAttribute iHelpBox in helpBoxes)
                {
                    EditorGUILayout.HelpBox(iHelpBox.Message, (MessageType)iHelpBox.MessageType);
                }
            }

            KnSeparatorLineAttribute separatorLine = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnSeparatorLineAttribute)) as KnSeparatorLineAttribute;
            if (separatorLine != null)
            {
                if (separatorLine.HasSetColor)
                {
                    KnSeparatorLine.Draw(separatorLine.TopMargin, separatorLine.BottomMargin, separatorLine.LineColor, separatorLine.Thickness);
                }
                else
                {
                    KnSeparatorLine.Draw(separatorLine.TopMargin, separatorLine.BottomMargin, separatorLine.Thickness);
                }
            }
        }

        private static void ShowContextMenu(Rect pRect, object pObject, MemberInfo pMember)
        {
            Event currentEvent = Event.current;

            KnContextMenuItemAttribute[] contextMenuItems = Attribute.GetCustomAttributes(pMember, typeof(KnContextMenuItemAttribute)) as KnContextMenuItemAttribute[];
            if (contextMenuItems != null && contextMenuItems.Length > 0)
            {
                if (currentEvent.type == EventType.ContextClick)
                {
                    Vector2 mousePosition = currentEvent.mousePosition;

                    if (pRect.Contains(mousePosition))
                    {
                        GenericMenu contextMenu = new GenericMenu();
                        Array.Sort<KnContextMenuItemAttribute>(contextMenuItems, new KnContextMenuItemAttributeComparer());
                        foreach (KnContextMenuItemAttribute iContextMenuItem in contextMenuItems)
                        {
                            iContextMenuItem.Target = pObject;
                            contextMenu.AddItem(new GUIContent(iContextMenuItem.Text), false, CallContextMenuItemMethod, iContextMenuItem);
                        }
                        contextMenu.ShowAsContext();
                    }
                }
            }
        }
        #endregion

        #region Show Members
        private static void ShowUnityObject(MemberInfo pMemberInfo, GUIContent pContent, string pPrefix, Type pMemberType, ref UnityObject rpCurrentValue)
        {
            KnObjectFieldAttribute objectField = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnObjectFieldAttribute)) as KnObjectFieldAttribute;
            bool allowSceneObjects = true;
            bool isEditable = false;
            if (objectField != null)
            {
                allowSceneObjects = objectField.AllowSceneObjects;
                isEditable = objectField.IsEditable;
            }
            rpCurrentValue = KnEditorGUILayout.ObjectField(pContent, rpCurrentValue, pMemberType, allowSceneObjects);
            if (isEditable && rpCurrentValue != null)
            {
                string key = pPrefix + "_" + pContent.text;
                if (InspectorTitlebar(key, rpCurrentValue))
                {
                    PushTargets(pMemberInfo);
                    Editor.CreateEditor(rpCurrentValue).OnInspectorGUI();
                    KnEditorGUILayout.EndFoldout();
                    _targets.Pop();
                }
                KnSeparatorLine.Draw(3, 9);
            }
        }

        private static void ShowInt(MemberInfo pMemberInfo, GUIContent pContent, ref int rpCurrentValue, bool pUseCache)
        {
            KnRangeAttribute slider = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnRangeAttribute)) as KnRangeAttribute;
            KnMaskFieldAttribute maskField = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnMaskFieldAttribute)) as KnMaskFieldAttribute;
            KnIntPopupAttribute intPopup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnIntPopupAttribute)) as KnIntPopupAttribute;
            KnPopupAttribute popup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnPopupAttribute)) as KnPopupAttribute;
            KnAdvancedPopupAttribute advancedPopup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnAdvancedPopupAttribute)) as KnAdvancedPopupAttribute;

            if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnLayerAttribute)) != null)
            {
                rpCurrentValue = KnEditorGUILayout.LayerField(pContent, rpCurrentValue);
            }
            else if (slider != null)
            {
                int min = (int)slider.Min;
                int max = (int)slider.Max;

                if (slider.ShowMinMax)
                {
                    pContent.text = string.Format("{0} ({1};{2})", pContent.text, min, max);
                }

                rpCurrentValue = KnEditorGUILayout.IntSlider(pContent, rpCurrentValue, min, max);
            }
            else if (maskField != null)
            {
                rpCurrentValue = KnEditorGUILayout.MaskField(pContent, rpCurrentValue, maskField.DisplayedOptions);
            }
            else if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnSceneAttribute)) != null)
            {
                rpCurrentValue = KnEditorGUILayout.SceneField(pContent, rpCurrentValue, pUseCache);
            }
            else if (intPopup != null)
            {
                rpCurrentValue = KnEditorGUILayout.IntPopup(pContent, rpCurrentValue, intPopup.DisplayedOptions, intPopup.Options);
            }
            else if (popup != null)
            {
                rpCurrentValue = KnEditorGUILayout.Popup(pContent, rpCurrentValue, popup.DisplayedOptions.ToArray());
            }
            else if (advancedPopup != null)
            {
                if (!pUseCache)
                {
                    ComputeAdvancedPopupDisplayedOptions(advancedPopup);
                }
                rpCurrentValue = KnEditorGUILayout.Popup(pContent, rpCurrentValue, _displayedOptions.ToArray());
            }
            else
            {
                rpCurrentValue = KnEditorGUILayout.IntField(pContent, rpCurrentValue);
            }
        }

        private static void ShowFloat(MemberInfo pMemberInfo, GUIContent pContent, ref float rpCurrentValue)
        {
            KnRangeAttribute slider = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnRangeAttribute)) as KnRangeAttribute;

            if (slider != null)
            {
                float min = slider.Min;
                float max = slider.Max;

                if (slider.ShowMinMax)
                {
                    pContent.text = string.Format("{0} ({1};{2})", pContent.text, min, max);
                }

                rpCurrentValue = KnEditorGUILayout.Slider(pContent, rpCurrentValue, min, max);
            }
            else
            {
                rpCurrentValue = KnEditorGUILayout.FloatField(pContent, rpCurrentValue);
            }
        }

        private static void ShowString(MemberInfo pMemberInfo, Type pObjectType, GUIContent pContent, ref string rpCurrentValue, bool pUseCache)
        {
            KnPopupAttribute popup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnPopupAttribute)) as KnPopupAttribute;
            KnAdvancedPopupAttribute advancedPopup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnAdvancedPopupAttribute)) as KnAdvancedPopupAttribute;
            KnTextAreaAttribute textArea = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnTextAreaAttribute)) as KnTextAreaAttribute;
            KnMultilineAttribute multiline = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnMultilineAttribute)) as KnMultilineAttribute;
            KnMethodNameAttribute methodName = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnMethodNameAttribute)) as KnMethodNameAttribute;
            KnAssetPathAttribute assetPath = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnAssetPathAttribute)) as KnAssetPathAttribute;

            if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnLayerAttribute)) != null)
            {
                int layer = KnEditorGUILayout.LayerField(pContent, LayerMask.NameToLayer(rpCurrentValue));
                rpCurrentValue = LayerMask.LayerToName(layer);
            }
            else if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnTagFieldAttribute)) != null)
            {
                rpCurrentValue = KnEditorGUILayout.TagField(pContent, rpCurrentValue);
            }
            else if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnSceneAttribute)) != null)
            {
                GUIContent currentContentValue = KnEditorGUILayout.SceneField(pContent, rpCurrentValue, pUseCache);
                rpCurrentValue = currentContentValue != null ? currentContentValue.text : string.Empty;
            }
            else if (multiline != null)
            {
                EditorGUILayout.LabelField(pContent);
                rpCurrentValue = EditorGUILayout.TextArea(rpCurrentValue);
            }
            else if (textArea != null)
            {
                EditorGUILayout.LabelField(pContent);
                float max = EditorStyles.textArea.lineHeight * textArea.MaxLines + 3.0f;
                rpCurrentValue = EditorGUILayout.TextArea(rpCurrentValue, GUILayout.MaxHeight(max));
            }
            else if (popup != null)
            {
                List<GUIContent> displayedOptions = popup.DisplayedOptions;
                int index = displayedOptions.IndexOf(rpCurrentValue);
                index = KnEditorGUILayout.Popup(pContent, index, displayedOptions.ToArray());
                if (index >= 0 && index < displayedOptions.Count)
                {
                    rpCurrentValue = displayedOptions[index].text;
                }
            }
            else if (advancedPopup != null)
            {
                if (!pUseCache)
                {
                    ComputeAdvancedPopupDisplayedOptions(advancedPopup);
                }
                int index = _displayedOptions.IndexOf(rpCurrentValue);
                index = KnEditorGUILayout.Popup(pContent, index, _displayedOptions.ToArray());
                if (index >= 0 && index < _displayedOptions.Count)
                {
                    GUIContent currentValue = _displayedOptions[index];
                    rpCurrentValue = currentValue.text;
                }
            }
            else if (methodName != null)
            {
                rpCurrentValue = KnEditorGUILayout.MethodField(methodName.TargetType, pContent, rpCurrentValue, methodName.GetInheritedMethods, pUseCache);
            }
            else if (assetPath != null)
            {
                rpCurrentValue = KnEditorGUILayout.AssetPathField(pContent, rpCurrentValue, assetPath.Root, assetPath.PathType, assetPath.FilterType, assetPath.Extensions, assetPath.IsRecursive, pUseCache);
            }
            else if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnPasswordFieldAttribute)) != null)
            {
                rpCurrentValue = KnEditorGUILayout.PasswordField(pContent, rpCurrentValue);
            }
            else
            {
                rpCurrentValue = KnEditorGUILayout.TextField(pContent, rpCurrentValue);
            }
        }

        private static void ShowVector2(MemberInfo pMemberInfo, GUIContent pContent, ref Vector2 rpCurrentValue)
        {
            KnMinMaxSliderAttribute minMaxSlider = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnMinMaxSliderAttribute)) as KnMinMaxSliderAttribute;
            if (minMaxSlider != null)
            {
                float minValue = rpCurrentValue.x;
                float maxValue = rpCurrentValue.y;
                float minLimit = minMaxSlider.MinLimit;
                float maxLimit = minMaxSlider.MaxLimit;

                EditorGUILayout.BeginHorizontal();
                if (minMaxSlider.ShowMinMax)
                {
                    pContent.text += " (" + minLimit.ToString() + " ; " + maxLimit.ToString() + ")";
                }

                KnEditorGUILayout.MinMaxSlider(pContent, ref minValue, ref maxValue, minLimit, maxLimit);

                rpCurrentValue.x = minValue;
                rpCurrentValue.y = maxValue;
                rpCurrentValue.x = EditorGUILayout.FloatField(rpCurrentValue.x);
                rpCurrentValue.y = EditorGUILayout.FloatField(rpCurrentValue.y);
                EditorGUILayout.EndHorizontal();
            }
            else
            {
                rpCurrentValue = KnEditorGUILayout.Vector2Field(pContent, rpCurrentValue);
            }
        }

        private static void ShowEnum(MemberInfo pMemberInfo, GUIContent pContent, ref Enum rpCurrentValue)
        {
            if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnEnumMaskAttribute)) != null)
            {
                rpCurrentValue = KnEditorGUILayout.EnumMaskField(pContent, rpCurrentValue);
            }
            else
            {
                rpCurrentValue = KnEditorGUILayout.EnumPopup(pContent, rpCurrentValue);
            }
        }

        private static void ShowAnimationCurve(MemberInfo pMemberInfo, GUIContent pContent, ref AnimationCurve rpCurrentValue)
        {
            KnCurveAttribute curve = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnCurveAttribute)) as KnCurveAttribute;
            if (rpCurrentValue == null)
            {
                rpCurrentValue = new AnimationCurve();
            }
            if (curve != null)
            {
                rpCurrentValue = KnEditorGUILayout.CurveField(pContent, rpCurrentValue, curve.Color, curve.Ranges);
            }
            else
            {
                rpCurrentValue = KnEditorGUILayout.CurveField(pContent, rpCurrentValue);
            }
        }
        #endregion

        #region List
        private static void ShowList(MemberInfo pMemberInfo, Type pObjectType, ref IList rpList, Type pItemType, string pKey)
        {
            KnEnumCollectionAttribute enumCollection = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnEnumCollectionAttribute)) as KnEnumCollectionAttribute;
            int size = 0;

            if (enumCollection == null)
            {
                size = KnEditorGUILayout.IntField(new GUIContent("Size"), rpList.Count);
            }
            else
            {
                size = Enum.GetValues(enumCollection.EnumType).Length;
            }

            if (size < 0)
            {
                size = 0;
            }
            bool needToCopy = false;
            if (size != rpList.Count)
            {
                needToCopy = true;
            }

            KnItemLabelAttribute itemLabel = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnItemLabelAttribute)) as KnItemLabelAttribute;
            string prefixLabel = (itemLabel == null ? ITEM_LABEL : itemLabel.Label) + " ";

            if (pItemType == typeof(int))
            {
                if (needToCopy)
                {
                    rpList = CopyList<int>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    int value = (int)rpList[i];
                    ShowInt(pMemberInfo, new GUIContent(label), ref value, i > 0);
                    rpList[i] = value;
                }
            }
            else if (pItemType == typeof(float))
            {
                if (needToCopy)
                {
                    rpList = CopyList<float>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    float value = (float)rpList[i];
                    ShowFloat(pMemberInfo, new GUIContent(label), ref value);
                    rpList[i] = value;
                }
            }
            else if (pItemType == typeof(bool))
            {
                if (needToCopy)
                {
                    rpList = CopyList<bool>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnToggleLeftAttribute)) as KnToggleLeftAttribute != null)
                    {
                        rpList[i] = EditorGUILayout.ToggleLeft(label, (bool)rpList[i]);
                    }
                    else
                    {
                        rpList[i] = KnEditorGUILayout.Toggle(new GUIContent(label), (bool)rpList[i]);
                    }
                }
            }
            else if (pItemType == typeof(string))
            {
                if (needToCopy)
                {
                    rpList = CopyList<string>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    string value = rpList[i] as string;
                    ShowString(pMemberInfo, pObjectType, new GUIContent(label), ref value, i > 0);
                    rpList[i] = value;
                }
            }
            else if (pItemType == typeof(Vector2))
            {
                if (needToCopy)
                {
                    rpList = CopyList<Vector2>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    Vector2 value = (Vector2)rpList[i];
                    ShowVector2(pMemberInfo, new GUIContent(label), ref value);
                    rpList[i] = value;
                }
            }
            else if (pItemType == typeof(Vector3))
            {
                if (needToCopy)
                {
                    rpList = CopyList<Vector3>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    rpList[i] = EditorGUILayout.Vector3Field(label, (Vector3)rpList[i]);
                }
            }
            else if (pItemType == typeof(Vector4))
            {
                if (needToCopy)
                {
                    rpList = CopyList<Vector4>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    rpList[i] = EditorGUILayout.Vector4Field(label, (Vector4)rpList[i]);
                }
            }
            else if (pItemType == typeof(Rect))
            {
                if (needToCopy)
                {
                    rpList = CopyList<Rect>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    rpList[i] = EditorGUILayout.RectField(label, (Rect)rpList[i]);
                }
            }
            else if (pItemType == typeof(LayerMask))
            {
                if (needToCopy)
                {
                    rpList = CopyList<LayerMask>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    rpList[i] = KnEditorGUILayout.LayerMaskField(new GUIContent(label), (int)rpList[i], i > 0);
                }
            }
            else if (pItemType.IsEnum)
            {
                if (needToCopy)
                {
                    rpList = CopyList<Enum>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    Enum value = rpList[i] as Enum;
                    ShowEnum(pMemberInfo, new GUIContent(label), ref value);
                    rpList[i] = value;
                }
            }
            else if (pItemType == typeof(Color))
            {
                if (needToCopy)
                {
                    rpList = CopyList<Color>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    rpList[i] = EditorGUILayout.ColorField(label, (Color)rpList[i]);
                }
            }
            else if (pItemType == typeof(Bounds))
            {
                if (needToCopy)
                {
                    rpList = CopyList<Bounds>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    rpList[i] = EditorGUILayout.BoundsField(label, (Bounds)rpList[i]);
                }
            }
            else if (pItemType == typeof(AnimationCurve))
            {
                if (needToCopy)
                {
                    rpList = CopyList<AnimationCurve>(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    AnimationCurve value = rpList[i] as AnimationCurve;
                    ShowAnimationCurve(pMemberInfo, new GUIContent(label), ref value);
                    rpList[i] = value;
                }
            }
            else if (pItemType.IsSubclassOf(typeof(UnityObject)))
            {
                if (needToCopy)
                {
                    rpList = CopyList(rpList, size);
                }
                for (int i = 0; i < rpList.Count; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    UnityObject value = rpList[i] as UnityObject;
                    ShowUnityObject(pMemberInfo, new GUIContent(label), pKey, pItemType, ref value);
                    rpList[i] = value;
                }
            }
            else
            {
                if (needToCopy)
                {
                    rpList = CopyList<object>(rpList, size, pItemType);
                }
                KnInspectorAttribute inspector = Attribute.GetCustomAttribute(pItemType, typeof(KnInspectorAttribute)) as KnInspectorAttribute;
                string itemTypeLabel = " (" + pItemType.ToString() + ")";
                if (inspector != null)
                {
                    for (int i = 0; i < rpList.Count; i++)
                    {
                        string objectName = prefixLabel + i.ToString();
                        if (enumCollection != null)
                        {
                            objectName = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                        }
                        GUIContent content = new GUIContent(objectName);
                        content.text += itemTypeLabel;

                        string objectKey = pKey + "_" + objectName;
                        if (i == 0)
                        {
                            KnSeparatorLine.Draw(3, 3);
                        }

                        bool showObject = true;
                        if (inspector.ShowFoldout)
                        {
                            showObject = BeginFoldOut(content, objectKey);
                        }
                        else
                        {
                            KnEditorGUILayout.PrefixLabel(content);
                            EditorGUILayout.LabelField(content);
                            EditorGUI.indentLevel += 1;
                        }
                        if (showObject)
                        {
                            ShowObject(rpList[i], objectKey, inspector);
                            KnEditorGUILayout.EndFoldout();
                        }
                        KnSeparatorLine.Draw(3, 3);
                    }
                }
                else
                {
                    for (int i = 0; i < rpList.Count; i++)
                    {
                        string objectName = prefixLabel + i.ToString();
                        if (enumCollection != null)
                        {
                            objectName = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                        }
                        GUIContent content = new GUIContent(objectName);
                        content.text += itemTypeLabel;
                        KnEditorGUILayout.LabelField(content, rpList[i].ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Copy a list with a new size.
        /// </summary>
        /// <typeparam name="T">The generic type of the list.</typeparam>
        /// <param name="pOriginal">The original list.</param>
        /// <param name="pNewSize">The new size.</param>
        /// <returns>Return the new sized list.</returns>
        public static IList CopyList<T>(IList pOriginal, int pNewSize)
        {
            IList result = Activator.CreateInstance(pOriginal.GetType()) as IList;
            for (int i = 0; i < pNewSize; i++)
            {
                if (i < pOriginal.Count)
                {
                    result.Add(pOriginal[i]);
                }
                else
                {
                    result.Add(default(T));
                }
            }
            return result;
        }

        /// <summary>
        /// Copy a list with a new size.
        /// </summary>
        /// <typeparam name="T">The generic type of the list.</typeparam>
        /// <param name="pOriginal">The original list.</param>
        /// <param name="pNewSize">The new size.</param>
        /// <param name="pItemType">The list item type.</param>
        /// <param name="pParameters">The parameters to create the new item instances.</param>
        /// <returns>Return the new sized list.</returns>
        public static IList CopyList<T>(IList pOriginal, int pNewSize, Type pItemType, params object[] pParameters)
        {
            IList result = Activator.CreateInstance(pOriginal.GetType()) as IList;

            for (int i = 0; i < pNewSize; i++)
            {
                if (i < pOriginal.Count)
                {
                    result.Add(pOriginal[i]);
                }
                else
                {
                    result.Add(Activator.CreateInstance(pItemType, pParameters));
                }
            }
            return result;
        }

        /// <summary>
        /// Copy a list with a new size.
        /// </summary>
        /// <param name="pOriginal">The original list.</param>
        /// <param name="pNewSize">The new size.</param>
        /// <returns>Return the new sized list.</returns>
        public static IList CopyList(IList pOriginal, int pNewSize)
        {
            IList result = Activator.CreateInstance(pOriginal.GetType()) as IList;

            for (int i = 0; i < pNewSize; i++)
            {
                if (i < pOriginal.Count)
                {
                    result.Add(pOriginal[i]);
                }
                else
                {
                    result.Add(null);
                }
            }
            return result;
        }
        #endregion

        #region Array
        private static void ShowArray(MemberInfo pMemberInfo, Type pObjectType, ref Array rpArray, Type pItemType, GUIContent pContent, string pPrefix, string pKey)
        {
            KnEnumCollectionAttribute enumCollection = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnEnumCollectionAttribute)) as KnEnumCollectionAttribute;
            KnNamedArray namedArray = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnNamedArray)) as KnNamedArray;
            KnFixedLengthArray fixedLengthArray = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnFixedLengthArray)) as KnFixedLengthArray;

            int size = 0;

            if (enumCollection != null)
            {
                size = Enum.GetValues(enumCollection.EnumType).Length;
            }
            else if (namedArray != null)
            {
                size = namedArray.Names.Length;
            }
            else if (fixedLengthArray != null)
            {
                size = fixedLengthArray.Length;
            }
            else
            {
                size = KnEditorGUILayout.IntField(new GUIContent("Size"), rpArray.Length);
            }
            if (size < 0)
            {
                size = 0;
            }
            bool needToCopy = false;
            if (size != rpArray.Length)
            {
                needToCopy = true;
            }

            KnItemLabelAttribute itemLabel = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnItemLabelAttribute)) as KnItemLabelAttribute;
            string prefixLabel = (itemLabel == null ? ITEM_LABEL : itemLabel.Label) + " ";

            if (pItemType == typeof(int))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<int>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    int value = (int)rpArray.GetValue(i);
                    ShowInt(pMemberInfo, new GUIContent(label), ref value, i > 0);
                    rpArray.SetValue(value, i);
                }
            }
            else if (pItemType == typeof(float))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<float>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    float value = (float)rpArray.GetValue(i);
                    ShowFloat(pMemberInfo, new GUIContent(label), ref value);
                    rpArray.SetValue(value, i);
                }
            }
            else if (pItemType == typeof(bool))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<bool>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnToggleLeftAttribute)) as KnToggleLeftAttribute != null)
                    {
                        rpArray.SetValue(EditorGUILayout.ToggleLeft(label, (bool)rpArray.GetValue(i)), i);
                    }
                    else
                    {
                        rpArray.SetValue(KnEditorGUILayout.Toggle(new GUIContent(label), (bool)rpArray.GetValue(i)), i);
                    }
                }
            }
            else if (pItemType == typeof(string))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<string>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    string value = rpArray.GetValue(i) as string;
                    ShowString(pMemberInfo, pObjectType, new GUIContent(label), ref value, i > 0);
                    rpArray.SetValue(value, i);
                }
            }
            else if (pItemType == typeof(Vector2))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<Vector2>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    Vector2 value = (Vector2)rpArray.GetValue(i);
                    ShowVector2(pMemberInfo, new GUIContent(label), ref value);
                    rpArray.SetValue(value, i);
                }
            }
            else if (pItemType == typeof(Vector3))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<Vector3>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    rpArray.SetValue(EditorGUILayout.Vector3Field(label, (Vector3)rpArray.GetValue(i)), i);
                }
            }
            else if (pItemType == typeof(Vector4))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<Vector4>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    rpArray.SetValue(EditorGUILayout.Vector4Field(label, (Vector4)rpArray.GetValue(i)), i);
                }
            }
            else if (pItemType == typeof(Rect))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<Rect>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    rpArray.SetValue(EditorGUILayout.RectField(label, (Rect)rpArray.GetValue(i)), i);
                }
            }
            else if (pItemType == typeof(LayerMask))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<LayerMask>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    rpArray.SetValue(KnEditorGUILayout.LayerMaskField(new GUIContent(label), (int)rpArray.GetValue(i), i > 0), i);
                }
            }
            else if (pItemType.IsEnum)
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<Enum>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    Enum value = rpArray.GetValue(i) as Enum;
                    ShowEnum(pMemberInfo, new GUIContent(label), ref value);
                    rpArray.SetValue(value, i);
                }
            }
            else if (pItemType == typeof(Color))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<Color>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    rpArray.SetValue(EditorGUILayout.ColorField(label, (Color)rpArray.GetValue(i)), i);
                }
            }
            else if (pItemType == typeof(Bounds))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<Bounds>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    rpArray.SetValue(EditorGUILayout.BoundsField(label, (Bounds)rpArray.GetValue(i)), i);
                }
            }
            else if (pItemType == typeof(AnimationCurve))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<AnimationCurve>(rpArray, size);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    AnimationCurve value = rpArray.GetValue(i) as AnimationCurve;
                    ShowAnimationCurve(pMemberInfo, new GUIContent(label), ref value);
                    rpArray.SetValue(value, i);
                }
            }
            else if (pItemType.IsSubclassOf(typeof(UnityObject)))
            {
                if (needToCopy)
                {
                    rpArray = CopyArray(rpArray, size, pItemType);
                }
                for (int i = 0; i < rpArray.Length; i++)
                {
                    string label = prefixLabel + i.ToString();
                    if (enumCollection != null)
                    {
                        label = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                    }
                    else if (namedArray != null)
                    {
                        label = namedArray.Names[i];
                    }
                    UnityObject value = rpArray.GetValue(i) as UnityObject;
                    ShowUnityObject(pMemberInfo, new GUIContent(label), pKey, pItemType, ref value);
                    rpArray.SetValue(value, i);
                }
            }
            else
            {
                if (needToCopy)
                {
                    rpArray = CopyArray<object>(rpArray, size, pItemType);
                }
                KnInspectorAttribute inspector = Attribute.GetCustomAttribute(pItemType, typeof(KnInspectorAttribute)) as KnInspectorAttribute;
                string itemTypeLabel = " (" + pItemType.ToString() + ")";
                if (inspector != null)
                {
                    for (int i = 0; i < rpArray.Length; i++)
                    {
                        string objectName = prefixLabel + i.ToString();
                        if (enumCollection != null)
                        {
                            objectName = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                        }
                        GUIContent content = new GUIContent(objectName);
                        content.text += itemTypeLabel;

                        string objectKey = pKey + "_" + objectName;
                        if (i == 0)
                        {
                            KnSeparatorLine.Draw(3, 3);
                        }

                        bool showObject = true;
                        if (inspector.ShowFoldout)
                        {
                            showObject = BeginFoldOut(content, objectKey);
                        }
                        else
                        {
                            KnEditorGUILayout.PrefixLabel(content);
                            EditorGUILayout.LabelField(content);
                            EditorGUI.indentLevel += 1;
                        }
                        if (showObject)
                        {
                            ShowObject(rpArray.GetValue(i), objectKey, inspector);
                            KnEditorGUILayout.EndFoldout();
                        }
                        KnSeparatorLine.Draw(3, 3);
                    }
                }
                else
                {
                    for (int i = 0; i < rpArray.Length; i++)
                    {
                        string objectName = prefixLabel + i.ToString();
                        if (enumCollection != null)
                        {
                            objectName = Enum.GetValues(enumCollection.EnumType).GetValue(i).ToString();
                        }
                        GUIContent content = new GUIContent(objectName);
                        content.text += itemTypeLabel;
                        KnEditorGUILayout.LabelField(content, rpArray.GetValue(i).ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Copy an array with a new size.
        /// </summary>
        /// <typeparam name="T">The array item type.</typeparam>
        /// <param name="pOriginal">The original array.</param>
        /// <param name="pNewSize">The new size.</param>
        /// <returns>The new sized array.</returns>
        public static Array CopyArray<T>(Array pOriginal, int pNewSize)
        {
            Array result = Activator.CreateInstance(pOriginal.GetType(), pNewSize) as Array;

            for (int i = 0; i < pNewSize; i++)
            {
                if (i < pOriginal.Length)
                {
                    result.SetValue(pOriginal.GetValue(i), i);
                }
                else
                {
                    result.SetValue(default(T), i);
                }
            }
            return result;
        }

        /// <summary>
        /// Copy an array with a new size.
        /// </summary>
        /// <typeparam name="T">The array item type.</typeparam>
        /// <param name="pOriginal">The original array.</param>
        /// <param name="pNewSize">The new size.</param>
        /// <param name="pItemType">The array item type.</param>
        /// <param name="pParameters">The parameters to create the new item instances.</param>
        /// <returns>The new sized array.</returns>
        public static Array CopyArray<T>(Array pOriginal, int pNewSize, Type pItemType, params object[] pParameters)
        {
            Array result = (Array)Activator.CreateInstance(pOriginal.GetType(), pNewSize);

            for (int i = 0; i < pNewSize; i++)
            {
                if (i < pOriginal.Length)
                {
                    result.SetValue(pOriginal.GetValue(i), i);
                }
                else
                {
                    result.SetValue(Activator.CreateInstance(pItemType, pParameters), i);
                }
            }
            return result;
        }

        /// <summary>
        /// Copy an array with a new size.
        /// </summary>
        /// <param name="pOriginal">The original array.</param>
        /// <param name="pNewSize">The new size.</param>
        /// <param name="pItemType">The array item type.</param>
        /// <returns>The new sized array.</returns>
        public static Array CopyArray(Array pOriginal, int pNewSize, Type pItemType)
        {
            Array result = (Array)Activator.CreateInstance(pOriginal.GetType(), pNewSize);

            for (int i = 0; i < pNewSize; i++)
            {
                if (i < pOriginal.Length)
                {
                    result.SetValue(pOriginal.GetValue(i), i);
                }
                else
                {
                    result.SetValue(null, i);
                }
            }
            return result;
        }
        #endregion

        #region Reorderable List
        private static void ShowReorderableList(MemberInfo pMemberInfo, Type pObjectType, ref IList rpList, Type pItemType, string pKey, string pElementName, bool pDraggable, bool pDisplayAddButton, bool pDisplayRemoveButton)
        {
            if (!_reorderableLists.ContainsKey(pKey))
            {
                ReorderableList newList = new ReorderableList(rpList, pItemType, pDraggable, false, pDisplayAddButton, pDisplayRemoveButton);
                _reorderableLists.Add(pKey, newList);
                newList.headerHeight = 2.0f;
            }
            _currentList = _reorderableLists[pKey];
            RegisterDrawEvent(pMemberInfo, pObjectType, _currentList, pItemType);
            _currentListName = pElementName;
            _currentList.DoLayoutList();
            rpList = _currentList.list;
        }

        private static void RegisterDrawEvent(MemberInfo pMemberInfo, Type pObjectType, ReorderableList pList, Type pItemType)
        {
            if (pItemType == typeof(LayerMask))
            {
                pList.drawElementCallback = DrawLayerMaskField;
            }
            else if (pItemType.IsSubclassOf(typeof(UnityObject)))
            {
                KnObjectFieldAttribute objectField = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnObjectFieldAttribute)) as KnObjectFieldAttribute;
                if (objectField == null)
                {
                    objectField = new KnObjectFieldAttribute();
                }
                _currentAttribute = objectField;
                pList.drawElementCallback = DrawUnityObject;
            }
            else if (pItemType == typeof(int))
            {
                RegisterIntDrawEvent(pMemberInfo, pList, pItemType);
            }
            else if (pItemType == typeof(float))
            {
                RegisterFloatDrawEvent(pMemberInfo, pList, pItemType);
            }
            else if (pItemType == typeof(bool))
            {
                if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnToggleLeftAttribute)) != null)
                {
                    pList.drawElementCallback = DrawToggleLeft;
                }
                else
                {
                    pList.drawElementCallback = DrawToggle;
                }
            }
            else if (pItemType == typeof(string))
            {
                pList.onAddCallback = AddString;
                RegisterStringDrawEvent(pMemberInfo, pObjectType, pList, pItemType);
            }
            else if (pItemType == typeof(Vector2))
            {
                RegisterVector2DrawEvent(pMemberInfo, pList, pItemType);
            }
            else if (pItemType == typeof(Vector3))
            {
                pList.drawElementCallback = DrawVector3Field;
            }
            else if (pItemType == typeof(Vector4))
            {
                pList.drawElementCallback = DrawVector4Field;
            }
            else if (pItemType == typeof(Rect))
            {
                pList.drawElementCallback = DrawRectField;
            }
            else if (pItemType.IsEnum)
            {
                if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnEnumMaskAttribute)) != null)
                {
                    pList.drawElementCallback = DrawEnumMaskField;
                }
                else
                {
                    pList.drawElementCallback = DrawEnumPopup;
                }
            }
            else if (pItemType == typeof(Color))
            {
                pList.drawElementCallback = DrawColorField;
            }
            else if (pItemType == typeof(Bounds))
            {
                pList.drawElementCallback = DrawBoundsField;
            }
            else if (pItemType == typeof(AnimationCurve))
            {
                KnCurveAttribute curve = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnCurveAttribute)) as KnCurveAttribute;
                if (curve != null)
                {
                    _currentAttribute = curve;
                    pList.drawElementCallback = DrawAdvancedCurveField;
                }
                else
                {
                    pList.drawElementCallback = DrawCurveField;
                }
            }
        }

        private static void AddString(ReorderableList list)
        {
            list.list.Add(string.Empty);
        }

        private static void RegisterIntDrawEvent(MemberInfo pMemberInfo, ReorderableList pList, Type pItemType)
        {
            KnRangeAttribute slider = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnRangeAttribute)) as KnRangeAttribute;
            KnMaskFieldAttribute maskField = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnMaskFieldAttribute)) as KnMaskFieldAttribute;
            KnIntPopupAttribute intPopup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnIntPopupAttribute)) as KnIntPopupAttribute;
            KnPopupAttribute popup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnPopupAttribute)) as KnPopupAttribute;
            KnAdvancedPopupAttribute advancedPopup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnAdvancedPopupAttribute)) as KnAdvancedPopupAttribute;

            if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnLayerAttribute)) != null)
            {
                pList.drawElementCallback = DrawLayerField;
            }
            else if (slider != null)
            {
                _currentAttribute = slider;
                pList.drawElementCallback = DrawIntSlider;
            }
            else if (maskField != null)
            {
                _currentAttribute = maskField;
                pList.drawElementCallback = DrawMaskField;
            }
            else if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnSceneAttribute)) != null)
            {
                pList.drawElementCallback = DrawSceneField;
            }
            else if (intPopup != null)
            {
                _currentAttribute = intPopup;
                pList.drawElementCallback = DrawIntPopup;
            }
            else if (popup != null)
            {
                _currentAttribute = popup;
                pList.drawElementCallback = DrawPopup;
            }
            else if (advancedPopup != null)
            {
                _currentAttribute = advancedPopup;
                pList.drawElementCallback = DrawAdvancedPopup;
            }
            else
            {
                pList.drawElementCallback = DrawIntField;
            }
        }

        private static void RegisterFloatDrawEvent(MemberInfo pMemberInfo, ReorderableList pList, Type pItemType)
        {
            KnRangeAttribute slider = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnRangeAttribute)) as KnRangeAttribute;
            if (slider != null)
            {
                _currentAttribute = slider;
                pList.drawElementCallback = DrawSlider;
            }
            else
            {
                pList.drawElementCallback = DrawFloatField;
            }
        }

        private static void RegisterStringDrawEvent(MemberInfo pMemberInfo, Type pObjectType, ReorderableList pList, Type pItemType)
        {
            KnPopupAttribute popup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnPopupAttribute)) as KnPopupAttribute;
            KnAdvancedPopupAttribute advancedPopup = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnAdvancedPopupAttribute)) as KnAdvancedPopupAttribute;
            KnMethodNameAttribute methodName = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnMethodNameAttribute)) as KnMethodNameAttribute;
            KnAssetPathAttribute assetPath = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnAssetPathAttribute)) as KnAssetPathAttribute;

            if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnLayerAttribute)) != null)
            {
                pList.drawElementCallback = DrawStringLayerField;
            }
            else if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnTagFieldAttribute)) != null)
            {
                pList.drawElementCallback = DrawTagField;
            }
            else if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnSceneAttribute)) != null)
            {
                pList.drawElementCallback = DrawStringSceneField;
            }
            else if (popup != null)
            {
                _currentAttribute = popup;
                pList.drawElementCallback = DrawStringPopup;
            }
            else if (advancedPopup != null)
            {
                _currentAttribute = advancedPopup;
                pList.drawElementCallback = DrawStringAdvancedPopup;
            }
            else if (methodName != null)
            {
                _currentAttribute = methodName;
                pList.drawElementCallback = DrawMethodNameField;
            }
            else if (assetPath != null)
            {
                _currentAttribute = assetPath;
                pList.drawElementCallback = DrawAssetPathField;
            }
            else if (Attribute.GetCustomAttribute(pMemberInfo, typeof(KnPasswordFieldAttribute)) != null)
            {
                pList.drawElementCallback = DrawPasswordField;
            }
            else
            {
                pList.drawElementCallback = DrawTextField;
            }
        }

        private static void RegisterVector2DrawEvent(MemberInfo pMemberInfo, ReorderableList pList, Type pItemType)
        {
            KnMinMaxSliderAttribute minMaxSlider = Attribute.GetCustomAttribute(pMemberInfo, typeof(KnMinMaxSliderAttribute)) as KnMinMaxSliderAttribute;
            if (minMaxSlider != null)
            {
                _currentAttribute = minMaxSlider;
                pList.drawElementCallback = DrawMinMaxSlider;
            }
            else
            {
                pList.drawElementCallback = DrawVector2Field;
            }
        }

        private static void DrawLayerMaskField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            LayerMask currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (LayerMask)_currentList.list[pIndex] : new LayerMask();
            currentValue = KnEditorGUI.LayerMaskField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue, pIndex > 0);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawUnityObject(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            UnityObject currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? _currentList.list[pIndex] as UnityObject : null;
            KnObjectFieldAttribute objectField = _currentAttribute as KnObjectFieldAttribute;
            currentValue = EditorGUI.ObjectField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue, currentValue.GetType(), objectField.AllowSceneObjects);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawIntField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            int currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (int)_currentList.list[pIndex] : 0;
            currentValue = EditorGUI.IntField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawIntSlider(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            int currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (int)_currentList.list[pIndex] : 0;
            KnRangeAttribute range = _currentAttribute as KnRangeAttribute;
            int min = (int)range.Min;
            int max = (int)range.Max;

            string name = string.Empty;
            if (range.ShowMinMax)
            {
                name = string.Format("{0} {1} ({2};{3})", _currentListName, pIndex, min, max);
            }
            else
            {
                name = string.Format("{0} {1}", _currentListName, pIndex);
            }

            currentValue = EditorGUI.IntSlider(pRect, name, currentValue, min, max);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawLayerField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            int currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (int)_currentList.list[pIndex] : 0;
            currentValue = EditorGUI.LayerField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawMaskField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            int currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (int)_currentList.list[pIndex] : 0;
            KnMaskFieldAttribute maskField = _currentAttribute as KnMaskFieldAttribute;
            currentValue = EditorGUI.MaskField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue, maskField.DisplayedOptions);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawSceneField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            int currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (int)_currentList.list[pIndex] : 0;
            currentValue = KnEditorGUI.SceneField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue, pIndex > 0);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawIntPopup(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            int currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (int)_currentList.list[pIndex] : 0;
            KnIntPopupAttribute intPopup = _currentAttribute as KnIntPopupAttribute;
            currentValue = EditorGUI.IntPopup(pRect, new GUIContent(string.Format("{0} {1}", _currentListName, pIndex)), currentValue, intPopup.DisplayedOptions, intPopup.Options);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawPopup(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            int currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (int)_currentList.list[pIndex] : 0;
            KnPopupAttribute popup = _currentAttribute as KnPopupAttribute;
            currentValue = EditorGUI.Popup(pRect, new GUIContent(string.Format("{0} {1}", _currentListName, pIndex)), currentValue, popup.DisplayedOptions.ToArray());
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawAdvancedPopup(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            int currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (int)_currentList.list[pIndex] : 0;
            if (pIndex == 0)
            {
                KnAdvancedPopupAttribute advancedPopup = _currentAttribute as KnAdvancedPopupAttribute;
                ComputeAdvancedPopupDisplayedOptions(advancedPopup);
            }
            currentValue = EditorGUI.Popup(pRect, new GUIContent(string.Format("{0} {1}", _currentListName, pIndex)), currentValue, _displayedOptions.ToArray());
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawFloatField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            float currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (float)_currentList.list[pIndex] : 0.0f;
            currentValue = EditorGUI.FloatField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawSlider(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            float currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (float)_currentList.list[pIndex] : 0.0f;
            KnRangeAttribute range = _currentAttribute as KnRangeAttribute;
            float min = range.Min;
            float max = range.Max;

            string name = string.Empty;
            if (range.ShowMinMax)
            {
                name = string.Format("{0} {1} ({2};{3})", _currentListName, pIndex, min, max);
            }
            else
            {
                name = string.Format("{0} {1}", _currentListName, pIndex);
            }
            currentValue = EditorGUI.Slider(pRect, name, currentValue, min, max);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawToggle(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            bool currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (bool)_currentList.list[pIndex] : false;
            currentValue = EditorGUI.Toggle(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawToggleLeft(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            bool currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (bool)_currentList.list[pIndex] : false;
            currentValue = EditorGUI.ToggleLeft(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawTextField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            string currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (string)_currentList.list[pIndex] : string.Empty;
            currentValue = EditorGUI.TextField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawPasswordField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            string currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (string)_currentList.list[pIndex] : string.Empty;
            currentValue = EditorGUI.PasswordField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawStringLayerField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            string currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (string)_currentList.list[pIndex] : string.Empty;
            int layer = LayerMask.NameToLayer(currentValue);
            layer = EditorGUI.LayerField(pRect, string.Format("{0} {1}", _currentListName, pIndex), layer);
            _currentList.list[pIndex] = LayerMask.LayerToName(layer);
        }

        private static void DrawTagField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            string currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (string)_currentList.list[pIndex] : string.Empty;
            currentValue = EditorGUI.TagField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawStringSceneField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            string currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (string)_currentList.list[pIndex] : string.Empty;
            currentValue = KnEditorGUI.SceneField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue, pIndex > 0);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawStringPopup(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            string currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (string)_currentList.list[pIndex] : string.Empty;
            KnPopupAttribute popup = _currentAttribute as KnPopupAttribute;
            List<GUIContent> displayedOptions = popup.DisplayedOptions;
            int index = displayedOptions.IndexOf(currentValue);
            index = EditorGUI.Popup(pRect, new GUIContent(string.Format("{0} {1}", _currentListName, pIndex)), index, displayedOptions.ToArray());
            if (index >= 0 && index < displayedOptions.Count)
            {
                currentValue = displayedOptions[index].text;
            }
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawStringAdvancedPopup(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            string currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (string)_currentList.list[pIndex] : string.Empty;
            if (pIndex == 0)
            {
                KnAdvancedPopupAttribute advancedPopup = _currentAttribute as KnAdvancedPopupAttribute;
                ComputeAdvancedPopupDisplayedOptions(advancedPopup);
            }
            int index = _displayedOptions.IndexOf(currentValue);
            index = EditorGUI.Popup(pRect, new GUIContent(string.Format("{0} {1}", _currentListName, pIndex)), index, _displayedOptions.ToArray());
            if (index >= 0 && index < _displayedOptions.Count)
            {
                currentValue = _displayedOptions[index].text;
            }
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawMethodNameField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            string currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (string)_currentList.list[pIndex] : string.Empty;
            KnMethodNameAttribute methodName = _currentAttribute as KnMethodNameAttribute;
            currentValue = KnEditorGUI.MethodField(methodName.TargetType, pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue, methodName.GetInheritedMethods, pIndex > 0);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawAssetPathField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            string currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (string)_currentList.list[pIndex] : string.Empty;
            KnAssetPathAttribute assetPath = _currentAttribute as KnAssetPathAttribute;
            currentValue = KnEditorGUI.AssetPathField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue, assetPath.Root, assetPath.PathType, assetPath.FilterType, assetPath.Extensions, assetPath.IsRecursive, pIndex > 0);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawVector2Field(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            Vector2 currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (Vector2)_currentList.list[pIndex] : Vector2.zero;
            currentValue = EditorGUI.Vector2Field(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawMinMaxSlider(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            Vector2 currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (Vector2)_currentList.list[pIndex] : Vector2.zero;
            KnMinMaxSliderAttribute minMaxSlider = _currentAttribute as KnMinMaxSliderAttribute;
            float minValue = currentValue.x;
            float maxValue = currentValue.y;
            string label = string.Format("{0} {1}", _currentListName, pIndex);
            if (minMaxSlider.ShowMinMax)
            {
                label += " (" + minMaxSlider.MinLimit.ToString(".00") + " ; " + minMaxSlider.MaxLimit.ToString(".00") + ")";
            }
            float floatFieldWidth = pRect.width * 15.0f / 100.0f;
            pRect.width -= 2 * floatFieldWidth;
            EditorGUI.MinMaxSlider(new GUIContent(label), pRect, ref minValue, ref maxValue, minMaxSlider.MinLimit, minMaxSlider.MaxLimit);
            currentValue.x = minValue;
            currentValue.y = maxValue;
            pRect.x += pRect.width;
            pRect.width = floatFieldWidth;
            currentValue.x = EditorGUI.FloatField(pRect, currentValue.x);
            pRect.x += floatFieldWidth;
            currentValue.y = EditorGUI.FloatField(pRect, currentValue.y);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawVector3Field(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            Vector3 currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (Vector3)_currentList.list[pIndex] : Vector3.zero;
            currentValue = EditorGUI.Vector3Field(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawVector4Field(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            Vector4 currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (Vector4)_currentList.list[pIndex] : Vector4.zero;
            currentValue = EditorGUI.Vector3Field(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawRectField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            Rect currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (Rect)_currentList.list[pIndex] : new Rect();
            currentValue = EditorGUI.RectField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawEnumMaskField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            Enum currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (Enum)_currentList.list[pIndex] : default(Enum);
            currentValue = EditorGUI.EnumMaskField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawEnumPopup(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            Enum currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (Enum)_currentList.list[pIndex] : default(Enum);
            currentValue = EditorGUI.EnumPopup(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawColorField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            Color currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (Color)_currentList.list[pIndex] : Color.black;
            currentValue = EditorGUI.ColorField(pRect, string.Format("{0} {1}", _currentListName, pIndex), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawBoundsField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            Bounds currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (Bounds)_currentList.list[pIndex] : new Bounds();
            currentValue = EditorGUI.BoundsField(pRect, new GUIContent(string.Format("{0} {1}", _currentListName, pIndex)), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawCurveField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            AnimationCurve currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (AnimationCurve)_currentList.list[pIndex] : new AnimationCurve();
            currentValue = EditorGUI.CurveField(pRect, new GUIContent(string.Format("{0} {1}", _currentListName, pIndex)), currentValue);
            _currentList.list[pIndex] = currentValue;
        }

        private static void DrawAdvancedCurveField(Rect pRect, int pIndex, bool pActive, bool pFocused)
        {
            AnimationCurve currentValue = (pIndex >= 0 && pIndex < _currentList.count) ? (AnimationCurve)_currentList.list[pIndex] : new AnimationCurve();
            KnCurveAttribute curve = _currentAttribute as KnCurveAttribute;
            currentValue = EditorGUI.CurveField(pRect, new GUIContent(string.Format("{0} {1}", _currentListName, pIndex)), currentValue, curve.Color, curve.Ranges);
            _currentList.list[pIndex] = currentValue;
        }
        #endregion

        #region Preview
        /// <summary>
        /// Returns true if the inspected object has a custom preview.
        /// </summary>
        /// <returns>Returns true if the inspected object has a custom preview.</returns>
        public override bool HasPreviewGUI()
        {
            return targets.Length == 1 && Preview != null && _showPreview;
        }

        /// <summary>
        /// Return the preview window title.
        /// </summary>
        /// <returns>The preview title.</returns>
        public override GUIContent GetPreviewTitle()
        {
            return new GUIContent(Preview.Title);
        }

        /// <summary>
        /// Draws the preview in the preview area.
        /// </summary>
        /// <param name="pPreviewArea">The preview area.</param>
        public override void DrawPreview(Rect pPreviewArea)
        {
            base.DrawPreview(pPreviewArea);
            KnPreviewAttribute preview = Preview;
            string drawPreviewFunction = preview.DrawPreviewFunction;
            if (string.IsNullOrEmpty(drawPreviewFunction))
            {
                return;
            }

            string className = preview.Class;
            if (string.IsNullOrEmpty(className))
            {
                Invoke(target, target.GetType(), drawPreviewFunction, new object[] { pPreviewArea, target });
            }
            else
            {
                Invoke(target, className, drawPreviewFunction, KnInvokeMethodBaseAttribute.ASSEMBLY_EDITOR, new object[] { pPreviewArea, target });
            }
        }

        /// <summary>
        /// Shows custom controls in the preview header.
        /// </summary>
        public override void OnPreviewSettings()
        {
            base.OnPreviewSettings();

            KnPreviewAttribute preview = Preview;
            string drawSettingsFunction = preview.OnPreviewSettingsFunction;
            if (string.IsNullOrEmpty(drawSettingsFunction))
            {
                return;
            }

            string className = preview.Class;
            if (string.IsNullOrEmpty(className))
            {
                Invoke(target, target.GetType(), drawSettingsFunction, new object[] { target });
            }
            else
            {
                Invoke(target, className, drawSettingsFunction, KnInvokeMethodBaseAttribute.ASSEMBLY_EDITOR, new object[] { target });
            }
        }
        #endregion

        #endregion
    }
}