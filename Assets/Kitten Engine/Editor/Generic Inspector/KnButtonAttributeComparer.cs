﻿/**********************************************************************************************************************
 * Kitten Editor
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using System.Collections.Generic;
#endregion

namespace KittenEditor.GenericInspector
{
    /// <summary>
    /// Compares <see cref="KnButtonAttribute"/>.
    /// </summary>
    public class KnButtonAttributeComparer : IComparer<KnButtonAttribute>
    {
        #region Methods
        /// <summary>
        /// Compares <see cref="KnButtonAttribute"/> order, if equal, compares their names.
        /// </summary>
        /// <param name="pFirst">The first <see cref="KnButtonAttribute"/> to compare.</param>
        /// <param name="pSecond">The second <see cref="KnButtonAttribute"/> to compare.</param>
        /// <returns>If result &lt; 0 : pFirst is less than pSecond.</returns>
        public int Compare(KnButtonAttribute pFirst, KnButtonAttribute pSecond)
        {
            int firstOrder = pFirst.Order;
            int secondOrder = pSecond.Order;

            if (firstOrder != secondOrder)
            {
                return firstOrder.CompareTo(secondOrder);
            }

            return pFirst.Text.CompareTo(pSecond.Text);
        }
        #endregion
    }
}
