﻿/**********************************************************************************************************************
 * Kitten Editor
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEditor.EditorUtils;
using UnityEditor;
using UnityEngine;
using UnityObject = UnityEngine.Object;
#endregion

namespace KittenEditor.GenericInspector
{
    /// <summary>
    /// Displays an editor widow with the given <see cref="UnityObject"/>.
    /// </summary>
    /// <typeparam name="T1">The editor window.</typeparam>
    /// <typeparam name="T2">The <see cref="UnityObject"/></typeparam>
    public abstract class KnGenericEditorWindow<T1, T2> : EditorWindow
        where T1 : EditorWindow
        where T2 : UnityObject
    {
        #region Fields
        /// <summary>
        /// The <see cref="UnityObject"/>The object instance.
        /// </summary>
        protected T2 _instance = null;
        #endregion

        #region Methods
        /// <summary>
        /// Create the editor window.
        /// </summary>
        /// <param name="pTitle">The windows title.</param>
        public static void CreateWindow(string pTitle)
        {
            T1 window = EditorWindow.GetWindow(typeof(T1)) as T1;
            window.title = pTitle;
        }

        /// <summary>
        /// Displays the <see cref="UnityObject"/>.
        /// </summary>
        protected virtual void OnGUI()
        {
            _instance = KnEditorGUILayout.ObjectField(new GUIContent("Editor Instance"), _instance, typeof(T2), true) as T2;
            if (_instance == null)
            {
                return;
            }
            Editor inspector = Editor.CreateEditor(_instance);
            inspector.OnInspectorGUI();
        }
        #endregion
    }
}
