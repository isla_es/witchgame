﻿/**********************************************************************************************************************
 * Kitten Editor
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using System.Collections.Generic;
using UnityEngine;
#endregion

namespace KittenEditor.GenericInspector
{
    /// <summary>
    /// Compares <see cref="KnHelpBoxAttributeComparer"/>.
    /// </summary>
    public class KnHelpBoxAttributeComparer : IComparer<KnHelpBoxAttribute>
    {
        #region Methods
        /// <summary>
        /// Compares <see cref="KnHelpBoxAttributeComparer"/>, if equal, compares their <see cref="E_MessageType"/>, if still equal, compare their names.
        /// </summary>
        /// <param name="pFirst">The first <see cref="KnHelpBoxAttributeComparer"/> to compare.</param>
        /// <param name="pSecond">The second <see cref="KnHelpBoxAttributeComparer"/> to compare.</param>
        /// <returns>If result &lt; 0 : pFirst is less than pSecond.</returns>
        public int Compare(KnHelpBoxAttribute pFirst, KnHelpBoxAttribute pSecond)
        {
            int firstOrder = pFirst.Order;
            int secondOrder = pSecond.Order;

            if (firstOrder != secondOrder)
            {
                return firstOrder.CompareTo(secondOrder);
            }

            int firstType = (int)pFirst.MessageType;
            int secondType = (int)pSecond.MessageType;
            if (firstOrder != secondType)
            {
                return secondType.CompareTo(firstType);
            }

            return pFirst.Message.CompareTo(pSecond.Message);
        }
        #endregion
    }
}
