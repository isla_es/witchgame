﻿/**********************************************************************************************************************
 * Kitten Editor
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using System.Collections.Generic;
#endregion

namespace KittenEditor.GenericInspector
{
    /// <summary>
    /// Compares <see cref="KnInvokeMethodBaseAttributeComparer"/>.
    /// </summary>
    public class KnInvokeMethodBaseAttributeComparer : IComparer<KnInvokeMethodBaseAttribute>
    {
        #region Methods
        /// <summary>
        /// Compares <see cref="KnInvokeMethodBaseAttributeComparer"/>, if equal, compare their functions.
        /// </summary>
        /// <param name="pFirst">The first <see cref="KnInvokeMethodBaseAttributeComparer"/> to compare.</param>
        /// <param name="pSecond">The second <see cref="KnInvokeMethodBaseAttributeComparer"/> to compare.</param>
        /// <returns>If result &lt; 0 : pFirst is less than pSecond.</returns>
        public int Compare(KnInvokeMethodBaseAttribute pFirst, KnInvokeMethodBaseAttribute pSecond)
        {
            int firstOrder = pFirst.Order;
            int secondOrder = pSecond.Order;

            if (firstOrder != secondOrder)
            {
                return firstOrder.CompareTo(secondOrder);
            }

            return pFirst.Function.CompareTo(pSecond.Function);
        }
        #endregion
    }
}
