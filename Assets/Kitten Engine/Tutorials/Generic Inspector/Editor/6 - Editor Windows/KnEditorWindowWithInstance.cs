﻿/**********************************************************************************************************************
 * Kitten Editor Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEditor.GenericInspector;
using KittenEngine.Tutorials.GenericInspector;
using UnityEditor;
#endregion

namespace KittenEditor.Tutorials.GenericInspector
{
    public class KnEditorWindowWithInstance : KnGenericEditorWindow<KnEditorWindowWithInstance, KnEditorWindowInstance>
    {
        [MenuItem("Window/Window With Instance")]
        public static void CreateWindow()
        {
            CreateWindow("Editor Windows");
        }
    }
}
