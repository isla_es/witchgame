﻿/**********************************************************************************************************************
 * Kitten Editor Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using System.Collections.Generic;
using UnityEngine;
#endregion

namespace KittenEditor.Tutorials.GenericInspector
{
    public static class KnBasicControlHelper
    {
        public static List<GUIContent> GetStaticOptionsWithParameters(string pPrefix, string pSuffix)
        {
            List<GUIContent> contents = new List<GUIContent>();
            for (int i = 0; i < 4; i++)
            {
                contents.Add(new GUIContent(string.Format("{0} - Static Option {1} - {2}", pPrefix, i, pSuffix)));
            }
            return contents;
        }
    }
}
