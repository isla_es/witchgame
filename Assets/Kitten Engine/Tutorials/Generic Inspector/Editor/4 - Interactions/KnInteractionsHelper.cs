﻿/**********************************************************************************************************************
 * Kitten Editor Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.Tutorials.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEditor.Tutorials.GenericInspector
{
    public static class KnInteractionsHelper
    {
        public static void PrintMethod(KnMethodCalls pSender, string pMessage)
        {
            Debug.Log("I've been called by: " + pSender.gameObject.name + " and he asked me to say " + pMessage);
        }
    }
}
