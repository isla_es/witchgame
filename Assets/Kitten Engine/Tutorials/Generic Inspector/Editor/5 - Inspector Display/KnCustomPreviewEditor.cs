﻿/**********************************************************************************************************************
 * Kitten Editor Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.Tutorials.GenericInspector;
using UnityEditor;
using UnityEngine;
#endregion

namespace KittenEditor.Tutorials.GenericInspector
{
    public static class KnPreviewDrawer
    {
        public static void DrawCustomPreview(Rect pRect, KnCustomPreview pPreview)
        {
            if (pPreview.Texture != null)
            {
                EditorGUI.DrawPreviewTexture(pRect, pPreview.Texture, null, pPreview.ScaleMode);
            }
            else
            {
                EditorGUI.LabelField(pRect, "Set a texture to display");
            }
        }

        public static void OnCustomPreviewSettings(KnCustomPreview pPreview)
        {
            pPreview.ScaleMode = (ScaleMode)EditorGUILayout.EnumPopup(pPreview.ScaleMode);
        }
    }
}