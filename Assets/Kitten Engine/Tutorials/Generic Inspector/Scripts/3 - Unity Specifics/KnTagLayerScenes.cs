﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnTagLayerScenes : MonoBehaviour
    {
        [KnTagField]
        [KnHelpBox("To create a tag field, add the KnTagfield attribute to a string.")]
        public string Tag = null;

        [KnLayer]
        [KnHelpBox("To create a layer popup, add the KnLayer attribute to a string.")]
        public string StringLayer = null;

        [KnLayer]
        [KnHelpBox("It also works with an int.")]
        public int IntLayer = 0;

        [KnScene]
        [KnHelpBox("To create a scene popup, add the KnScene attribute to a string.\n" +
            "Please note that it will only list the scene that are registered in the Bulid Settings.")]
        public string StringScene = null;

        [KnScene]
        [KnHelpBox("It also works with an int.")]
        public int SceneIndex = 0;
    }
}
