﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnUnityObjectFields : MonoBehaviour
    {
        [KnHelpBox("You can display classic Unity Object field without adding any attribute.")]
        public KnTagLayerScenes TagLayerScenesObject = null;

        [KnObjectField(true, true)]
        [KnHelpBox("But you can also add the KnObjectField(allowSceneObjects, isEditable) to a UnityObject.\n" +
            "You will have more options: you can forbide scene objects or edit the object directly in the inspector!")]
        public KnPathAndCurves PathAndCurvesObject = null;
    }
}
