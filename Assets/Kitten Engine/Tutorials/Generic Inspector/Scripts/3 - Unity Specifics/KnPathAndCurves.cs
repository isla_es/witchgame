﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using KittenEngine.IO;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnPathAndCurves : MonoBehaviour
    {
        [KnAssetPath("", E_PathType.Asset, true, E_FilterType.Include, ".unity")]
        [KnHelpBox("To create an asset path popup, add the KnAssetPath(root, pathType, isRecursive, filterType, extensions) to a string.")]
        public string AllScenes = null;

        [KnAssetPath("Kitten Engine/Tutorials", E_PathType.Asset, true, E_FilterType.Exclude, ".unity")]
        [KnHelpBox("Adjust filters and extensions to create the popup you want.")]
        public string AllButScenes = null;

        [KnCurve(255.0f, 0.0f, 0.0f)]
        [KnHelpBox("To create a custom Curve field, add the KnCurve(red, green, blue, alpha) attribute to an AnimationCurve.")]
        public AnimationCurve Curve = null;
    }
}
