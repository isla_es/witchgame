﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    [KnButton("Say Hello", "Say Hello World", null, "HelloWorld", null, false)]
    [KnHelpBox("To create a button, add the KnButton(text, tooltip, class, method, parameters) to the class.")]
    public class KnButtonCalls : MonoBehaviour
    {
        [KnButton("Say Hello", "Say Hello World", null, "HelloWorld", null, false)]
        [KnHelpBox("You can also add it to a member!")]
        public int IHaveAButton = 0;

        [KnButton("Kitten Engine/Tutorials/Generic Inspector/Textures/Invincible Cat Button.png", null, "HelloWorld", null, false)]
        [KnHelpBox("To create a textured button, replace the text, tooltip parameters with the path of the texture.")]
        public int TexturedButton = 0;

        [KnButtonAlignment(E_ButtonAlignment.Boxed)]
        [KnButton("Say", "Say Hello World", null, "Talk", new object[] { "Say" }, false, E_EditorStateCondition.All, 0, false)]
        [KnButton("Hello", "Say Hello World", null, "Talk", new object[] { "Hello" }, false, E_EditorStateCondition.All, 1, false)]
        [KnButton("Dude", "Say Hello World", null, "Talk", new object[] { "Dude" }, false, E_EditorStateCondition.All, 2, false)]
        [KnHelpBox("To align your buttons, add the KnButtonAlignment(type) to the class or the member.\n"
            + "Try the different alignment types!")]
        public int AlignedButtons = 0;

        public void HelloWorld()
        {
            Debug.Log("Hello World!");
        }

        public void Talk(string pMessage)
        {
            Debug.Log(pMessage);
        }
    }
}
