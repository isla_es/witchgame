﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnContextMenuItems : MonoBehaviour
    {
        [KnContextMenuItem("Say Hello", null, "HelloWorld", null, false, E_EditorStateCondition.All, 0, false)]
        [KnContextMenuItem("Say", null, "Talk", new object[] { "Say" }, false, E_EditorStateCondition.All, 1, false)]
        [KnContextMenuItem("Hello", null, "Talk", new object[] { "Hello" }, false, E_EditorStateCondition.All, 2, false)]
        [KnContextMenuItem("Dude", null, "Talk", new object[] { "Dude" }, false, E_EditorStateCondition.All, 3, false)]
        public string DebugMessage = "";

        public void HelloWorld()
        {
            Debug.Log("Hello World!");
        }

        public void Talk(string pMessage)
        {
            Debug.Log(pMessage);
        }
    }
}
