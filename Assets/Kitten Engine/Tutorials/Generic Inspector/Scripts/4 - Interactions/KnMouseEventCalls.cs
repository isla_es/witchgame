﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    [KnMouseEvent(null, "HelloWorld", E_MouseButton.Left, EventModifiers.Shift, E_EventType.MouseDown, new object[] { "event" }, false)]
    [KnHelpBox("To call a method responding to a mouse event, add the KnMouseEvent(class, method, mouseButton, keyModifiers, mouseEvent, parameters) attribute to the class." +
         "You can use the event keyword to send the event to the method" +
         "You can call methods from the targeted object or a static class, with all the parameters you want and in any assembly.\n" +
         "You will find all the information in the documentation!")]
    public class KnMouseEventCalls : MonoBehaviour
    {
        public void HelloWorld(Event pEvent)
        {
            Debug.Log("I say Hello World! because you : " + pEvent.ToString());
        }
    }
}
