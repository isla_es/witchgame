﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    [KnMethod(null, "Print", null, false)]
    [KnMethod(null, "PrintWithParameter", new object[] { "f:DebugMessage" }, false)]
    [KnMethod("KittenEditor.Tutorials.GenericInspector.KnInteractionsHelper", "PrintMethod", new object[] { "self", "It's easy to call methods from everywhere!" }, true)]
    [KnHelpBox("To call a method from the inspector, add the KnMethod(class, method, parameters) attribute to the class.\n" +
        "You can call methods from the targeted object or a static class, with all the parameters you want and in any assembly.\n" +
        "You will find all the information in the documentation!")]
    public class KnMethodCalls : MonoBehaviour
    {
        public string DebugMessage = "";

        public void Print()
        {
            Debug.Log("I've been called by: " + this.gameObject.name);
        }

        public void PrintWithParameter(string pMessage)
        {
            Debug.Log("I've been called by: " + this.gameObject.name + " with parameter: " + DebugMessage);
        }
    }
}
