﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
using System.Collections.Generic;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnReorderableCollections : MonoBehaviour
    {
        [KnReorderableList]
        [KnHelpBox("To create a reorderable list, add the KnReorderableList attribute to a list.\n")]
        public List<int> IntReorderableList = null;

        [KnReorderableList]
        [KnHelpBox("It also works with arrays !.\n")]
        public string[] StringReorderableArray = null;

        [KnReorderableList]
        [KnRange(0.0f, 100.0f)]
        [KnHelpBox("Like with all collections, reorderable list are combinables with many other attributes, read the description.")]
        public List<float> FloatReorderableList = null;
    }
}
