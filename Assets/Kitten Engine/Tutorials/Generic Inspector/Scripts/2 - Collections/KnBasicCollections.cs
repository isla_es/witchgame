﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
using System.Collections.Generic;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnBasicCollections : MonoBehaviour
    {
        [KnRange(0.0f, 100.0f)]
        [KnHelpBox("You can combine attributes with lists!")]
        public List<float> SliderList = null;

        [KnEnumMask]
        [KnHelpBox("Even with arrays! You can check the compatibility in the attribute description.\n" +
            "Please note that every attributes that don't affect the member (like KnIndent, KnSpace, etc...) are compatibles with everything.")]
        public E_Elements[] EnumMaskArray = null;

        [KnFixedLengthArray(5)]
        [KnHelpBox("To create a fixed length array, add KnFixedLengthArray(length) attribute.")]
        public int[] FixedLengthArray = null;
    }
}
