﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
using System.Collections.Generic;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnNamedCollections : MonoBehaviour
    {
        [KnItemLabel("Label")]
        [KnHelpBox("To specify the name of your collection elements, add the attribute KnItemLabel(name).")]
        public List<string> LabeledList = null;

        [KnNamedArray("First Player", "Second Player", "Third Player")]
        [KnHelpBox("To add a fixed length array choosing each item label, add the KnNamedArray(names) attribute.\n" +
            "This one is only compatble with arrays.")]
        public string[] NamedArray = null;

        [KnEnumCollection(typeof(E_Elements))]
        [KnHelpBox("To add a collection which names (and sizes) correspond to an enum, add the KnEnumCollection(enum type) attribute.")]
        public List<int> ElementValues = null;
    }
}
