﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    public class KnMethodNameHolder : MonoBehaviour
    {
        public void Kill() { }
        public void Touch() { }
        public void Touch(int pAmount) { }
        public string GetName() { return string.Empty; }

    }
}
