﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using System;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [Serializable]
    [KnInspector(true, false, false)]
    public class KnInspectorNoFoldout
    {
        public float IDontShowFoldout = 0.0f;
    }
}
