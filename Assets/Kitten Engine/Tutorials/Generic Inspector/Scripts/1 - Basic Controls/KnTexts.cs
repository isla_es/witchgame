﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnTexts : MonoBehaviour
    {
        [KnTextArea(5)]
        [KnHelpBox("To create a text area field, add the KnTextArea(minLines, maxLines) attribute to a string.\n" +
            "This one has 5 lines.")]
        public string TextArea = null;

        [KnMultiline]
        [KnHelpBox("To create a multiline text field, add the KnMultiline attribute to a string.")]
        public string MultilineText = null;

        [KnPasswordField]
        [KnHelpBox("To create a password field, add the KnPasswordField attribute to a string.")]
        public string Password = null;

        [KnMethodName(typeof(KnMethodNameHolder))]
        [KnHelpBox("To create a method name field, add the KnMethodName(targetType) attribute to a string.")]
        public string MethoToCall = null;
    }
}
