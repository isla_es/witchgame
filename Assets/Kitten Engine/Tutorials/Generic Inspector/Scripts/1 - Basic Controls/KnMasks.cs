﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    public enum E_Elements
    {
        Fire = 4,
        Ice = 6,
        Water = 2,
        Plant = 5
    }

    [KnInspector]
    public class KnMasks : MonoBehaviour
    {
        [KnMaskField("Fire", "Ice", "Water", "Plant")]
        [KnHelpBox("To create a mask field, add the KnMaskField(options) attribute to an int.")]
        public int Mask = 0;

        [KnEnumMask]
        [KnHelpBox("To create a enum mask, add the KnEnumMask attribute to an enum.\n" +
            "You can set the values you want for your enum, this won't affect the enum mask!")]
        public E_Elements EnumMask = E_Elements.Fire;
    }
}
