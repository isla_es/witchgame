﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnBools : MonoBehaviour
    {
        [KnToggleLeft]
        [KnHelpBox("To create a toggle left field, add the KnToggleLeft attribute to a bool.")]
        public bool ToggleLeft = false;

        [KnToggleGroup("f:ToggleFieldOne", "p:TogglePropertyTwo", "f:ToggleFieldThree")]
        [KnHelpBox("To create a toggle group, add the KnToogleGroup(group) attribute to a bool.\n" +
            "The group is composed by object's members. Don't forget to use HideInInspector attribute to hide the group's members.\n" +
            "   - \"f:\" followed by the name of the field.\n" +
            "   - \"p:\" followed by the name of the porperty.")]
        public bool ToogleGroup = false;

        [HideInInspector]
        public bool ToggleFieldOne = false;

        [SerializeField]
        private bool _togglePropertyTwo = false;

        public bool TogglePropertyTwo
        {
            get { return _togglePropertyTwo; }
            set { _togglePropertyTwo = value; }
        }

        [HideInInspector]
        public bool ToggleFieldThree = false;
    }
}
