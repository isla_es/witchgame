﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector(false, true)]
    [KnHelpBox("To show a MonoBehaviour properties, add the KnInspector(showFields, showProperties) attribute to your class.\n" +
        "Don't forget to add the SerializeField attribute to your private fields." +
        "Keep in mind that if you want to edit values, your properties must have a getter AND a setter.")]
    public class KnInspectorProperties : MonoBehaviour
    {
        [SerializeField]
        private float _floatField = 0.0f;
        [SerializeField]
        private string _stringField = "";

        public float FloatProperty
        {
            get { return _floatField; }
            set { _floatField = value; }
        }

        public string StringProperty
        {
            get { return _stringField; }
            set { _stringField = value; }
        }
    }
}
