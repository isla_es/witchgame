﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    [KnHelpBox("To Show pure C# objects, you must add the KnInspector attribute to your C# class.\n" +
        "Don't forget to add the Serializable attribute to your C# class!")]
    public class KnInspectorObjectHolder : MonoBehaviour
    {
        [KnHelpBox("You can chose to show your C# with a foldout.")]
        public KnInspectorShowFoldout ShowFoldout = new KnInspectorShowFoldout();
        [KnHelpBox("Or not.")]
        public KnInspectorNoFoldout DonShowFoldout = new KnInspectorNoFoldout();
    }
}
