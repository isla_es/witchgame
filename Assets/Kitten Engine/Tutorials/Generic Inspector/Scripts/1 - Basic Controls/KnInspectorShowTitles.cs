﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector(true, true, true, E_TitleDisplay.Name)]
    [KnHelpBox("To show the header (Field and/or Properties, add the KnInspector(showFields, showProperties, showFoldout, showTitle) attribute to your class.\n" +
        "You can display only the titles, the name of the object type or the full name of the object type.")]
    public class KnInspectorShowTitles : MonoBehaviour
    {
        public float MyFloatField = 0.0f;
        [SerializeField]
        private float _myFloatField = 0.0f;

        public float MyFloatProperty
        {
            get { return _myFloatField; }
            set { _myFloatField = value; }
        }
    }
}
