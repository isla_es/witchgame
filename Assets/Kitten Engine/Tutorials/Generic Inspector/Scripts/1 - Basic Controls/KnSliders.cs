﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnSliders : MonoBehaviour
    {
        [KnRange(0.0f, 100.0f)]
        [KnHelpBox("To create a float slider, add the KnRange(min, max) attribute to a float.\n"
            + "This slider goes from 0.0f to 100.0f")]
        public float Slider = 0.0f;

        [KnMinMaxSlider(0.0f, 100.0f)]
        [KnHelpBox("To create a min max slider, add KnMinMaxSlider(minLimit, maxLimit) attribute to a Vector2.\n"
            + "This one goes from 0.0f to 100.0f.\n"
            + "The X value of the Vector2 represents the min value while Y represents the max value.")]
        public Vector2 MinMaxSlider = Vector2.zero;
    }
}
