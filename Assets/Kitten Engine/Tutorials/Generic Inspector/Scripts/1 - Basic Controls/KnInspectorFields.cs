﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    [KnHelpBox("To show a MonoBehaviour fields, add the KnInspector attribute to your class.")]
    public class KnInspectorFields : MonoBehaviour
    {
        public float FloatField = 0.0f;
        public string StringField = "";
    }
}