﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
using System.Collections.Generic;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector(true, true)]
    public class KnPopups : MonoBehaviour
    {
        [KnPopup("Zero", "One", "Two", "Three")]
        [KnHelpBox("To create a popup, add the KnPopup(parameters[]) attribute to an int.\n"
            + "The parameters are the popup values you want to display.")]
        public int PopupOnAnInt = 0;

        [KnPopup("Zero", "One", "Two", "Three")]
        [KnHelpBox("You can also create a popup on a string.")]
        public string PopupOnAString = null;

        [KnIntPopup("Two", 2, "Three", 3, "One", 1, "Zero", 0)]
        [KnHelpBox("To create an integer popup, add the KnIntPopup(parameters[]) attribute to an int.\n"
            + "The parameters come by pair: string labelToDisplay ; int value.")]
        public int IntPopup = 0;

        [KnAdvancedPopup(null, "GetOptions")]
        [KnHelpBox("To create an advanced popup, add the KnAdvancedPopup(class, function) attribute to an int.\n"
           + "Set the class to null if the method is declared by the object itself.")]
        public int AdvancedPopupOnAnInt = 0;

        [KnAdvancedPopup(null, "GetOptions")]
        [KnHelpBox("It works as well on a string!")]
        public string AdvancedPopupOnAString = null;

        [KnAdvancedPopup(null, "GetOptionsWithParameters", false, new object[] { 6, "p:PopupOnAnIntValue", "f:PopupOnAString" })]
        [KnHelpBox("You can also call methods with parameters.\n" +
            "You can use special keywords to give parameters:\n" +
            "   - \"self\" for the object.\n" +
            "   - \"f:\" followed by the name of the field.\n" +
            "   - \"p:\" followed by the name of the porperty.")]
        public int AdvancedPopupOnAnIntWithParameters = 0;

        [KnAdvancedPopup("KittenEditor.Tutorials.GenericInspector.KnBasicControlHelper", "GetStaticOptionsWithParameters", true, new object[] { "f:PopupOnAString", "p:PopupOnAnIntValue" })]
        [KnHelpBox("You can call static methods from an other class.\n" +
            "If you use namespaces, dont't forget to specify the whole namespace with you class name.\n" +
            "Be carefull to set the assembly, specialy, if you use dll's !")]
        public int AdvancedPopupOnAnIntCallingStaticEditorMethod = 0;

        #region Labels
        [KnLabel]
        public string PopupOnAnIntValue
        {
            get { return PopupOnAnInt.ToString(); }
        }

        [KnLabel]
        public string PopupOnAStringValue
        {
            get { return PopupOnAString.ToString(); }
        }

        [KnLabel]
        public string IntPopupValue
        {
            get { return IntPopup.ToString(); }
        }

        [KnLabel]
        public string AdvancedPopupOnAnIntValue
        {
            get { return AdvancedPopupOnAnInt.ToString(); }
        }

        [KnLabel]
        public string AdvancedPopupOnAStringValue
        {
            get { return AdvancedPopupOnAString.ToString(); }
        }

        [KnLabel]
        public string AdvancedPopupOnAnIntCallingStaticEditorMethodValue
        {
            get { return AdvancedPopupOnAnIntCallingStaticEditorMethod.ToString(); }
        }
        #endregion

        public List<GUIContent> GetOptions()
        {
            List<GUIContent> contents = new List<GUIContent>();
            for (int i = 0; i < 4; i++)
            {
                contents.Add(new GUIContent("Get Options " + i.ToString()));
            }
            return contents;
        }

        public List<GUIContent> GetOptionsWithParameters(int pLength, string pPrefix, string pSuffix)
        {
            List<GUIContent> contents = new List<GUIContent>();
            for (int i = 0; i < pLength; i++)
            {
                contents.Add(new GUIContent(string.Format("{0} - Option {1} - {2}", pPrefix, i, pSuffix)));
            }
            return contents;
        }
    }
}
