﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    [KnPreview("This is an awesome preview !", "KittenEditor.Tutorials.GenericInspector.KnPreviewDrawer", "DrawCustomPreview", "OnCustomPreviewSettings")]
    [KnHelpBox("To create a custom preview to your object, add the KnPreview(title, class, DrawPreview, OnPreviewSettings) attribute to your class.")]
    public class KnCustomPreview : MonoBehaviour
    {
        public Texture Texture = null;
        private ScaleMode _scaleMode = ScaleMode.ScaleToFit;
        public ScaleMode ScaleMode
        {
            get { return _scaleMode; }
            set { _scaleMode = value; }
        }
    }
}
