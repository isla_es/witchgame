﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
using System.Collections.Generic;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector(true, true)]
    public class KnReadOnlyLabel : MonoBehaviour
    {
        [KnLabel]
        [KnHelpBox("To display a read-only value, add the KnLabel attribute to the member.")]
        public float FloatLabel = 5.4f;

        [KnSelectableLabel]
        [KnHelpBox("To display a selectable read-only value, add the KnSelectableLabel attribute to the member.")]
        public Vector3 Vector3SelectableLabel = new Vector3(12.0f, 45.4f, 98.0f);

        private int _intLabel = 0;
        [KnLabel]
        [KnHelpBox("KnLabel also works with properties that as only a getter!")]
        public int IntLabel
        {
            get { return _intLabel; }
        }

        private string _selectableLabel = "Select Me";

        [KnSelectableLabel]
        [KnHelpBox("KnSelectableLabel also works with properties that as only a getter!")]
        public string SelectableLabel
        {
            get { return _selectableLabel; }
        }
    }
}
