﻿/**********************************************************************************************************************
* Kitten Engine Tutorials
* Invincible Cat © 2012-2014
* Author : Timothée Verrouil
********************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
using System.Collections.Generic;
using KittenEngine.EditorUtils;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnConditions : MonoBehaviour
    {
        [KnLabel]
        [KnBeginShowCondition(E_EditorStateCondition.Editing)]
        [KnEndShowCondition]
        [KnHelpBox("To create a conditional group, add the KnBeginShowCondition(editorState) attribute to the first member and the KnEndShowCondition attribute to the last member of the group.\n" +
            "Try it! Press play, you will see by yourself!")]
        public string Editor = "I'm not playing!";

        [KnLabel]
        [KnBeginShowCondition(E_EditorStateCondition.Playing)]
        [KnEndShowCondition]
        [KnHelpBox("See? It's easy right?")]
        public string Playing = "I'm playing!";
    }
}
