﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
using System.Collections.Generic;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector]
    public class KnDisplayInformations : MonoBehaviour
    {
        [KnHeader("I'm the best header ever!")]
        [KnHelpBox("To create a header, add the KnHeader(text) attribute to the member.")]
        public string AboveMeIsAHeader = "";

        [KnTooltip("I'm the tooltip.")]
        [KnHelpBox("To create a tooltip, add the KnTooltip(text) attribute to the member.")]
        public string IHaveATooltipInMe = "";

        [KnHelpBox("To create a helpox, add the KnHelpbox(text, type, order) to the member", E_MessageType.None, 0)]
        [KnHelpBox("You can add more than one tooltip, but be carefull of the order !", E_MessageType.None, 1)]
        [KnHelpBox("I'm an info helpbox", E_MessageType.Info, 2)]
        [KnHelpBox("I'm a warning helpbox", E_MessageType.Warning, 3)]
        [KnHelpBox("I'm an error helpbox", E_MessageType.Error, 4)]
        public string ICanShowYouHelpBoxes = "";
    }
}
