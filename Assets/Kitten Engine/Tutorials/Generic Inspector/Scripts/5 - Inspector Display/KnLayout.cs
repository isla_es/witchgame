﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
using System.Collections.Generic;
using KittenEngine.EditorUtils;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector(true, false, true, E_TitleDisplay.None, EditorUtils.E_DisplayType.Fitted)]
    public class KnLayout : MonoBehaviour
    {
        [KnGUIColor(255.0f, 0.0f, 0.0f)]
        [KnHelpBox("To change the color, add the KnGUIColor(red, green, blue, alpha) attribute to the member.")]
        public int ARedInt = 1000;

        [KnIndent(10)]
        [KnHelpBox("To indent, add the KnIndent(amount) attribute to the member.")]
        public int ImFarAway = 10;

        [KnSeparatorLine]
        [KnHelpBox("To create a line, add the KnSeparatorLine attribute to the member.")]
        public int LookAtTheLine = 2;

        [KnSpace(50)]
        [KnHelpBox("To create a space, add the KnSpace(height) attribute to the member.")]
        public int WhatASpace = 5;

        [KnIndent(-10)]
        [KnBeginHorizontal]
        public int X = 2;
        public int Y = 3;
        [KnEndHorizontal]
        [KnHelpBox("To create an horizontal group, add the KnBeginHorizontal attribute to the first member and the KnEndHorizontal to the last member of the group.")]
        public int Z = 5;

        [KnDisplay(E_DisplayType.Normal)]
        [KnHelpBox("To change how member are displayed, add the KnDisplay(display) attribute.\n"
            + "You can also set the default display type with the KnInspector Attribute!\n"
            + "So, this is the normal display.")]
        public int UnityIntDisplay = 0;

        [KnDisplay(E_DisplayType.Fitted)]
        [KnHelpBox("And this is the fitted display.\n"
            + "Currently, the default display type is fitted.")]
        public int FittedIntDisplay = 0;
    }
}
