﻿/**********************************************************************************************************************
 * Kitten Engine Tutorials
 * Invincible Cat © 2012-2014
 * Author : Timothée Verrouil
 * *******************************************************************************************************************/

#region Imports
using KittenEngine.GenericInspector;
using UnityEngine;
using System.Collections.Generic;
#endregion

namespace KittenEngine.Tutorials.GenericInspector
{
    [KnInspector(true, false, true, E_TitleDisplay.None, KittenEngine.EditorUtils.E_DisplayType.Normal, false)]
    [KnHelpBox("To create an editor window, you need to create an new class in an Editor folder.\n"
        + "The class must derive from KnGenericEditorWindow<T1, T2> where T1 is the editor window and T2 is the Unity Object instance you want to display.\n"
        + "The instance can come from the scene or the project browser. I think it's better to use an instance from project browser because you can save the parameters "
        + "witout caring about the scene you are editing.\n"
        + "To avoir mistakes, you can hide the script field of the instance in the KnInspector attribute.")]
    public class KnEditorWindowInstance : MonoBehaviour
    {
        [KnButton("Print Value", "", null, "PrintValue", null, false)]
        [KnRange(0.0f, 100.0f)]
        public float Value = 0.0f;

        public void PrintValue()
        {
            Debug.Log("You ask me to print: " + Value.ToString());
        }
    }
}
