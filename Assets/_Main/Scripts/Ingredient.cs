﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Apothecary
{
	public class Ingredient : Item
	{
		
	}
	[System.Serializable]
	public class Item
	{
		public string name;
		public string description;
		public Texture2D image;
	}
}
