﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KittenEngine.GenericInspector;

namespace Apothecary
{
	public class RecipeBook : MonoBehaviour
	{
		[System.Serializable]
		public struct Entry
		{
			public string name;
			public string description;
			public Texture2D image;
		}

		public List<Entry> entries = new List<Entry>();
	}
}